
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

entity TCDS_capture is

  port (
    clk_LHC           : in  std_logic;
    reset             : in  std_logic;
    TCDS_p            : in  std_logic;
    TCDS_n            : in  std_logic;
    data              : out std_logic_vector(7 downto 0) := "00000000";
    data_is_k_char    : out std_logic                    := '0';
    data_valid        : out std_logic                    := '0';
    data_locked       : out std_logic                    := '0';
    error_bad_code    : out std_logic                    := '0';
    error_disparity   : out std_logic                    := '0';
    error_alignment   : out std_logic                    := '0';
    error_overflow    : out std_logic                    := '0';
    DCM_phase         : out signed(7 downto 0)           := x"00";
    DCM_status        : out std_logic_vector(2 downto 0) := "000";
    DCM_input_stopped : out std_logic                    := '0';
    DCM_locked        : out std_logic                    := '0';
    DCM_valid         : out std_logic                    := '0'
    );
end entity TCDS_capture;

architecture behavioral of TCDS_capture is

  component pacd is
    port (
      iPulseA : in  std_logic;
      iClkA   : in  std_logic;
      iRSTAn  : in  std_logic;
      iClkB   : in  std_logic;
      iRSTBn  : in  std_logic;
      oPulseB : out std_logic);
  end component pacd;

  component Phased_LHC_DCM is
    port (
      clk_LHC           : in  std_logic;
      clk_LHC_5x_p      : out std_logic;
      clk_LHC_5x_n      : out std_logic;
      PSCLK             : in  std_logic;
      PSEN              : in  std_logic;
      PSINCDEC          : in  std_logic;
      PSDONE            : out std_logic;
      RESET             : in  std_logic;
      STATUS            : out std_logic_vector(2 downto 0);
      INPUT_CLK_STOPPED : out std_logic;
      LOCKED            : out std_logic;
      CLK_VALID         : out std_logic);
  end component Phased_LHC_DCM;

  component capture_DDR is
    port (
      clk_DDR     : in  std_logic;
      clk_DDR_180 : in  std_logic;
      stream_in_p : in  std_logic;
      stream_in_n : in  std_logic;
      stream_out  : out std_logic_vector(1 downto 0));
  end component capture_DDR;

  component sync_lockon is
    generic (
      SYNC_STREAM : std_logic_vector);
    port (
      reset           : in  std_logic;
      clk_in_stream   : in  std_logic;
      stream_in       : in  std_logic_vector(1 downto 0);
      clk_out_stream  : in  std_logic;
      data_locked     : out std_logic                    := '0';
      data_out        : out std_logic_vector(9 downto 0) := (others => '0');
      data_valid      : out std_logic                    := '0';
      alignment_error : out std_logic                    := '0';
      overflow_error  : out std_logic                    := '0');
  end component sync_lockon;

  component decode_8b10b_top is
    generic (
      C_HAS_CODE_ERR : integer;
      C_HAS_DISP_ERR : integer;
      C_HAS_CE       : integer;
      C_HAS_ND       : integer;
      C_HAS_SINIT    : integer);
    port (
      CLK        : in  std_logic                    := '0';
      DIN        : in  std_logic_vector(9 downto 0) := (others => '0');
      DOUT       : out std_logic_vector(7 downto 0);
      KOUT       : out std_logic;
      CE         : in  std_logic                    := '0';
      CE_B       : in  std_logic                    := '0';
      CLK_B      : in  std_logic                    := '0';
      DIN_B      : in  std_logic_vector(9 downto 0) := (others => '0');
      DISP_IN    : in  std_logic                    := '0';
      DISP_IN_B  : in  std_logic                    := '0';
      SINIT      : in  std_logic                    := '0';
      SINIT_B    : in  std_logic                    := '0';
      CODE_ERR   : out std_logic                    := '0';
      CODE_ERR_B : out std_logic                    := '0';
      DISP_ERR   : out std_logic                    := '0';
      DISP_ERR_B : out std_logic                    := '0';
      DOUT_B     : out std_logic_vector(7 downto 0);
      KOUT_B     : out std_logic;
      ND         : out std_logic                    := '0';
      ND_B       : out std_logic                    := '0';
      RUN_DISP   : out std_logic;
      RUN_DISP_B : out std_logic;
      SYM_DISP   : out std_logic_vector(1 downto 0);
      SYM_DISP_B : out std_logic_vector(1 downto 0));
  end component decode_8b10b_top;

  -------------------------------------------------------------------------------
  -- signals
  -------------------------------------------------------------------------------
  signal clk_LHC_5x_p : std_logic := '0';
  signal clk_LHC_5x_n : std_logic := '1';

  signal TCDS_resync : std_logic := '0';

  --phase change  
  constant TCDS_search_start : std_logic_vector(7 downto 0) := x"00";
  constant TCDS_search_end   : std_logic_vector(7 downto 0) := x"10";
  signal TCDS_search_counter : std_logic_vector(7 downto 0) := TCDS_search_start;
  
  --DCM phase change signals
  signal phase_change_valid : std_logic                      := '0';
  signal phase_change       : std_logic                      := '1';
  signal phase_change_wait  : std_logic                      := '0';
  signal phase_change_done  : std_logic                      := '0';
  signal current_phase      : signed(7 downto 0)   := (others => '0');
  signal phase_good         : std_logic_vector(255 downto 0) := (others => '0');
  signal phase_shift_direction : std_logic := '1';
  signal phase_shift_good_counts : std_logic_vector(63 downto 0) := (others => '0');
  signal phase_shift_good_8b10b_chars : std_logic_vector(63 downto 0) := (others => '0');
  signal local_DCM_status        : std_logic_vector(2 downto 0) := "000";
  constant good_8b10b_char_max_count : integer := 3;
  
  --sync_lockon
  signal TCDS_stream     : std_logic_vector(1 downto 0) := "00";
  signal alignment_error : std_logic                    := '0';
  signal overflow_error  : std_logic                    := '0';


  --8b10b deserializer
  signal data_10b             : std_logic_vector(9 downto 0) := (others => '0');
  signal data_10b_valid       : std_logic                    := '0';
  signal data_8b              : std_logic_vector(8 downto 0) := "000000000";
  signal data_8b_valid        : std_logic                    := '0';
  signal data_code_error      : std_logic                    := '0';
  signal data_disparity_error : std_logic                    := '0';
  signal local_data_valid     : std_logic                    := '0';

  signal data_locked_buffer : std_logic := '0';
  signal locked             : std_logic := '0';

begin

  -------------------------------------------------------------------------------
  -- Generate clock for TCDS DDR capture
  -------------------------------------------------------------------------------  

  Phased_LHC_DCM_1 : entity work.Phased_LHC_DCM
    port map (
      clk_LHC           => clk_LHC,
      clk_LHC_5x_p      => clk_LHC_5x_p,
      clk_LHC_5x_n      => clk_LHC_5x_n,
      PSCLK             => clk_LHC,
      PSEN              => phase_change_valid,
      PSINCDEC          => phase_shift_direction,
      PSDONE            => phase_change_done,
      RESET             => reset,
      STATUS            => local_DCM_status,
      INPUT_CLK_STOPPED => DCM_input_stopped,
      LOCKED            => locked,
      CLK_VALID         => DCM_valid);
  DCM_locked <= locked;
  DCM_status <= local_DCM_status;
  
--  TCDS_phase_finder : process (clk_LHC, locked) is
--  begin  -- process TCDS_phase_finder
--    if locked = '0' then                -- asynchronous reset (active low)
--      phase_change_valid  <= '0';
--      phase_change_wait   <= '0';
--      TCDS_search_counter <= TCDS_search_start;
--      TCDS_resync         <= '0';
--      current_phase       <= (others => '0');
--      phase_good          <= (others => '0');
--    elsif clk_LHC'event and clk_LHC = '1' then  -- rising clock edge
--      phase_change_valid <= '0';
--      if current_phase(8) = '0' then
--
--        if phase_change_wait = '1' then
--          -- while waiting for PLL phase adjust, holdsystem in resync
--          TCDS_resync <= '1';
--          -- wait until the phase change is done and then stop waiting
--          if phase_change_done = '1' then
--            TCDS_search_counter <= TCDS_search_start;
--            TCDS_resync       <= '0';
--            phase_change_wait <= '0';
--          end if;
--        elsif data_locked_buffer = '0' then
--          -- wait for the data to lock, if it doesn't by search_max, adjust the phase
--          TCDS_search_counter <= std_logic_vector(unsigned(TCDS_search_counter) + 1);
--          if TCDS_search_counter = TCDS_search_end then
--            
--            --record that this phase is bad
--            phase_good(to_integer(unsigned(current_phase))) <= '0';
--            current_phase <= std_logic_vector(unsigned(current_phase) + 1);
--            
--            -- start the next search
--            phase_change_valid  <= '1';
--            phase_change_wait   <= '1';
--          end if;
--        else
--          -- data_locked_buffer = '1'
--          --record that this phase is bad
--          phase_good(to_integer(unsigned(current_phase))) <= '1';
--          current_phase <= std_logic_vector(unsigned(current_phase) + 1);
--          
--          -- start the next search
--          phase_change_valid  <= '1';
--          phase_change_wait   <= '1';
--          
--        end if;
--      end if;
--
--    end if;
--  end process TCDS_phase_finder;

  DCM_phase <= current_phase;
  TCDS_phase_adjuster : process (clk_LHC, locked) is
  begin  -- process TCDS_phase_adjuster
    if locked = '0' then                -- asynchronous reset (active low)
      phase_change_valid  <= '0';
      phase_change_wait   <= '0';
      TCDS_search_counter <= TCDS_search_start;
      TCDS_resync         <= '0';
    elsif clk_LHC'event and clk_LHC = '1' then  -- rising clock edge
      --action signal resets
      phase_change_valid <= '0';
      TCDS_resync        <= '0';

      if phase_change_wait = '1' then
        -- while waiting for PLL phase adjust, holdsystem in resync
        TCDS_resync <= '1';
        -- wait until the phase change is done and then stop waiting
        if phase_change_done = '1' then
          TCDS_resync       <= '0';
          phase_change_wait <= '0';
        end if;
      elsif data_locked_buffer = '0' then
        -- wait for the data to lock, if it doesn't by search_max, adjust the phase
        TCDS_search_counter <= std_logic_vector(unsigned(TCDS_search_counter) + 1);
        if TCDS_search_counter = TCDS_search_end then
          TCDS_search_counter <= TCDS_search_start;
          phase_change_valid  <= '1';
          phase_change_wait   <= '1';
          TCDS_resync         <= '1';
        end if;        
      elsif data_code_error = '1' then
        -- we are locked, but got a bad character code
        -- go back to searching        
        phase_shift_good_counts <= (others => '0');
        TCDS_resync         <= '1';
        TCDS_search_counter <= TCDS_search_start;
        phase_change_valid  <= '1';
        phase_change_wait   <= '1';
--        if local_DCM_status(0) = '1' then
--          phase_shift_direction <= not phase_shift_direction;          
--        end if;
        phase_shift_good_8b10b_chars <= (others => '0');
        if phase_shift_direction = '1' and current_phase < x"7F" then
          current_phase <= current_phase + x"01";
        elsif current_phase > x"FF" then          
          current_phase <= current_phase - x"01";
        elsif current_phase = x"FF" then
          phase_shift_direction <= '1';
        elsif current_phase = x"7F" then
          phase_shift_direction <= '0';
        end if;

      else
        -- check if we have gone enough phase shifts where we get good locks
        if phase_shift_good_counts(63) = '0' then
          -- count the number of good decoded characters at this phase shift
          if local_data_valid = '1' and data_code_error = '0' then
            phase_shift_good_8b10b_chars <= phase_shift_good_8b10b_chars(62 downto 0) & '1';
          end if;
          -- if we have 64 good characters, move to the next phase
          if phase_shift_good_8b10b_chars(good_8b10b_char_max_count) = '1' then
            phase_shift_good_counts <= phase_shift_good_counts(62 downto 0) & '1';
            phase_shift_good_8b10b_chars <= (others => '0');
            TCDS_resync         <= '1';
            TCDS_search_counter <= TCDS_search_start;
            phase_change_valid  <= '1';
            phase_change_wait   <= '1';
--            if local_DCM_status(0) = '1' then
--              phase_shift_direction <= not phase_shift_direction;          
--            end if;
            phase_shift_good_8b10b_chars <= (others => '0');
            if phase_shift_direction = '1' and current_phase < x"7F" then
              current_phase <= current_phase + x"01";
            elsif current_phase > x"FF" then          
              current_phase <= current_phase - x"01";
            elsif current_phase = x"FF" then
              phase_shift_direction <= '1';
            elsif current_phase = x"7F" then
              phase_shift_direction <= '0';
            end if;
          end if;
        end if;        
      -- we are happy
      end if;
    end if;
  end process TCDS_phase_adjuster;


  -------------------------------------------------------------------------------
  -- DDR capture of the TCDS stream
  -------------------------------------------------------------------------------  
  capture_DDR_1 : entity work.capture_DDR
    port map (
      clk_DDR     => clk_LHC_5x_p,
      clk_DDR_180 => clk_LHC_5x_n,
      stream_in_p => TCDS_p,
      stream_in_n => TCDS_n,
      stream_out  => TCDS_stream);

  -------------------------------------------------------------------------------
  -- Search incoming stream for sync sequence and then align to that
  -------------------------------------------------------------------------------  
  sync_lockon_1 : entity work.sync_lockon
    generic map (
      SYNC_STREAM => "1100000101")
--      SYNC_STREAM => "1010000011")
    port map (
      resync          => TCDS_resync,
      clk_in_stream   => clk_LHC_5x_p,
      stream_in       => TCDS_stream,
      clk_out_stream  => clk_LHC,
      data_out        => data_10b,
      data_valid      => data_10b_valid,
      alignment_error => alignment_error,
      overflow_error  => overflow_error);

  -- determine if the data is locked
  data_locked <= data_locked_buffer;
  process (clk_LHC) is
  begin  -- process
    if clk_LHC'event and clk_LHC = '1' then  -- rising clock edge
      -- remove data_locked_buffer on TCDS_resync
      if phase_change_valid = '1' then
        data_locked_buffer <= '0';
      else
        -- data_locked_buffer goes high once we get a data_10b_valid
        if data_10b_valid = '1' then
          data_locked_buffer <= '1';
        end if;
      end if;
    end if;
  end process;

  -------------------------------------------------------------------------------
  -- Decode 10b symbols to 8b symbols
  -------------------------------------------------------------------------------    
  decode_8b10b_top_1 : entity work.decode_8b10b_top
    generic map (
      C_HAS_CODE_ERR => 1,
      C_HAS_DISP_ERR => 1,
      C_HAS_CE       => 1,
      C_HAS_ND       => 1,
      C_HAS_SINIT    => 1)
    port map (
      CLK        => clk_LHC,
      DIN        => data_10b,
      DOUT       => data,
      KOUT       => data_is_k_char,
      CE         => data_10b_valid,
      CE_B       => '0',
      CLK_B      => '0',
      DIN_B      => "0000000000",
      DISP_IN    => '0',
      DISP_IN_B  => '0',
      SINIT      => '0',
      SINIT_B    => '0',
      CODE_ERR   => data_code_error,
      CODE_ERR_B => open,
      DISP_ERR   => data_disparity_error,
      DISP_ERR_B => open,
      DOUT_B     => open,
      KOUT_B     => open,
      ND         => local_data_valid,
      ND_B       => open,
      RUN_DISP   => open,
      RUN_DISP_B => open,
      SYM_DISP   => open,
      SYM_DISP_B => open);


  data_valid <= local_data_valid;
  error_bad_code  <= data_code_error;
  error_disparity <= data_disparity_error;
  error_alignment <= alignment_error;
  error_overflow  <= overflow_error;


end architecture behavioral;
