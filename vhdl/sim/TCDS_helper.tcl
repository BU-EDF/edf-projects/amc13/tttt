source vhdl/sim/8b10b_helper.tcl

proc send_TCDS { disp stream_p stream_n TTS } {    
    for { set i 0 } {$i < 4 } {incr i} {
	set disp [gen_8b10b_control_code BC $disp $stream_p $stream_n]
    }
    set disp [gen_8b10b_data_code $TTS $disp $stream_p $stream_n]
    return $disp
}



