-------------------------------------------------------------------------------
-- top.vhd :
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_signed."+";
use ieee.std_logic_signed."=";

use ieee.numeric_std.all;
use IEEE.std_logic_misc.all;

library UNISIM;
use UNISIM.vcomponents.all;

entity uC is

  port (
    clk   : in std_logic;               -- 100MHz oscillator input
    reset : in std_logic;               -- reset to picoblaze

    UART_Rx : in  std_logic;              -- serial in
    UART_Tx : out std_logic;              -- serial out

    -- register interface
    reg_addr       : out std_logic_vector(15 downto 0);
    reg_data_in_rd : out std_logic;
    reg_data_in    : in  std_logic_vector(63 downto 0);
    reg_data_in_dv : in  std_logic;
    reg_data_out   : out std_logic_vector(63 downto 0);
    reg_wr         : out std_logic
    );


end entity uC;

architecture arch of uC is

--
-- KCPSM6 uc
-- 
  component kcpsm6
    generic(
      hwbuild                 : std_logic_vector(7 downto 0)  := X"00";
      interrupt_vector        : std_logic_vector(11 downto 0) := X"600";
      scratch_pad_memory_size : integer                       := 256);
    port (
      address        : out std_logic_vector(11 downto 0);
      instruction    : in  std_logic_vector(17 downto 0);
      bram_enable    : out std_logic;
      in_port        : in  std_logic_vector(7 downto 0);
      out_port       : out std_logic_vector(7 downto 0);
      port_id        : out std_logic_vector(7 downto 0);
      write_strobe   : out std_logic;
      k_write_strobe : out std_logic;
      read_strobe    : out std_logic;
      interrupt      : in  std_logic;
      interrupt_ack  : out std_logic;
      sleep          : in  std_logic;
      reset          : in  std_logic;
      clk            : in  std_logic);
  end component;

--
-- KCPSM6 ROM
-- 
  component cli
    generic(
      C_FAMILY             : string  := "S6";
      C_RAM_SIZE_KWORDS    : integer := 1;
      C_JTAG_LOADER_ENABLE : integer := 0);
    port (
      address     : in  std_logic_vector(11 downto 0);
      instruction : out std_logic_vector(17 downto 0);
      enable      : in  std_logic;
      rdl         : out std_logic;
      clk         : in  std_logic);
  end component;

--
-- UART Transmitter     
--
  component uart_tx6
    port (
      data_in             : in  std_logic_vector(7 downto 0);
      en_16_x_baud        : in  std_logic;
      serial_out          : out std_logic;
      buffer_write        : in  std_logic;
      buffer_data_present : out std_logic;
      buffer_half_full    : out std_logic;
      buffer_full         : out std_logic;
      buffer_reset        : in  std_logic;
      clk                 : in  std_logic);
  end component;

--
-- UART Receiver
--
  component uart_rx6
    port (
      serial_in           : in  std_logic;
      en_16_x_baud        : in  std_logic;
      data_out            : out std_logic_vector(7 downto 0);
      buffer_read         : in  std_logic;
      buffer_data_present : out std_logic;
      buffer_half_full    : out std_logic;
      buffer_full         : out std_logic;
      buffer_reset        : in  std_logic;
      clk                 : in  std_logic);
  end component;

-----------------------------------------------------------------------------
-- Signals
-----------------------------------------------------------------------------
  signal rst : std_logic;

--
-- KCPSM6 & ROM signals
--
  signal address        : std_logic_vector(11 downto 0);
  signal instruction    : std_logic_vector(17 downto 0);
  signal bram_enable    : std_logic;
  signal in_port        : std_logic_vector(7 downto 0) := "00000000";
  signal out_port       : std_logic_vector(7 downto 0) := "00000000";
  signal port_id        : std_logic_vector(7 downto 0) := "00000000";
  signal write_strobe   : std_logic                    := '0';
  signal k_write_strobe : std_logic                    := '0';
  signal read_strobe    : std_logic                    := '0';
  signal interrupt      : std_logic;
  signal interrupt_ack  : std_logic;
  signal kcpsm6_sleep   : std_logic;
  signal kcpsm6_reset   : std_logic;

--
-- UART_TX signals
--
  signal UART_Tx_local          : std_logic := '1';
  signal uart_tx_data_in      : std_logic_vector(7 downto 0);
  signal write_to_uart_tx     : std_logic;
  signal uart_tx_data_present : std_logic;
  signal uart_tx_half_full    : std_logic;
  signal uart_tx_full         : std_logic;
  signal uart_tx_reset        : std_logic := '0';

--
-- UART_RX signals
--
  signal UART_Rx_local          : std_logic := '1';
  signal uart_rx_data_out     : std_logic_vector(7 downto 0);
  signal read_from_uart_rx    : std_logic;
  signal uart_rx_data_present : std_logic;
  signal uart_rx_half_full    : std_logic;
  signal uart_rx_full         : std_logic;
  signal uart_rx_reset        : std_logic := '0';

--
-- UART baud rate signals
--
  signal baud_count   : integer range 0 to 67 := 0;
  signal en_16_x_baud : std_logic             := '0';

--
-- DAQ register signals
--
  type Register_In is record
    byte_select : std_logic_vector(2 downto 0);
    data_buffer : std_logic_vector(63 downto 0);
    valid : std_logic;
    data_read  : std_logic;
  end record Register_In;
  signal reg_in : Register_In := ("000",X"0000000000000000",'0','0');

  type Register_Out is record
    byte_select : std_logic_vector(2 downto 0);
    data_buffer : std_logic_vector(63 downto 0);
    data_write  : std_logic;
  end record Register_Out;
  signal reg_out : Register_Out := ("000",X"0000000000000000",'0');

  signal reg_address_buffer : std_logic_vector(15 downto 0) := (others => '0');
  
begin  -- architecture arch


-----------------------------------------------------------------------------------------
-- Instantiate KCPSM6 and connect to Program Memory
-----------------------------------------------------------------------------------------
  processor : kcpsm6
    generic map (
      hwbuild                 => X"01",
      interrupt_vector        => X"7FF",
      scratch_pad_memory_size => 256)
    port map(
      address        => address,
      instruction    => instruction,
      bram_enable    => bram_enable,
      port_id        => port_id,
      write_strobe   => write_strobe,
      k_write_strobe => k_write_strobe,
      out_port       => out_port,
      read_strobe    => read_strobe,
      in_port        => in_port,
      interrupt      => interrupt,
      interrupt_ack  => interrupt_ack,
      sleep          => kcpsm6_sleep,
      reset          => rst,
      clk            => clk);

--
--Disable sleep and interrupts on kcpsm6
--
  kcpsm6_sleep <= '0';
  interrupt    <= interrupt_ack;
  rst          <= kcpsm6_reset or reset;

--
-- KCPSM6 ROM
--
  program_rom : cli
    generic map(
      C_FAMILY             => "S6",     --Family 'S6', 'V6' or '7S'
      C_RAM_SIZE_KWORDS    => 2,        --Program size '1', '2' or '4'
      C_JTAG_LOADER_ENABLE => 0)        --Include JTAG Loader when set to '1' 
    port map(
      address     => address,
      instruction => instruction,
      enable      => bram_enable,
      rdl         => kcpsm6_reset,
      clk         => clk);

--
-- UART Transmitter
--

  tx : uart_tx6
    port map (
      data_in             => uart_tx_data_in,
      en_16_x_baud        => en_16_x_baud,
      serial_out          => UART_Tx,
      buffer_write        => write_to_uart_tx,
      buffer_data_present => uart_tx_data_present,
      buffer_half_full    => uart_tx_half_full,
      buffer_full         => uart_tx_full,
      buffer_reset        => uart_tx_reset,
      clk                 => clk);
--
-- UART Receiver
--
  rx : uart_rx6
    port map (
      serial_in           => UART_Rx,
      en_16_x_baud        => en_16_x_baud,
      data_out            => uart_rx_data_out,
      buffer_read         => read_from_uart_rx,
      buffer_data_present => uart_rx_data_present,
      buffer_half_full    => uart_rx_half_full,
      buffer_full         => uart_rx_full,
      buffer_reset        => uart_rx_reset,
      clk                 => clk);


-----------------------------------------------------------------------------------------
-- RS232 (UART) baud rate 
-----------------------------------------------------------------------------------------
--
-- To set serial communication baud rate to 115,200 then en_16_x_baud must pulse 
-- High at 1,843,200Hz which is every 54 cycles at 100MHz. In this implementation 
-- a pulse is generated every 54 cycles resulting is a baud rate of 110,000 baud 
--

  baud_rate : process(clk)
  begin
    if clk'event and clk = '1' then
      if baud_count = 21 then           -- counts 22 states including zero
        baud_count   <= 0;
        en_16_x_baud <= '1';            -- single cycle enable pulse
      else
        baud_count   <= baud_count + 1;
        en_16_x_baud <= '0';
      end if;
    end if;
  end process baud_rate;






-------------------------------------------------------------------------------
-- DAQ interface
-------------------------------------------------------------------------------
  DAQ_interface : process (clk, reset) is
  begin  -- process DAQ_interface
    if reset = '1' then                 -- asynchronous reset (active high)
      reg_in.valid <= '0';
            
      reg_data_in_rd      <= '0';
      reg_data_out        <= (others => '0');
      reg_wr              <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge

      --update register address
      reg_addr <= reg_address_buffer;

      --handle a read from the register
      reg_data_in_rd <= '0';
      if reg_in.data_read = '1' then
        --start a read
        reg_data_in_rd <= '1';
        --set valid to zero
        reg_in.valid <= '0';
      elsif reg_data_in_dv = '1' then
        reg_in.valid <= '1';
        reg_in.data_buffer <= reg_data_in;
      end if;

      --handle a write to the register
      reg_wr <= '0';
      if reg_out.data_write = '1' then
        --start write
        reg_wr <= '1';
        --output data
        reg_data_out <= reg_out.data_buffer;        
      end if;
      
    end if;
  end process DAQ_interface;

-----------------------------------------------------------------------------
-- Reading interface of the UC to the UART
-----------------------------------------------------------------------------
  input_ports : process(clk)
  begin
    if clk'event and clk = '1' then
      case port_id is
        -----------------------------------------------------------------------
        -- UART status port
        when x"00" =>
          in_port(0)          <= uart_tx_data_present;
          in_port(1)          <= uart_tx_half_full;
          in_port(2)          <= uart_tx_full;
          in_port(3)          <= uart_rx_data_present;
          in_port(4)          <= uart_rx_half_full;
          in_port(5)          <= uart_rx_full;
          in_port(7 downto 6) <= "00";
        -----------------------------------------------------------------------
        -- UART input port
        when x"01" =>
          -- (see 'buffer_read' pulse generation below) 
          in_port <= uart_rx_data_out;
        ----------------------------------------------------------------------
        -- DAQ register control
        when x"02" =>
          in_port <= "0" & reg_out.byte_select & reg_in.valid & reg_in.byte_select;
        ----------------------------------------------------------------------
        -- DAQ register addr (LSB)
        when x"03" =>
          in_port <= reg_address_buffer( 7 downto  0);
        -- DAQ register addr (MSB)
        when x"04" =>
          in_port <= reg_address_buffer(15 downto  8);
        -----------------------------------------------------------------------
        -- DAQ input byte
        when x"05" =>
          case reg_in.byte_select is
            when "000" => in_port <= reg_in.data_buffer( 7 downto  0);
            when "001" => in_port <= reg_in.data_buffer(15 downto  8);
            when "010" => in_port <= reg_in.data_buffer(23 downto 16);
            when "011" => in_port <= reg_in.data_buffer(31 downto 24);
            when "100" => in_port <= reg_in.data_buffer(39 downto 32);
            when "101" => in_port <= reg_in.data_buffer(47 downto 40);
            when "110" => in_port <= reg_in.data_buffer(55 downto 48);
            when "111" => in_port <= reg_in.data_buffer(63 downto 56);
            when others => null;
          end case;
        when others => in_port <= x"00";                       
      end case;      
      -------------------------------------------------------------------------
      -- For UART read
      -- Generate 'buffer_read' pulse following read from port address 01
      if (read_strobe = '1') and (port_id = x"01") then
        read_from_uart_rx <= '1';
      else
        read_from_uart_rx <= '0';
      end if;
      
    end if;
  end process input_ports;

-----------------------------------------------------------------------------
-- Writing interface of the UC to UART and other devices
-----------------------------------------------------------------------------
  output_ports : process(clk, reset)
  begin
    if reset = '1' then
      reg_in.data_read <= '0';
      reg_out.data_write <= '0';    
    elsif clk'event and clk = '1' then
      -- reset register strobe
      reg_in.data_read <= '0';
      reg_out.data_write <= '0';    
      -------------------------------------------------------------------------
      -- Constant write strobe
      if k_write_strobe = '1' then
        case port_id is
          --------------------
          -- Uart status port 
          when x"00" =>
            uart_tx_reset <= out_port(0);
            uart_rx_reset <= out_port(1);
          when others => null;
        end case;
      -------------------------------------------------------------------------
      -- write strobe
      elsif write_strobe = '1' then
        case port_id is
          --------------------
          -- DAQ register control
          when x"02" =>
            -- start read strobe
            if out_port(3) = '1' then
              reg_in.data_read <= '1';
            end if;
            -- start write strobe
            if out_port(7) = '1' then
              reg_out.data_write <= '1';
            end if;
            --update read/write bytes
            reg_out.byte_select <= out_port(6 downto 4);
            reg_in.byte_select <= out_port(2 downto 0);
          --------------------
          -- DAQ register address (LSB)
          when x"03" =>
            reg_address_buffer( 7 downto  0) <= out_port;
          --------------------
          -- DAQ register address (MSB)
          when x"04" =>
            reg_address_buffer(15 downto  8) <= out_port;
          --------------------
          -- DAQ output byte
          when x"05" =>
            case reg_out.byte_select is
              when "000" => reg_out.data_buffer( 7 downto  0) <= out_port;
              when "001" => reg_out.data_buffer(15 downto  8) <= out_port;
              when "010" => reg_out.data_buffer(23 downto 16) <= out_port;
              when "011" => reg_out.data_buffer(31 downto 24) <= out_port;
              when "100" => reg_out.data_buffer(39 downto 32) <= out_port;
              when "101" => reg_out.data_buffer(47 downto 40) <= out_port;
              when "110" => reg_out.data_buffer(55 downto 48) <= out_port;
              when "111" => reg_out.data_buffer(63 downto 56) <= out_port;
              when others => null;
            end case;
          when others => null;
        end case;
      end if;
    end if;
  end process output_ports;

--
-- Update the uart out signals without the one clock delay
-- 
  uart_tx_data_in  <= out_port;
  write_to_uart_tx <= '1' when (write_strobe = '1') and (port_id = x"01")
                      else '0';


end architecture arch;
