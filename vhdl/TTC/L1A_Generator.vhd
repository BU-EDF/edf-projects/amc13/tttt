----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
-- 
-- Create Date:    
-- Design Name: 
-- Module Name:  TTC-TTS control - Behavioral
-- Project Name: TTTT
-- Target Devices: XC6SLX9-TQFP144
-- Tool versions: 13.3 (lin64)
-- Description: 
--
-- Dependencies: 
--
-- Revision:
-- Revision 0.1 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;


-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

use work.TTC.all;
use work.BC_tuning.all;


entity Trigger_Generator is
  
  port (
    lhc_clk            : in  std_logic;   -- lhc clock
    reset              : in  std_logic;     
    
    TTS                : in std_logic_vector(3 downto 0);
    enable_TTS         : in std_logic;
    
    bc_tunes           : in bc_tune;
    bc_start_triggers  : in std_logic_vector(11 downto 0); -- inclusive
    bc_end_triggers    : in std_logic_vector(11 downto 0); -- exclusive

    enable_trigger_rule : in std_logic_vector(3 downto 0);
    
    --trigger sources
    enable_periodic    : in std_logic;
    period             : in unsigned(15 downto 0);

    enable_random      : in std_logic;
    random_timescale   : in unsigned(15 downto 0);

    software_trigger   : in std_logic;

    software_bcn           : in std_logic_vector(11 downto 0);
    software_on_bc_trigger : in std_logic;    
    
    enable_external    : in std_logic;
    external_trigger   : in std_logic;
      
    --final trigger stream
    Trigger_stream_out : out std_logic; -- stream of L1As
    Trigger_monitor         : out L1A_triggers;
    Skipped_Trigger_monitor : out L1A_triggers); 

end Trigger_Generator;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- ARCHITECTURE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

architecture Behavioral of Trigger_Generator is
  -- L1A for this window
  signal enable_triggers : std_logic := '0';
  
  signal periodic_L1A : std_logic := '0';
  signal random_L1A   : std_logic := '0';
  signal software_L1A : std_logic := '0';
  signal external_L1A : std_logic := '0';
  signal software_on_bc_L1A : std_logic := '0';
  
  signal periodic_counter : unsigned(15 downto 0) := x"0000";

  signal raw_random_number : std_logic_vector(31 downto 0) := x"00000000";
  signal random_number : unsigned(15 downto 0) := x"0000";
  signal generator_running : std_logic := '0';
  signal random_counter : unsigned(15 downto 0) := x"0000";
  component urand
    port (
      clk   : in  std_logic;
      rst_n : in  std_logic;
      u     : out std_logic_vector(31 downto 0);
      dv    : out std_logic);
  end component;


  component counter is
    generic (
      roll_over : std_logic;
      end_value   : std_logic_vector;
      start_value : std_logic_vector;
      width     : integer);
    port (
      clk       : in  std_logic;
      reset     : in  std_logic;
      event     : in  std_logic;
      count     : out unsigned(width-1 downto 0);
      at_max    : out std_logic);
  end component counter;

  signal skipped_trigger          : std_logic := '0';
  signal skipped_periodic_trigger : std_logic := '0';
  signal skipped_random_trigger   : std_logic := '0';
  signal skipped_software_trigger : std_logic := '0';
  signal skipped_software_on_bc_trigger : std_logic := '0';
  signal skipped_external_trigger : std_logic := '0';     
  signal skipped_trigger_bad_TTS  : std_logic := '0';
  
  signal external_last : std_logic := '0';

  signal queued_software_trigger : std_logic := '0';
  signal queued_software_bc : std_logic := '0';
  signal software_bc : std_logic_vector(11 downto 0) := x"FFF";
  
  constant TTS_READY : std_logic_vector(3 downto 0) := x"8";
  
  signal Trigger_stream : std_logic := '0';
  type unsigned_array is array (0 to 3) of unsigned(8 downto 0);
  signal L1A_History : unsigned_array := ((others => '0'),(others => '0'),(others => '0'),(others => '0'));
  signal L1A_History_valid : std_logic_vector(3 downto 0) := "0000";
------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- BEGIN
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
begin

  -----------------------------------------------------------------------------
  -- Process the trigger types and take the or of them
  -----------------------------------------------------------------------------
  
  trigger_period_enable: process (lhc_clk) is
  begin  -- process trigger_period_enable
    if lhc_clk'event and lhc_clk = '1' then  -- rising clock edge
      --check if bunch count is greater than or equal to the start bc time
      -- and less than the end time
      enable_triggers <= '0';
      if ( bc_tunes.l1a_allowed_range >= unsigned(bc_start_triggers)) and
         ( bc_tunes.l1a_allowed_range <  unsigned(bc_end_triggers  )) then
        enable_triggers <= '1';
      end if;
    end if;
  end process trigger_period_enable;
  
  -------------------
  -- Periodic trigger
  periodic_trigger: process (lhc_clk)
  begin  -- process periodic_trigger
    if lhc_clk'event and lhc_clk = '1' then  -- rising clock edge
      periodic_L1A <= '0';
      if enable_periodic = '1' then
        periodic_counter <= periodic_counter + 1;
        if periodic_counter = period then
          periodic_L1A <= '1' and enable_triggers;
          periodic_counter <= x"0000";
        end if;
      else
        periodic_counter <= x"0000";
      end if;
    end if;
  end process periodic_trigger;

  -------------------
  -- Random trigger
  urand_1: urand
    port map (
      clk   => lhc_clk,
      rst_n => enable_random,
      u     => raw_random_number,       --std_logic_vector(random_number),
      dv    => generator_running);

  --find random numbers in our range
  filter_random_number: process (lhc_clk)
  begin  -- process filter_random_number
    if lhc_clk'event and lhc_clk = '1' then  -- rising clock edge
      if generator_running = '1' then
        if unsigned(raw_random_number(15 downto 0)) <= random_timescale then
          random_number <= unsigned(raw_random_number(15 downto 0));
        end if;
      end if;
    end if;  
  end process filter_random_number;

  --generate random number
  random_trigger: process (lhc_clk)
  begin  -- process random_trigger
    if lhc_clk'event and lhc_clk = '1' then  -- rising clock edge
      random_L1A <= '0';
      if enable_random = '1' then
        random_counter <= random_counter - 1;
        if random_counter = 0 then
          random_L1A <= '1'  and enable_triggers;
          random_counter <= random_number;
        end if;
      else
        random_counter <= (others => '0');
      end if;        
    end if;
  end process random_trigger;


  -------------------
  -- software trigger
  process_software_trigger: process (lhc_clk)
  begin  -- process oftware_trigger
    if lhc_clk'event and lhc_clk = '1' then  -- rising clock edge
      software_L1A <= '0';
      if software_trigger = '1' then
        if enable_triggers = '1' then
          software_L1A <= '1';
        else
          queued_software_trigger <= '1';
        end if;
      elsif queued_software_trigger = '1' then
        if enable_triggers = '1' then
          software_L1A <= '1';
          queued_software_trigger <= '0';
        end if;
      end if;          
    end if;
  end process process_software_trigger;

  -------------------
  -- software trigger on a bunch crossing
  process_software_on_bc_trigger: process (lhc_clk)
  begin  -- process oftware_trigger
    if lhc_clk'event and lhc_clk = '1' then  -- rising clock edge      
      software_on_bc_L1A <= '0';
      -- send a queued L1a on the specified bunch crossing
      if queued_software_bc = '1' then
        if (bc_tunes.l1a_software_trigger = unsigned(software_bc)) then
          -- reset our queued status whenever the bunch crossing is met
          queued_software_bc <= '0';
          -- send an L1a if we are at the correct bunch crossing and are
          -- allowed to
          if enable_triggers = '1' then 
            software_on_bc_L1A <= '1';
          end if;            
        end if;
      --catch that we want to queue a new trigger
      elsif software_on_bc_trigger = '1' then
        software_bc <= software_bcn;
        queued_software_bc <= '1';
      end if;

    end if;
  end process process_software_on_bc_trigger;


  
  -------------------
  -- external trigger
  process_external_trigger: process (lhc_clk)
  begin  -- process oftware_trigger
    if lhc_clk'event and lhc_clk = '1' then  -- rising clock edge
      external_last <= external_trigger;
      external_L1A <= '0';
      if enable_external = '1' and external_trigger = '1' and external_last = '0' then
        external_L1A <= '1'  and enable_triggers;
      end if;
    end if;
  end process process_external_trigger;
 
  -----------------------------------------------------------------------------
  -- apply the trigger rules
  -----------------------------------------------------------------------------
  Trigger_stream_out <= Trigger_stream;
  Trigger_rules: process (lhc_clk)
  begin  -- process Trigger_rules
    if lhc_clk'event and lhc_clk = '1' then  -- rising clock edge
      -- Set the output L1A to zero
      Trigger_stream <= '0';

      -- zero skipped trigger signals
      skipped_trigger          <= '0';
      skipped_periodic_trigger <= '0';
      skipped_random_trigger   <= '0';
      skipped_software_trigger <= '0';
      skipped_software_on_bc_trigger <= '0';
      skipped_external_trigger <= '0';     
      skipped_trigger_bad_TTS  <= '0';
      
      if reset = '1' then
        L1A_history <= ((others => '0'),(others => '0'),(others => '0'),(others => '0'));
        
      else
        --Check if we have a valid trigger from any enabled source
        if (enable_periodic       = '1' and periodic_L1A       = '1') or 
           (enable_random         = '1' and random_L1A         = '1') or 
           (                                software_L1A       = '1') or
           (                                software_on_bc_L1A = '1') or 
           (enable_external       = '1' and external_L1A       = '1') then
          --new trigger
          
          ---------------------
          --Check trigger rules
          if
            --0) TTS state
            (enable_TTS = '1' and TTS /= TTS_READY) or
            --i)  No more than 1 Level 1 Accept per 75 ns (minimum 2 bx between L1A)
            (enable_trigger_rule(0) = '1' and L1A_History_valid(0) = '1' and L1A_History(0) < 2) or       
            --ii) No more than 2 Level 1 Accepts per 625 ns (25 bx)
            (enable_trigger_rule(1) = '1' and L1A_History_valid(1 downto 0) = "11" and L1A_History(1) < 25 ) or 
            --iii) No more than 3 Level 1 Accepts per 2.5 us (100 bx)
            (enable_trigger_rule(2) = '1' and L1A_History_valid(2 downto 0) = "111" and L1A_History(2) < 100) or 
            --iv) No more than 4 Level 1 Accepts per 6 us (240 bx)
            (enable_trigger_rule(3) = '1' and L1A_History_valid(3 downto 0) = "1111" and L1A_History(3) < 240) then

            ---------------------
            --Failed
            for iHistory in 0 to 3 loop
              --update historic triggers
              if L1A_History_valid(iHistory) = '1' then
                if L1A_History(iHistory) > 239 then
                  L1A_History_valid(iHistory) <= '0';
                else
                  L1A_History(iHistory) <= L1A_History(iHistory) + 1;
                end if; 
              end if;           
            end loop;  -- iHistory

            skipped_trigger <= '1';
            if (enable_TTS = '1' and TTS /= TTS_READY) then 
              skipped_trigger_bad_TTS  <= '1';
            end if;
            if ((enable_periodic = '1' and periodic_L1A = '1')) then
              skipped_periodic_trigger <= '1';
            end if;
            if ((enable_random   = '1' and random_L1A   = '1')) then
              skipped_random_trigger <= '1';
            end if;
            if software_L1A = '1' then
              skipped_software_trigger <= '1';
            end if;
            if software_on_bc_L1A = '1' then
              skipped_software_on_bc_trigger <= '1';
            end if;
            if ((enable_external = '1' and external_L1A = '1')) then
              skipped_external_trigger <= '1';
            end if;
          else
            ---------------------
            --Passed
            Trigger_stream <= '1';
            --Update history
            for iHistory in 1 to 3 loop
              L1A_History_valid(iHistory) <= L1A_History_valid(iHistory-1);
              L1A_History(iHistory) <= L1A_History(iHistory-1) + 1;
            end loop;  -- iHistory
            L1A_History_valid(0) <= '1';
            L1A_History(0) <= TO_UNSIGNED(0,9);
          end if;          
        else          
          --no new event, 
          -- update history
          for iHistory in 0 to 3 loop
            --update historic triggers
            if L1A_History_valid(iHistory) = '1' then
              if L1A_History(iHistory) > 239 then
                L1A_History_valid(iHistory) <= '0';
              else
                L1A_History(iHistory) <= L1A_History(iHistory) + 1;
              end if;
            end if;
          end loop;  -- iHistory
        end if;
      end if;         
    end if;
  end process Trigger_rules;

  -----------------------------------------------------------------------------
  -- Counters
  -----------------------------------------------------------------------------
  -- overall triggers
  counter_trigger: entity work.counter
    generic map (
      roll_over   => '1',
      end_value   => x"FFFFFFFF",
      start_value => x"00000000",
      width       => 32)
    port map (
      clk    => lhc_clk,
      reset  => reset,
      event  => trigger_stream,
      count  => Trigger_monitor.total,
      at_max => open);
  counter_skipped_trigger: entity work.counter
    generic map (
      roll_over   => '1',
      end_value   => x"FFFFFFFF",
      start_value => x"00000000",
      width       => 32)
    port map (
      clk    => lhc_clk,
      reset  => reset,
      event  => skipped_trigger,
      count  => Skipped_Trigger_monitor.total,
      at_max => open);
  -- periodic triggers
  counter_periodic_trigger: entity work.counter
    generic map (
      roll_over   => '1',
      end_value   => x"FFFFFFFF",
      start_value => x"00000000",
      width       => 32)
    port map (
      clk    => lhc_clk,
      reset  => reset,
      event  => periodic_L1A,
      count  => Trigger_monitor.periodic,
      at_max => open);
  counter_skipped_periodic_trigger: entity work.counter
    generic map (
      roll_over   => '1',
      end_value   => x"FFFFFFFF",
      start_value => x"00000000",
      width       => 32)
    port map (
      clk    => lhc_clk,
      reset  => reset,
      event  => skipped_periodic_trigger,
      count  => Skipped_Trigger_monitor.periodic,
      at_max => open);
  -- random triggers
  counter_random_trigger: entity work.counter
    generic map (
      roll_over   => '1',
      end_value   => x"FFFFFFFF",
      start_value => x"00000000",
      width       => 32)
    port map (
      clk    => lhc_clk,
      reset  => reset,
      event  => random_L1A,
      count  => Trigger_monitor.random,
      at_max => open);
  counter_skipped_random_trigger: entity work.counter
    generic map (
      roll_over   => '1',
      end_value   => x"FFFFFFFF",
      start_value => x"00000000",
      width       => 32)
    port map (
      clk    => lhc_clk,
      reset  => reset,
      event  => skipped_random_trigger,
      count  => Skipped_Trigger_monitor.random,
      at_max => open);
  -- overall software_triggers
  counter_software_trigger: entity work.counter
    generic map (
      roll_over   => '1',
      end_value   => x"FFFFFFFF",
      start_value => x"00000000",
      width       => 32)
    port map (
      clk    => lhc_clk,
      reset  => reset,
      event  => software_L1A,
      count  => Trigger_monitor.software,
      at_max => open);
  counter_skipped_software_trigger: entity work.counter
    generic map (
      roll_over   => '1',
      end_value   => x"FFFFFFFF",
      start_value => x"00000000",
      width       => 32)
    port map (
      clk    => lhc_clk,
      reset  => reset,
      event  => skipped_software_trigger,
      count  => Skipped_Trigger_monitor.software,
      at_max => open);
  -- overall software_on_bc_triggers
  counter_software_on_bc_trigger: entity work.counter
    generic map (
      roll_over   => '1',
      end_value   => x"FFFFFFFF",
      start_value => x"00000000",
      width       => 32)
    port map (
      clk    => lhc_clk,
      reset  => reset,
      event  => software_on_bc_L1A,
      count  => Trigger_monitor.software_on_bc,
      at_max => open);
  counter_skipped_software_on_bc_trigger: entity work.counter
    generic map (
      roll_over   => '1',
      end_value   => x"FFFFFFFF",
      start_value => x"00000000",
      width       => 32)
    port map (
      clk    => lhc_clk,
      reset  => reset,
      event  => skipped_software_on_bc_trigger,
      count  => Skipped_Trigger_monitor.software_on_bc,
      at_max => open);

  -- overall external_triggers
  counter_external_trigger: entity work.counter
    generic map (
      roll_over   => '1',
      end_value   => x"FFFFFFFF",
      start_value => x"00000000",
      width       => 32)
    port map (
      clk    => lhc_clk,
      reset  => reset,
      event  => external_L1A,
      count  => Trigger_Monitor.external,
      at_max => open);
  counter_skipped_external_trigger: entity work.counter
    generic map (
      roll_over   => '1',
      end_value   => x"FFFFFFFF",
      start_value => x"00000000",
      width       => 32)
    port map (
      clk    => lhc_clk,
      reset  => reset,
      event  => skipped_external_trigger,
      count  => Skipped_Trigger_monitor.external,
      at_max => open);
  --skipped due to TTS state
  counter_skipped_TTS: entity work.counter
    generic map (
      roll_over   => '1',
      end_value   => x"FFFFFFFF",
      start_value => x"00000000",
      width       => 32)
    port map (
      clk    => lhc_clk,
      reset  => reset,
      event  => skipped_trigger_bad_TTS,
      count  => Skipped_Trigger_monitor.tts,
      at_max => open);
  
end Behavioral;
