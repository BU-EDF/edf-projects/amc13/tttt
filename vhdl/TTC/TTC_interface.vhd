----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
-- 
-- Create Date:    
-- Design Name: 
-- Module Name: TTC - Behavioral
-- Project Name: TTTT
-- Target Devices: XC6SLX9-TQFP144
-- Tool versions: 13.3 (lin64)
-- Description: 
--
-- Dependencies: 
--
-- Revision:
-- Revision 0.1 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
use work.TTC_chB.all;
use work.TTC.all;
use work.BC_tuning.all;
-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity TTC_Interface is
  
  port (
    clk_lhc         : in std_logic;
    clk_lhc_4x      : in std_logic;

    reset        : in std_logic;                        -- logic reset
    -- TTS input
    TTS          : in std_logic_vector(3 downto 0);
    --Final TTC output stream
    SFP_Tx_P : out std_logic;
    SFP_Tx_N : out std_logic;

    external_trigger : in std_logic;
    Monitor : out TTC_Monitor;
    Control : in  TTC_Control
    
    ); 

end TTC_Interface;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- ARCHITECTURE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
architecture Behavioral of TTC_Interface is
  -----------------------------------------------------------------------------
  --signals
  -----------------------------------------------------------------------------
  signal en_clk_lhc : std_logic := '0';
  signal en_clk_lhc_2x : std_logic := '0';

  signal TTC_stream : std_logic := '0';     
  signal TTC_out : std_logic := '0';             -- single TTC output channel for all four SFPs

  signal coordinated_chB_L1a : std_logic := '0';  -- trigger to coordinate chB and l1A messages
  signal queued_chB_L1A_trigger : std_logic := '0'; -- signal to wait for next
                                                    -- orbit to start
                                                    -- coordinated triggers

  signal chB_wr : std_logic := '0';     -- wr for chB message queue     
  signal chB_stream : std_logic := '0';          -- stream of bits for channel B
  signal chB_overflow : std_logic := '0';
  signal BC0 : std_logic := '0';
  signal BC_tunes : bc_tune;
  signal orbit_number : unsigned(15 downto 0) := x"0000";

  signal software_on_bc_trigger : std_logic := '0';  -- name says it all
  signal L1A_stream : std_logic := '0';
  
  signal TTC_FIFO_wr : std_logic := '0';
  signal TTC_overflow : std_logic := '0';
  signal TTC_underflow : std_logic := '0';
  signal TTC_FIFO_full : std_logic := '0';
  signal TTC_FIFO_rd : std_logic := '0';
  signal TTC_FIFO_data : std_logic_vector(0 downto 0) := "0";
  signal TTC_FIFO_valid : std_logic := '0';
  signal TTC_FIFO_empty : std_logic := '0';
  signal TTC_DATA_in : std_logic_vector(1 downto 0) := "00";  
  signal TTC_stream_valid : std_logic := '0';

  signal bpmc_symbol : std_logic_vector(3 downto 0);
  
  component chB_encoder is
    port (
      clk_lhc      : in  std_logic;
      reset        : in  std_logic;
      enable_BCX_reset   : in  std_logic;
      chB_valid    : in  std_logic;
      chB          : in  Message;
      chB_full     : out std_logic;
      chB_overflow : out std_logic;
      orbit_number : out unsigned(15 downto 0);
      bc_tunes     : out bc_tune;
      bc_rollovers : in bc_tune_rollover;
      BC0          : out std_logic;
      stream       : out std_logic := '1');
  end component chB_encoder;

  component Trigger_Generator is
    port (
      lhc_clk            : in  std_logic;
      reset              : in  std_logic;
      TTS                : in std_logic_vector(3 downto 0);
      enable_TTS         : in std_logic;
      bc_tunes           : in bc_tune;
      bc_start_triggers  : in std_logic_vector(11 downto 0); -- inclusive
      bc_end_triggers    : in std_logic_vector(11 downto 0); -- exclusive
      enable_trigger_rule : in std_logic_vector(3 downto 0);
      enable_periodic    : in  std_logic;
      period             : in  unsigned(15 downto 0);
      enable_random      : in  std_logic;
      random_timescale   : in  unsigned(15 downto 0);
      software_trigger   : in  std_logic;
      software_bcn       : in std_logic_vector(11 downto 0);
      software_on_bc     : in std_logic;    
      enable_external    : in  std_logic;
      external_trigger   : in  std_logic;
      Trigger_stream_out : out std_logic;
      Trigger_monitor    : out L1A_triggers;
      Skipped_Trigger_monitor : out L1A_triggers);
  end component Trigger_Generator;
  
  component TTC_Data_FIFO is
    port (
      rst    : IN  STD_LOGIC;
      wr_clk : IN  STD_LOGIC;
      rd_clk : IN  STD_LOGIC;
      din    : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
      wr_en  : IN  STD_LOGIC;
      rd_en  : IN  STD_LOGIC;
      dout   : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      full   : OUT STD_LOGIC;
      empty  : OUT STD_LOGIC;
      valid  : OUT STD_LOGIC);
  end component TTC_Data_FIFO;
  
  component bmc_encoder
    port (
      data         : in  std_logic;
      data_valid   : in  std_logic;
      clk_2x       : in  std_logic;
      stream       : out std_logic);
  end component;
  
begin

  -----------------------------------------------------------------------------
  -- Coordinate a chb message with a L1A
  -----------------------------------------------------------------------------
  chb_l1a_coordinator: process (clk_lhc) is
  begin  -- process chb_l1a_coordinator
    if clk_lhc'event and clk_lhc = '1' then  -- rising clock edge
      coordinated_chB_L1a <= '0';
      if queued_chB_L1A_trigger = '1' then
        if bc_tunes.normal = x"DE9" then
          coordinated_chB_L1a <= '1';
          queued_chB_L1A_trigger <= '0';
        end if;
      elsif Control.chB_L1A_trigger = '1' then
        queued_chB_L1A_trigger <= '1';                
      end if;
    end if;
  end process chb_l1a_coordinator;

  chB_wr <= Control.chB_wr or coordinated_chB_L1a;
  software_on_bc_trigger <= Control.software_on_bc_trigger or coordinated_chB_L1a;
  -----------------------------------------------------------------------------
  -- Generate channel B sequence
  -----------------------------------------------------------------------------

  chB_encoder_1: entity work.chB_encoder
    port map (
      clk_lhc      => clk_lhc,
      reset        => reset,
      enable_BCX_reset  => Control.enable_BCX_reset,
      chB_valid    => chB_wr,
      chB          => Control.chB_message,
      chB_full     => Monitor.chB_full,
      chB_overflow => chB_overflow,
      orbit_number => orbit_number,
      bc_tunes     => bc_tunes,
      bc_rollovers => Control.bc_rollovers,
      BC0          => BC0,
      stream       => chB_stream);
  Monitor.BX_counter <= bc_tunes.normal;
  Monitor.orbit_number <= orbit_number;
  -----------------------------------------------------------------------------
  -- Generate a stream of L1As that follow the trigger rules  
  -----------------------------------------------------------------------------  
  Trigger_Generator_1: entity work.Trigger_Generator
    port map (
      lhc_clk            => clk_lhc,
      reset              => reset,
      TTS                => TTS,
      enable_TTS         => Control.enable_TTS,
      bc_tunes           => bc_tunes,
      bc_start_triggers  => Control.bc_start_triggers,
      bc_end_triggers    => Control.bc_end_triggers,
      enable_trigger_rule => Control.enable_trigger_rule,
      enable_periodic    => Control.enable_periodic,
      period             => Control.period,
      enable_random      => Control.enable_random,
      random_timescale   => Control.random_timescale,
      software_trigger   => Control.software_trigger,
      software_bcn       => Control.software_bcn,
      software_on_bc_trigger => software_on_bc_trigger,
      enable_external    => Control.enable_external,
      external_trigger   => external_trigger,
      Trigger_stream_out => L1A_stream,
      Trigger_monitor    => Monitor.Triggers,
      Skipped_Trigger_monitor => Monitor.Skipped_Triggers);

  
  -----------------------------------------------------------------------------
  -- Serialize the A(L1A) and B channels into TTC_datastream via a FIFO
  -----------------------------------------------------------------------------

  -- write into FIFO at clk_LHC
  TTC_FIFO_write: process (clk_lhc,reset) is
  begin  -- process TTC_FIFO_write
    if reset = '1' then               -- asynchronous reset (active high)
      TTC_FIFO_wr <= '0';
    elsif clk_lhc'event and clk_lhc = '1' then  -- rising clock edge
      -- write it fifo if there is room
      TTC_FIFO_wr <= not TTC_FIFO_full;
      TTC_DATA_in <= L1A_stream & chb_stream;
      -- note when it is full
      TTC_overflow <= '0';
      if TTC_FIFO_full = '1' then
        TTC_overflow <= '1';
      end if;
    end if;
  end process TTC_FIFO_write;

  counter_1: entity work.counter
    generic map (
      roll_over   => '0',
      end_value   => x"FF",
      start_value => x"00",
      width       => 8)
    port map (
      clk    => clk_lhc,
      reset  => reset,
      event  => TTC_overflow,
      count  => Monitor.TTC_overflow_count,
      at_max => open);
  
  TTC_Data_FIFO_1: entity work.TTC_Data_FIFO
    port map (
      rst    => reset,
      wr_clk => clk_lhc,
      rd_clk => clk_lhc_4x,
      din    => TTC_DATA_in,
      wr_en  => TTC_FIFO_wr,
      rd_en  => TTC_FIFO_rd,
      dout   => TTC_FIFO_data,
      full   => TTC_FIFO_full,
      empty  => TTC_FIFO_empty,
      valid  => TTC_FIFO_valid);

  -- read from FIFO at half the clk_lhc_4x clock
  TTC_FIFO_read: process (clk_lhc_4x, reset) is
  begin  -- process TTC_FIFO_read
    if reset = '1' then                 -- asynchronous reset (active high)
      
    elsif clk_lhc_4x'event and clk_lhc_4x = '1' then  -- rising clock edge
      -- process data coming out of the FIFO
      TTC_stream_valid <= '0';
      if TTC_FIFO_valid = '1' then
        TTC_stream <= TTC_FIFO_data(0);
        TTC_stream_valid <= '1';
      end if;

      -- process read strobe for the FIFO
      TTC_underflow <= '0';
      TTC_FIFO_rd <= '0';
      if en_clk_lhc_2x = '1' then
        -- read if not empty
        if TTC_FIFO_empty = '0' then
          TTC_FIFO_rd <= '1';
        else
          TTC_underflow <= '1';
        end if;
      end if;      
    end if;
  end process TTC_FIFO_read;

  counter_2: entity work.counter
    generic map (
      roll_over   => '0',
      end_value   => x"FF",
      start_value => x"00",
      width       => 8)
    port map (
      clk    => clk_lhc_4x,
      reset  => reset,
      event  => TTC_underflow,
      count  => Monitor.TTC_underflow_count,
      at_max => open);
  
  -----------------------------------------------------------------------------
  -- Generate clock enables for 2x lhc clocks based off of lhc clock
  -----------------------------------------------------------------------------
  lhc_clock_enable_generator: process (clk_lhc_4x, reset) is
  begin  -- process lhc_clock_enable_generator
    if reset = '1' then                 -- asynchronous reset (active high)
      en_clk_lhc_2x <= '0';
    elsif clk_lhc_4x'event and clk_lhc_4x = '1' then  -- rising clock edge
      en_clk_lhc_2x <= not en_clk_lhc_2x;      
    end if;
  end process lhc_clock_enable_generator;
  
  -----------------------------------------------------------------------------
  -- Generate biphase-mark coding for TTC_datastream
  -----------------------------------------------------------------------------
  Biphase_mark_encoding: bmc_encoder
    port map (
      data         => TTC_stream,
      data_valid   => TTC_stream_valid,
      clk_2x       => clk_lhc_4x,       
      stream       => TTC_out);

  bfmc_histo: for iSymbol in 0 to 3 generate
    bpmc_symbol(iSymbol) <= '1' when to_integer(unsigned(TTC_data_in)) = iSymbol else '0';
    counter_3: entity work.counter
      generic map (
        roll_over   => '0',
        end_value   => x"F",
        start_value => x"0",
        width       => 4)
      port map (
        clk    => clk_lhc,
        reset  => reset,
        event  => bpmc_symbol(iSymbol),
        count  => Monitor.bpmc_histo(iSymbol),
        at_max => open);
  end generate bfmc_histo;
  
  -----------------------------------------------------------------------------
  -- Connect the TTC out signal to each of the TTC SFP TXs.
  -----------------------------------------------------------------------------
  TTC_OBFUDS : OBUFDS
    generic map (
      IOSTANDARD => "LVDS_33")
    port map (
      I  => TTC_out,
      O  => SFP_Tx_P,
      OB => SFP_TX_N);
  

end Behavioral;
