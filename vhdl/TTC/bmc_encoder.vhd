----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:30:53 06/06/2012 
-- Design Name: 
-- Module Name:    bmc_encoder - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
-- use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity bmc_encoder is

  port (
    -- Inputs
    data       : in std_logic;  -- Serial data input
    data_valid : in std_logic;
    clk_2x     : in std_logic;  -- clock that is 2 times the incoming bitrate
    -- Outputs
    stream    : out std_logic  -- The output encoded data.
    );

end bmc_encoder;

architecture Behavioral of bmc_encoder is
  signal current_bit : std_logic := '0';
  signal stream_buffer : std_logic := '0';
begin
  stream <= stream_buffer;
  -- Encode the incoming datastream, which should be clocked at clk160/2 which will be the
  -- data rate. This module requires a clock that is 2x the desired datarate clock.
  encode_data : process(clk_2x)
  begin
    if rising_edge(clk_2x) then
      
      if data_valid = '1' then
        -- save new bit
        current_bit <= data;
        -- cause transition on this boundary
        stream_buffer <= not stream_buffer;
      else
        -- cause another transition if we want to encode a '1'
        if current_bit = '1' then
          stream_buffer <= not stream_buffer;
        end if;
      end if;      
    end if;
  end process;  -- encode_data
end Behavioral;
