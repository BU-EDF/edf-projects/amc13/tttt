library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

entity TTS_Monitor is
  
  port (
    clk_LHC   : in std_logic;
    reset     : in std_logic;
    TTS_in    : in std_logic_vector(7 downto 0);
    TTS_state_change : out std_logic_vector(63 downto 0);
    rd        : in std_logic;
    size      : out integer;
    );

end entity TTS_Monitor;
