library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

entity capture_DDR is
  
  port (
    clk_DDR        : in  std_logic;
    clk_DDR_180     : in  std_logic;
    stream_in_p    : in  std_logic;
    stream_in_n    : in  std_logic;
    stream_out     : out std_logic_vector(1 downto 0)
    );
end entity capture_DDR;

architecture behavioral of capture_DDR is

  signal input_stream : std_logic := '0';
  signal output_stream : std_logic_vector(1 downto 0) := "00";
  
begin
    ---------------------------------------
  --IBUFDS to buffer the incoming stream
  ---------------------------------------
  TCDS_input_buffer : IBUFDS
    generic map (
      DIFF_TERM  => TRUE,             -- Differential termination
      IOSTANDARD => "LVDS_25")
    port map (
      I          => stream_in_p,
      IB         => stream_in_n,
      O          => input_stream);


  Base_2_deserializer: IDDR2
    generic map (
      DDR_ALIGNMENT => "C0",
      INIT_Q0       => '0',
      INIT_Q1       => '0',
      SRTYPE        => "ASYNC")
    port map (
      D  => input_stream,
      C0 => clk_DDR,
      C1 => clk_DDR_180,
      CE => '1',
      R  => '0',
      Q0 => output_stream(0),
      Q1 => output_stream(1)
      );

  stream_buffer: process (clk_DDR) is
  begin  -- process stream_buffer
    if clk_DDR'event and clk_DDR = '1' then  -- rising clock edge
      stream_out <= output_stream;
    end if;
  end process stream_buffer;
  
end architecture behavioral;
