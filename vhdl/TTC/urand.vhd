-- Random number generator
-- Generate 32-bit random numbers according to:
--     U = 1664525L*U(0) + 1013904223L;    (modulo 2**32)
--


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;
--
library unisim;
use unisim.vcomponents.all;

entity urand is

  port (
    clk   : in  std_logic;
    rst_n : in  std_logic;
    u     : out std_logic_vector(31 downto 0);
    dv    : out std_logic
    );

end urand;

architecture arch of urand is

  component mult32x32
    port (
      clk : in  std_logic;
      a   : in  std_logic_vector(31 downto 0);
      b   : in  std_logic_vector(31 downto 0);
      p   : out std_logic_vector(31 downto 0)
      );
  end component;

  constant k1 : std_logic_vector(31 downto 0) := X"0019660d";
  constant k2 : std_logic_vector(31 downto 0) := X"3c6ef35f";

  signal p     : std_logic_vector(31 downto 0) := X"00000000";
  signal tu    : std_logic_vector(31 downto 0) := X"00000000";
  signal seed  : std_logic_vector(31 downto 0) := X"00000000";
  signal mmux  : std_logic_vector(31 downto 0) := X"00000000";

  signal r_seed   : std_logic := '0';
  signal tdv, vdv : std_logic := '0';

  signal reseed : std_logic := '1';

begin  -- arch

  process (clk, rst_n)
  begin  -- process
    if rst_n = '0' then                 -- asynchronous reset (active low)
      seed   <= (others => '0');
      tdv    <= '0';
      reseed <= '1';
    elsif clk'event and clk = '1' then  -- rising clock edge
      r_seed <= reseed;
      tu     <= p + k2;
      tdv    <= not tdv;
      if reseed = '1' then
        reseed <= '0';
      end if;
    end if;

  end process;

  mmux <= tu when r_seed = '0'
          else seed;

  u   <= tu;
  vdv <= tdv;
  dv  <= vdv;

  
  m1 : mult32x32
    port map (
      clk => clk,
      a   => mmux,
      b   => k1,
      p   => p);

end arch;
