restart
source vhdl/sim/UART_helper.tcl

#set initial conditions for UART
isim force add {/top/UART_rx} 1

#set clock on /top/clk
isim force add {/top/clk_local_in} 1 -radix bin -value 0 -radix bin -time 12.5 ns -repeat 25 ns


#let everything start up
run 2 ms

sendUART_str rw 115200 /top/UART_rx;
#send space (0x20)
sendUART_hex 20 115200 /top/UART_rx;
sendUART_str 32 115200 /top/UART_rx; #reg
#send space (0x20)
sendUART_hex 20 115200 /top/UART_rx;
#sendUART_str 0123456789abcdef 115200 /top/UART_rx; #L1a bc number
sendUART_str 00000DE90DE90DEB 115200 /top/UART_rx; #L1a bc number
#send CR (0x0D)
sendUART_hex 0D 115200 /top/UART_rx;


# take some triggers
sendUART_str l1a_adv 115200 /top/UART_rx;

#send space (0x20)
sendUART_hex 20 115200 /top/UART_rx;
sendUART_str d82 115200 /top/UART_rx; #L1a bc number

#send space (0x20)
sendUART_hex 20 115200 /top/UART_rx;
sendUART_str 400 115200 /top/UART_rx; #chb message bc number

#send space (0x20)
sendUART_hex 20 115200 /top/UART_rx;
sendUART_str 40 115200 /top/UART_rx; #message data

#send CR (0x0D)
sendUART_hex 0D 115200 /top/UART_rx;


run 2 ms
