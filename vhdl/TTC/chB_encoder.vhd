----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
-- 
-- Create Date:    
-- Design Name: 
-- Module Name: TTC ChB - Behavioral
-- Project Name: TTTT
-- Target Devices: XC6SLX9-TQFP144
-- Tool versions: 13.3 (lin64)
-- Description: 
--
-- Dependencies: 
--
-- Revision:
-- Revision 0.1 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
use work.TTC_chB.all;
use work.BC_tuning.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;



entity chB_encoder is
  port (
    clk_lhc : in std_logic;
    reset : in std_logic; -- sync
    enable_BCX_reset   : in  std_logic;
    
    chB_valid : in std_logic;
    chB : in Message;
    chB_full : out std_logic;           -- No room to add another message
    chB_overflow : out std_logic;

    orbit_number : out unsigned(15 downto 0);
    bc_tunes          : out bc_tune;    -- bunch number and offset bunch
                                        -- numbers for use elsewhere
    bc_rollovers : in bc_tune_rollover; -- rollover points for 

    BC0        : out std_logic;    
    
    stream : out std_logic := '1'
    );
end chB_encoder;
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- ARCHITECTURE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
architecture Behavioral of chB_encoder is


  constant bunch_BC0_start : unsigned(11 downto 0) := x"DD7";--rollover - 16 -4    
  constant bunch_start : unsigned(11 downto 0) := x"000";
  constant bunch_can_send_any_message : unsigned(11 downto 0) := x"DAD";--x"DC1"; 
  constant bunch_can_send_broadcast_message_only: unsigned(11 downto 0) := x"DC7"; --x"DDB"

  signal bunches : BC_tune := TUNE_START;  
  signal bunch : unsigned(11 downto 0) := X"000";
  signal bunch_bmesg : unsigned(11 downto 0) := X"000";
  signal bunch_amesg : unsigned(11 downto 0) := X"000";

  constant orbit_start : unsigned(15 downto 0) := x"0000";
  signal orbit : unsigned(15 downto 0) := orbit_start;
  
  constant sending_idle : unsigned(5 downto 0) := "000000";
  constant sending_end : unsigned(5 downto 0) := "000001";
  constant sending_broadcast_start : unsigned(5 downto 0) := "010000"; -- 16
  constant sending_addressed_start : unsigned(5 downto 0) := "101010"; -- 42
  signal sending_counter : unsigned(5 downto 0) := "000000";
  
  signal chB_we_check : std_logic := '0';
  signal pending_message_valid : std_logic := '0';
  constant BLANK_MESSAGE : Message := (data => X"00000000",bc_time=> x"000",others =>'0');
  signal pending_message : Message := blank_message;
  signal sending_message_valid : std_logic := '0';
  signal sending_message : Message := blank_message;
  signal pending_message_overflow : std_logic := '0';
  
begin  -- Behavioral

  -----------------------------------------------------------------------------
  -- Bunch counter
  -----------------------------------------------------------------------------
  bc_tunes <= bunches;
  orbit_number <= orbit;
  bunch <= bunches.normal;
  
  bunch_counter: process (clk_lhc)
  begin  -- process bunch
    if clk_lhc'event and clk_lhc = '1' then
      BC0 <= '0';
      if reset = '1' then
        bunches.normal <= BUNCH_START;        
        orbit <= orbit_start;

        bunches.l1a_allowed_range <= BUNCH_START;
        bunches.l1a_software_trigger <= BUNCH_START;
      else
        -- compute actual bunch crossing number
        if bunches.normal = bc_rollovers.normal then
          bunches.normal <= BUNCH_START;
          orbit <= orbit + x"001";
          if enable_BCX_reset = '1' then
            BC0 <= '1';
          end if;
        else
          bunches.normal <= bunches.normal + x"001";
        end if;

        -- compute allowed range bunch crossing number
        if bunches.normal = bc_rollovers.l1a_allowed_range then
          bunches.l1a_allowed_range <= BUNCH_START;
        else
          bunches.l1a_allowed_range <= bunches.l1a_allowed_range + x"001";
        end if;

        -- compute software trigger bunch crossing number
        if bunches.normal = bc_rollovers.l1a_software_trigger then
          bunches.l1a_software_trigger <= BUNCH_START;
        else
          bunches.l1a_software_trigger <= bunches.l1a_software_trigger + x"001";
        end if;

        -- compute bmesg bunch number
        if bunches.normal = (bc_rollovers.normal - sending_broadcast_start -3) then
          bunch_bmesg <= BUNCH_START;
        else
          bunch_bmesg <= bunch_bmesg + x"001";
        end if;

        -- compute amesg bunch number
        if bunches.normal = (bc_rollovers.normal - sending_addressed_start -3) then
          bunch_amesg <= BUNCH_START;
        else
          bunch_amesg <= bunch_bmesg + x"001";
        end if;

        
      end if;
    end if;
  end process bunch_counter;


  -------------------------------------------------------------------------
  --Inform the outside world if we have room in our pending message queue
  -------------------------------------------------------------------------
  chB_full <= pending_message_valid;
  

  -----------------------------------------------------------------------------
  -- Message processor
  -- Move incoming messages to the pending queue
  -- Move pending messages to the sending queue
  -- Guarantee BC0s get send out on queue
  -----------------------------------------------------------------------------

  mproc: process (clk_lhc)
  begin  -- process mproc
    if clk_lhc'event and clk_lhc = '1' then
      
      chB_overflow <= '0';
      
      if reset = '1' then
        sending_counter <= sending_idle;
        pending_message <= BLANK_MESSAGE;
        sending_message <= BLANK_MESSAGE;

      else
        -------------------------------------------------------------------------
        --load a new message from outside to pending if we don't have a pending one
        -------------------------------------------------------------------------
        pending_message_overflow <= '0';
        if chB_valid = '1' then
          if pending_message_valid = '0' then
            pending_message <= chB;
            pending_message_valid <= '1';
          else
            chB_overflow <= '1';
          end if;
        end if;  
        
        -------------------------------------------------------------------------
        --Update sending message structure
        -------------------------------------------------------------------------
        
        if bunch = bunch_BC0_start then
          -------------------------------------------------------------
          --we need to send a manditory BC0
          -------------------------------------------------------------
          if enable_BCX_reset = '1' then
            sending_message_valid <= '1';
            sending_message.mtype <= '0';
            sending_message.data <= X"000000" & X"01"; -- broadcasts are only
                                                       -- 8bits, but data is 32
                                                       -- because of addressed messages
            sending_counter <= sending_broadcast_start;                  
          end if;
        elsif sending_counter = 0 then
          -------------------------------------------------------------
          -- Move a pending message to the sending structure
          -------------------------------------------------------------
          if sending_message_valid = '0' then
            --timed message option
            if pending_message_valid = '1' then
              if pending_message.bc_time_en = '0' or -- untimed message
                (pending_message.mtype = '0' and pending_message.bc_time = std_logic_vector(bunch_bmesg)) or -- timed bmessage
                (pending_message.mtype = '1' and pending_message.bc_time = std_logic_vector(bunch_amesg)) then -- timed amessage
                if bunch < bunch_can_send_any_message then
                  -----------------------------------------------
                  --we have time to send a broadcast message or
                  --an addressed one, so check if we have one to send
                  -----------------------------------------------
                  -- move pending message to sending message
                  sending_message <= pending_message;
                  sending_message_valid <= '1';
                  --clear pending message
                  pending_message_valid <= '0';
                  --Set sending counter
                  if pending_message.mtype = '0' then
                    sending_counter <= sending_broadcast_start;
                  else
                    sending_counter <= sending_addressed_start;              
                  end if;
                elsif bunch < bunch_can_send_broadcast_message_only then  --3547
                  -----------------------------------------------
                  --we only have time to send a broadcast message,
                  --so check if we have one to send
                  -----------------------------------------------
                  -- move pending message to sending message
                  sending_message <= pending_message;
                  sending_message_valid <= '1';
                  -- clear pending message
                  pending_message_valid <= '0';
                  --set sending counter
                  sending_counter <= sending_broadcast_start;
                end if;
              end if;
            end if;           
          end if;
        elsif sending_counter = sending_end then
          ---------------------------------------------------------------
          -- handle end of sending message
          ---------------------------------------------------------------
          --reset sending counter
          sending_counter <= sending_idle;
          -- reset sending message
          sending_message_valid <= '0';
        else
          ---------------------------------------------------------------
          -- handle sending counter countdown
          --------------------------------------------------------------- 
          sending_counter <= sending_counter - TO_UNSIGNED(1,6);
        end if;
      end if;   
    end if;     
  end process mproc;
  

  -----------------------------------------------------------------------------
  -- Data streamer
  -- generate stream of data from a ch b message
  -----------------------------------------------------------------------------
  data_streamer: process (clk_lhc)
  begin  -- process stream
    if clk_lhc'event and clk_lhc = '1' then
      if reset = '1' then
        stream <= '1';        
      else
        -- check if we are sending out a message
        if sending_message_valid = '1' then
          -----------------------------------------------------------------------
          --broadcast message
          -----------------------------------------------------------------------
          if sending_message.mtype = '0' then
            case TO_INTEGER(sending_counter) is
              when 16 => stream <= '0';
              when 15 => stream <= '0';
              when 7 to 14 =>
                stream <= sending_message.data(TO_INTEGER(sending_counter) - 7);
              when 6 =>
                -- hmg[4] = d[0]^d[2]^d[3]^d[5]^d[6]^d[7];           
                stream <= sending_message.data(0) xor sending_message.data(2) xor sending_message.data(3) xor 
                          sending_message.data(5) xor sending_message.data(6) xor sending_message.data(7);
              when 5 =>
                -- hmg[3] = d[1]^d[3]^d[4]^d[6]^d[7];
                stream <= sending_message.data(1) xor sending_message.data(3) xor sending_message.data(4) xor 
                          sending_message.data(6) xor sending_message.data(7);
              when 4 =>
                -- hmg[2] = d[1]^d[2]^d[4]^d[5]^d[7];
                stream <= sending_message.data(1) xor sending_message.data(2) xor sending_message.data(4) xor 
                          sending_message.data(5) xor sending_message.data(7);
              when 3 =>
--                -- hmg[1] = d[0]^d[4]^d[5]^d[6];
--                stream <= sending_message.data(0) xor sending_message.data(4) xor sending_message.data(5) xor 
--                          sending_message.data(7);
                -- hmg[1] = d[0]^d[4]^d[5]^d[6];
                stream <= sending_message.data(0) xor sending_message.data(4) xor sending_message.data(5) xor 
                          sending_message.data(6);

              when 2 =>
                -- hmg[0] = d[0]^d[1]^d[2]^d[3];
                stream <= sending_message.data(0) xor sending_message.data(1) xor sending_message.data(2) xor 
                          sending_message.data(3);

              when 1 => stream <= '1';
              when others => stream <= '1';
            end case;
          -----------------------------------------------------------------------
          --Addressed message
          -----------------------------------------------------------------------
          else
            case TO_INTEGER(sending_counter) is
              when 42 => stream <= '0';
              when 41 => stream <= '1';
              when 9 to 40 =>
                stream <= sending_message.data(TO_INTEGER(sending_counter) - 9);
              when 8 =>
                -- hmg[6] = hmg[0]^hmg[1]^hmg[2]^hmg[3]^hmg[4]^hmg[5]^d[0]^d[1]^d[2]^d[3]
                --              ^d[4]^d[5]^d[6]^d[7]^d[8]^d[9]^d[10]^d[11]^d[12]^d[13]^d[14]
                --              ^d[15]^d[16]^d[17]^d[18]^d[19]^d[20]^d[21]^d[22]^d[23]^d[24]
                --              ^d[25]^d[26]^d[27]^d[28]^d[29]^d[30]^d[31];
                stream <= sending_message.data(2) xor sending_message.data(4) xor sending_message.data(5) xor                       
                          sending_message.data(7) xor sending_message.data(8) xor sending_message.data(10) xor
                          sending_message.data(13) xor sending_message.data(14) xor sending_message.data(17) xor 
                          sending_message.data(19) xor sending_message.data(20) xor sending_message.data(21) xor
                          sending_message.data(24) xor sending_message.data(26) xor sending_message.data(27) xor 
                          sending_message.data(29) xor sending_message.data(30) xor sending_message.data(31);
              when 7 =>
                -- hmg[5] = d[1]^d[3]^d[5]^d[6]^d[8]^d[10]^d[12]^d[14]^d[16]^d[18]^d[20]
                --            ^d[21]^d[23]^d[25]^d[27]^d[28]^d[30]^d[31];
                stream <= sending_message.data(1) xor sending_message.data(3) xor sending_message.data(5) xor
                          sending_message.data(6) xor sending_message.data(8) xor sending_message.data(10) xor
                          sending_message.data(12) xor sending_message.data(14) xor sending_message.data(16) xor
                          sending_message.data(18) xor sending_message.data(20) xor sending_message.data(21) xor
                          sending_message.data(23) xor sending_message.data(25) xor sending_message.data(27) xor
                          sending_message.data(28) xor sending_message.data(30) xor sending_message.data(31);
              when 6 =>
                -- hmg[4] = d[0]^d[3]^d[4]^d[6]^d[7]^d[10]^d[11]^d[14]^d[15]^d[18]^d[19]
                --            ^d[21]^d[22]^d[25]^d[26]^d[28]^d[29]^d[31];
                stream <= sending_message.data(0) xor sending_message.data(3) xor sending_message.data(4) xor
                          sending_message.data(6) xor sending_message.data(7) xor sending_message.data(10) xor
                          sending_message.data(11) xor sending_message.data(14) xor sending_message.data(15) xor
                          sending_message.data(18) xor sending_message.data(19) xor sending_message.data(21) xor
                          sending_message.data(22) xor sending_message.data(25) xor sending_message.data(26) xor
                          sending_message.data(28) xor sending_message.data(29) xor sending_message.data(31);
              when 5 =>
                -- hmg[3] = d[0]^d[1]^d[2]^d[6]^d[7]^d[8]^d[9]^d[14]^d[15]^d[16]^d[17]
                --            ^d[21]^d[22]^d[23]^d[24]^d[28]^d[29]^d[30];
                stream <= sending_message.data(0) xor sending_message.data(1) xor sending_message.data(2) xor
                          sending_message.data(6) xor sending_message.data(7) xor sending_message.data(8) xor
                          sending_message.data(9) xor sending_message.data(14) xor sending_message.data(15) xor
                          sending_message.data(16) xor sending_message.data(17) xor sending_message.data(21) xor
                          sending_message.data(22) xor sending_message.data(23) xor sending_message.data(24) xor
                          sending_message.data(28) xor sending_message.data(29) xor sending_message.data(30);
              when 4 =>
                -- hmg[2] = d[6]^d[7]^d[8]^d[9]^d[10]^d[11]^d[12]^d[13]^d[21]^d[22]^d[23]
                --            ^d[24]^d[25]^d[26]^d[27];
                stream <= sending_message.data(6) xor sending_message.data(7) xor sending_message.data(8) xor
                          sending_message.data(9) xor sending_message.data(10) xor sending_message.data(11) xor
                          sending_message.data(12) xor sending_message.data(13) xor sending_message.data(21) xor
                          sending_message.data(22) xor sending_message.data(23) xor sending_message.data(24) xor
                          sending_message.data(25) xor sending_message.data(26) xor sending_message.data(27);
              when 3 =>
                -- hmg[1] = d[6]^d[7]^d[8]^d[9]^d[10]^d[11]^d[12]^d[13]^d[14]^d[15]^d[16]
                --            ^d[17]^d[18]^d[19]^d[20];
                stream <= sending_message.data(6) xor sending_message.data(7) xor sending_message.data(8) xor
                          sending_message.data(9) xor sending_message.data(10) xor sending_message.data(11) xor
                          sending_message.data(12) xor sending_message.data(13) xor sending_message.data(14) xor
                          sending_message.data(15) xor sending_message.data(16) xor sending_message.data(17) xor
                          sending_message.data(18) xor sending_message.data(19) xor sending_message.data(20);
              when 2 =>
                -- hmg[0] = d[0]^d[1]^d[2]^d[3]^d[4]^d[5];
                stream <= sending_message.data(0) xor sending_message.data(1) xor sending_message.data(2) xor
                          sending_message.data(3) xor sending_message.data(4) xor sending_message.data(5);
              when 1 => stream <= '1';
              when others => stream <= '1';       
            end case;
          end if;
        else
          -- idle sequence
          stream <= '1';
        end if;         
      end if;
    end if;
  end process data_streamer;

end Behavioral;
