library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

use work.TCDS.all;

entity TCDS_interface is

  port (
    clk_LHC     : in  std_logic;
    reset       : in  std_logic;
    TCDS_p      : in  std_logic;
    TCDS_n      : in  std_logic;
    Monitor     : out TCDS_Monitor;
    Control     : in TCDS_Control);
end entity TCDS_interface;

architecture behavioral of TCDS_interface is
  component TCDS_capture is
    port (
      clk_LHC_5x_p    : in  std_logic;
      clk_LHC_5x_n    : in  std_logic;
      clk_LHC         : in  std_logic;
      reset           : in  std_logic;
      TCDS_p          : in  std_logic;
      TCDS_n          : in  std_logic;
      data            : out std_logic_vector(7 downto 0) := "00000000";
      data_is_k_char  : out std_logic                    := '0';
      data_valid      : out std_logic                    := '0';
      data_locked     : out std_logic                    := '0';
      error_bad_code  : out std_logic                    := '0';
      error_disparity : out std_logic                    := '0';
      error_alignment : out std_logic                    := '0';
      error_overflow  : out std_logic                    := '0';
      DCM_phase         : out signed(7 downto 0)         := x"00";
      DCM_status        : out std_logic_vector(2 downto 0) := "000";
      DCM_input_stopped : out std_logic                    := '0';
      DCM_locked        : out std_logic                    := '0';
      DCM_valid         : out std_logic                    := '0');
  end component TCDS_capture;

  component counter is
    generic (
      roll_over : std_logic;
      end_value   : std_logic_vector;
      start_value : std_logic_vector;
      width     : integer);
    port (
      clk       : in  std_logic;
      reset     : in  std_logic;
      event     : in  std_logic;
      count     : out unsigned(width-1 downto 0);
      at_max    : out std_logic);
  end component counter;

  -------------------------------------------------------------------------------
  -- signals
  -------------------------------------------------------------------------------

  -- TCDS capture
  signal TCDS_data           : std_logic_vector(7 downto 0) := x"00";
  signal TCDS_data_valid     : std_logic                    := '0';
  signal TCDS_data_locked    : std_logic                    := '0';
  signal TCDS_data_is_k_char : std_logic                    := '0';
  signal TCDS_k_char         : std_logic                    := '0';
  signal TCDS_data_char      : std_logic                    := '0';
  
  signal error_bad_code  : std_logic := '0';
  signal error_disparity : std_logic := '0';
  signal error_alignment : std_logic := '0';
  signal error_overflow  : std_logic := '0';

  signal TTS_state : std_logic_vector(3 downto 0) := x"0";

  signal data_event : std_logic_vector(255 downto 0);
begin
 
  -------------------------------------------------------------------------------
  -- Capture the TCDS stream
  -------------------------------------------------------------------------------  
  TCDS_capture_1 : entity work.TCDS_capture
    port map (
      clk_LHC         => clk_LHC,
      reset           => reset or Control.reset_DCM,
      TCDS_p          => TCDS_p,
      TCDS_n          => TCDS_n,
      data            => TCDS_data,
      data_is_k_char  => TCDS_data_is_k_char,
      data_valid      => TCDS_data_valid,
      data_locked     => TCDS_data_locked,
      error_bad_code  => error_bad_code,
      error_disparity => error_disparity,
      error_alignment => error_alignment,
      error_overflow  => error_overflow,
      DCM_phase       => Monitor.DCM_phase,
      DCM_status        => Monitor.DCM_status,
      DCM_input_stopped => Monitor.DCM_input_stopped,
      DCM_locked        => Monitor.DCM_locked,
      DCM_valid         => Monitor.DCM_valid      
      );

  -------------------------------------------------------------------------------
  -- Decode the TCDS stream and output the TTS state
  -------------------------------------------------------------------------------  
  Monitor.locked <= TCDS_data_locked;
  Monitor.TTS_state <= TTS_State;
  TTS_capture : process (clk_LHC, reset) is
  begin  -- process TTS_capture
    if reset = '1' then                 -- asynchronous reset (active high)
      TTS_State <= x"0";
    elsif clk_LHC'event and clk_LHC = '1' then  -- rising clock edge
      if TCDS_data_locked = '1' and TCDS_data_valid = '1' then
        if TCDS_data_is_k_char = '0' then
          TTS_State <= TCDS_data(3 downto 0);
        end if;
      else
        TTS_State <= x"0";
      end if;
    end if;
  end process TTS_capture;

  counter_1 : entity work.counter
    generic map (
      roll_over => '0',
      end_value => X"FFFFFFFFFFFFFFFF",
      start_value => x"0000000000000000",
      width     => 64)
    port map (
      clk       => clk_LHC,
      reset     => Control.reset_bad_10b_codes,
      event     => error_bad_code,
      count     => Monitor.count_bad_10b_codes,
      at_max    => open);

  counter_2 : entity work.counter
    generic map (
      roll_over => '0',
      end_value => X"FFFFFFFFFFFFFFFF",
      start_value => x"0000000000000000",
      width     => 64)
    port map (
      clk       => clk_LHC,
      reset     => Control.reset_disparity_errors,
      event     => error_disparity,
      count     => Monitor.count_disparity_errors,
      at_max    => open);

  counter_3 : entity work.counter
    generic map (
      roll_over => '0',
      end_value => X"FFFFFFFFFFFFFFFF",
      start_value => x"0000000000000000",
      width     => 64)
    port map (
      clk       => clk_LHC,
      reset     => Control.reset_idle_char_alignment_errors,
      event     => error_alignment,
      count     => Monitor.count_idle_char_alignment_errors,
      at_max    => open);

  counter_4 : entity work.counter
    generic map (
      roll_over => '0',
      end_value => X"FFFFFFFFFFFFFFFF",
      start_value => x"0000000000000000",
      width     => 64)
    port map (
      clk       => clk_LHC,
      reset     => Control.reset_overflowed_10b_chars,
      event     => error_overflow,
      count     => Monitor.count_overflowed_10b_chars,
      at_max    => open);


  TCDS_data_char <= TCDS_data_valid and TCDS_data_locked and (not TCDS_data_is_k_char);
  counter_5 : entity work.counter
    generic map (
      roll_over => '0',
      end_value => X"FFFFFFFFFFFFFFFF",
      start_value => x"0000000000000000",
      width     => 64)
    port map (
      clk       => clk_LHC,
      reset     => Control.reset_8b10b_data_chars,
      event     => TCDS_data_char,
      count     => Monitor.count_8b10b_data_chars,
      at_max    => open);
  TCDS_k_char <= TCDS_data_valid and TCDS_data_locked and TCDS_data_is_k_char;
  counter_6 : entity work.counter
    generic map (
      roll_over => '0',
      end_value => X"FFFFFFFFFFFFFFFF",
      start_value => x"0000000000000000",
      width     => 64)
    port map (
      clk       => clk_LHC,
      reset     => Control.reset_8b10b_k_chars,
      event     => TCDS_k_char,
      count     => Monitor.count_8b10b_k_chars,
      at_max    => open);


  tcds_counters: for iByte in 0 to Monitor.count_TCDS_states'length -1 generate
  
    data_event(iByte) <= TCDS_data_valid and (not TCDS_data_is_k_char) when (to_unsigned(iByte,8) = unsigned(TCDS_data)) else '0';
--    data_event <=  when (to_unsigned(iByte,8) == unsigned(TCDS_data)) else '0';
    counter_7 : entity work.counter
      generic map (
        roll_over => '0',
        end_value => X"FF",
        start_value => x"00",
        width     => 8)
      port map (
        clk       => clk_LHC,
        reset     => Control.reset_all,
        event     => data_event(iByte),
        count     => Monitor.count_TCDS_states(iByte)(7 downto 0),
        at_max    => open);
    
  end generate tcds_counters;

end architecture behavioral;
