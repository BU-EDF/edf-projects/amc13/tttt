restart
source vhdl/sim/UART_helper.tcl

#set initial conditions for UART
isim force add {/top/UART_rx} 1

#set clock on /top/clk
isim force add {/top/clk_local_in} 1 -radix bin -value 0 -radix bin -time 12.5 ns -repeat 25 ns

#get random number generator into a useful state
#isim force /top/TTC_Interface_1/Trigger_Generator_1/urand_1/m1/U0/ 

run 100 us


#Set triggers to only fall in the calibration window (FULL trigger rules)
isim force add {/top/reg_addr} 31 -radix hex
run 100ns
isim force add {/top/reg_data_out} 0d900d70000000F1 -radix hex
isim force add {/top/reg_wr} 1 -radix bin
run 25ns
isim force add {/top/reg_wr} 0 -radix bin


#turn on the on bc software trigger
isim force add {/top/reg_addr} 35 -radix hex
run 100ns
isim force add {/top/reg_wr} 1 -radix bin
isim force add {/top/reg_data_out} 0000000000000008 -radix hex
run 25ns
isim force add {/top/reg_wr} 0 -radix bin


isim force add {/top/reg_addr} 39 -radix hex
run 100ns
isim force add {/top/reg_wr} 1 -radix bin
isim force add {/top/reg_data_out} 000000000d820001 -radix hex
run 25ns
isim force add {/top/reg_wr} 0 -radix bin


run 100 us
