------------------------------------------------------------------------------
-- control_register :
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;
--use ieee.std_logic_signed."+";
--use ieee.std_logic_signed."-";
--use ieee.std_logic_signed."=";

use ieee.numeric_std.all;
use IEEE.std_logic_misc.all;

use work.TCDS.all;
use work.TTC.all;
use work.TTC_chB.all;

library UNISIM;
use UNISIM.vcomponents.all;

entity register_map is
  generic (
    WIDTH : integer := 64);  
  port (
    clk : in std_logic;
    reset : in std_logic;

    --interface to uC
    address : in std_logic_vector(15 downto  0);
    out_port_rd : in std_logic := '0';
    out_port : out std_logic_vector(WIDTH-1 downto 0);
    out_port_dv : out std_logic := '0';
    in_port : in std_logic_vector(WIDTH-1 downto 0);
    in_port_wr : in std_logic := '0';

    
    -- Logicboard firmware version
    Firmware_Version : in std_logic_vector(WIDTH-1 downto 0);
    --TCDS
    TCDS_in   : in TCDS_Monitor;
    TCDS_out  : out TCDS_Control;
    --TTC
    TTC_in    : in TTC_Monitor;
    TTC_out   : out TTC_Control
    );


end entity register_map;

architecture arch of register_map is

-----------------------------------------------------------------------------
-- Address space
-----------------------------------------------------------------------------

  ----------------------------
  -- Logic board
  ----------------------------
  constant CLK_STATUS              : unsigned(15 downto 0)  := x"0000";
  constant TTTT_Firmware_Version : unsigned(15 downto 0)  := x"0001";
  -- 31..24 (r) year (20YY)
  -- 23..16 (r) month (MM)
  -- 15..8  (r) day (DD)
  -- 7..0   (r) minor version  

  constant SCRATCH : unsigned(15 downto 0)  := x"0002";
  --63..0 (r/w) scratch

  
  constant TCDS_status                             : unsigned(15 downto 0)  := x"0010";
  -- 0 (r) locked
  -- 7..4 (r) TTS state
  -- 15..8 (r) DCM Phase
  -- 32 (r) DCM locked
  -- 35..33 (r) DCM status
  -- 36 (r) DCM valid
  -- 37 (r) DCM input stopped
  -- 62 (a) reset DCM
  -- 63 (a) reset counters
  constant TCDS_bad_10b_codes                      : unsigned(15 downto 0)  := x"0022";
  -- 63..0 (r/a) bad 10b codes
  constant TCDS_disparity_errors                   : unsigned(15 downto 0)  := x"0023";
  -- 63..0 (r/a) disparity errors
  constant TCDS_idle_char_alignment_errors         : unsigned(15 downto 0)  := x"0024";
  -- 63..0 (r/a) idle char alignment errors
  constant TCDS_overflowed_10b_chars               : unsigned(15 downto 0)  := x"0025";
  -- 63..0 (r/a) overflowed 10b chars
  constant TCDS_8b10b_data_chars                   : unsigned(15 downto 0)  := x"0026";
  -- 63..0 (r/a) TCDS data chars
  constant TCDS_8b10b_k_chars                      : unsigned(15 downto 0)  := x"0027";
  -- 63..0 (r/a) TCDS k chars

    
  constant TTC_status                              : unsigned(15 downto 0)  := x"0030";
  -- 11..0 (r) BX counter
  -- 31..16 (r) orbit number
  -- 35..32 (r) TTC symbol histo 00
  -- 39..36 (r) TTC symbol histo 01
  -- 43..40 (r) TTC symbol histo 10
  -- 47..44 (r) TTC symbol histo 11
  -- 55..48 (r) TTC fifo underflow Count            
  -- 63..56 (r) TTC fifo overflow Count            
  constant TTC_Control_1                           : unsigned(15 downto 0)  := x"0031";
  -- 0 (r/w) BX reset enabled
  -- 1 (r/w) TTS state rate control enabled
  -- 7..4 (r/w) enable trigger rule
  -- 43..32 (r/w) BC to start allowing triggers (inclusive)
  -- 59..48 (r/w) BC to stop allowing triggers (exclusive)
  constant TTC_CONTROL_2                           : unsigned(15 downto 0)  := x"0032";
  -- 11..0 (r/w) normal bc rollover number (0xDEB)
  -- 27..16 (r/w) l1a enable range
  -- 43..32 (r/w) l1a software trigger
  constant TTC_message                             : unsigned(15 downto 0)  := x"0033";
  -- 31..0 (r/w) data
  -- 32 (r/w) message type (0 broadcast;1 addressed)
  -- 36 (r/a) message queue full/message write
  -- 37 (r/w) message bcx time enabled
  -- 51..40 (r/w) message bx transmit time
  constant TTC_advanced_triggering                 : unsigned(15 downto 0)  := x"0034";
  -- 0 (a) send bc timed message and software on bc trigger
  
  constant TTC_Trigger_Control                     : unsigned(15 downto 0)  := x"0035";
  -- 0 (r/w) enable periodic trigger
  -- 1 (r/w) enable random trigger
  -- 2 (r/w) enable external trigger
  -- 7..3 (n) reserved
  
  constant TTC_Trigger_Periodic                    : unsigned(15 downto 0)  := x"0036";
  -- 15..0 (r/w) periodic trigger period (units?)
  constant TTC_Trigger_Random                      : unsigned(15 downto 0)  := x"0037";
  -- 15..0 (r/w) random trigger timescale (units?)
  constant TTC_Trigger_Software                    : unsigned(15 downto 0)  := x"0038";
  -- 0 (a)   send software trigger
  constant TTC_Trigger_Software_on_bc              : unsigned(15 downto 0)  := x"0039";
  -- 0 (a)   send software trigger
  -- 27..16 (r/w) bc number to send trigger 
  constant TTC_Trigger_External                    : unsigned(15 downto 0)  := x"003a";

  
  constant TTC_L1A_total                           : unsigned(15 downto 0)  := x"0040";
  -- 63..32 (r) triggers skipped due to trigger rules
  -- 31..0  (r) trigger count
  constant TTC_L1A_periodic                        : unsigned(15 downto 0)  := x"0041";
  -- 63..32 (r) triggers skipped due to trigger rules
  -- 31..0  (r) trigger count
  constant TTC_L1A_random                          : unsigned(15 downto 0)  := x"0042";
  -- 63..32 (r) triggers skipped due to trigger rules
  -- 31..0  (r) trigger count
  constant TTC_L1A_software                        : unsigned(15 downto 0)  := x"0043";
  -- 63..32 (r) triggers skipped due to trigger rules
  -- 31..0  (r) trigger count
  constant TTC_L1A_software_on_bc                  : unsigned(15 downto 0)  := x"0044";
  -- 63..32 (r) triggers skipped due to trigger rules
  -- 31..0  (r) trigger count
  constant TTC_L1A_external                        : unsigned(15 downto 0)  := x"0045";
  -- 63..32 (r) triggers skipped due to trigger rules
  -- 31..0  (r) trigger count
  constant TTC_L1A_TTS                             : unsigned(15 downto 0)  := x"0046";
  -- 63..32 (r) triggers skipped due to TTS state

  constant TCDS_COUNTERS_START                     : unsigned(15 downto 0)  := x"0100";
  constant TCDS_COUNTERS_END                       : unsigned(15 downto 0)  := TCDS_COUNTERS_START + TCDS_in.count_TCDS_states'length - 1;
  -- 63..32 (r) count of TCDS values

  
  -----------------------------------------------------------------------------
  -- Signals
  -----------------------------------------------------------------------------
  signal local_TTC_Control : TTC_Control := TTC_CONTROL_DEFAULT;

  signal scratch_reg : std_logic_vector(63 downto 0) := x"DEADBEEFABADCAFE";
  
begin  -- architecture arch

  --buffered TTC message
  TTC_out <= local_TTC_Control;
    
  -----------------------------------------------------------------------------
  -- read
  -----------------------------------------------------------------------------
  read_access: process (clk, reset)
  begin  -- process read_access
    if reset = '1' then                 -- asynchronous reset (active high)
      out_port_dv <= '0';
    elsif clk'event and clk = '1' then  -- rising clock edge
      out_port_dv <= '0';
      out_port <= (others => '0'); --zero out the bits we don't use
      if out_port_rd = '1' then
        out_port_dv <= '1';
        case to_integer(unsigned(address)) is
          when to_integer(TTTT_Firmware_Version            ) =>            
            out_port                <= Firmware_Version;
          when to_integer(TCDS_STATUS                      ) =>
            out_port(0)             <= TCDS_in.locked;
            out_port(7 downto 4)    <= TCDS_in.TTS_state;
            out_port(15 downto 8)   <= std_logic_vector(TCDS_in.DCM_phase);
            out_port(32)            <= TCDS_in.DCM_locked;
            out_port(35 downto 33)  <= TCDS_in.DCM_status;
            out_port(36)            <= TCDS_in.DCM_valid;
            out_port(37)            <= TCDS_in.DCM_input_stopped;
          when to_integer(SCRATCH) =>
            out_port <= scratch_reg;
          when to_integer(TCDS_bad_10b_codes               ) =>
            out_port                <= std_logic_vector(TCDS_in.count_bad_10b_codes);
          when to_integer( TCDS_disparity_errors           ) =>
            out_port                <= std_logic_vector(TCDS_in.count_disparity_errors);
          when to_integer(TCDS_idle_char_alignment_errors  ) =>
            out_port                <= std_logic_vector(TCDS_in.count_idle_char_alignment_errors);
          when to_integer(TCDS_overflowed_10b_chars        ) =>
            out_port                <= std_logic_vector(TCDS_in.count_overflowed_10b_chars);
          when to_integer(TCDS_8b10b_data_chars            ) =>
            out_port                <= std_logic_vector(TCDS_in.count_8b10b_data_chars);
          when to_integer(TCDS_8b10b_k_chars               ) =>
            out_port                <= std_logic_vector(TCDS_in.count_8b10b_k_chars);
          when to_integer(TTC_status                       ) =>
            out_port(11 downto 0)   <= std_logic_vector(TTC_in.BX_counter);
            out_port(31 downto 16)  <= std_logic_vector(TTC_in.orbit_number);
            out_port(35 downto 32)  <= std_logic_vector(TTC_in.bpmc_histo(0));
            out_port(39 downto 36)  <= std_logic_vector(TTC_in.bpmc_histo(1));
            out_port(43 downto 40)  <= std_logic_vector(TTC_in.bpmc_histo(2));
            out_port(47 downto 44)  <= std_logic_vector(TTC_in.bpmc_histo(3));
            out_port(55 downto 48)  <= std_logic_vector(TTC_in.TTC_underflow_count);
            out_port(63 downto 56)  <= std_logic_vector(TTC_in.TTC_overflow_count);
          when to_integer(TTC_CONTROL_1                    ) =>
            out_port(59 downto 48)  <= local_TTC_Control.bc_end_triggers;
            out_port(43 downto 32)  <= local_TTC_Control.bc_start_triggers;
            out_port(7 downto 4)    <= local_TTC_Control.enable_trigger_rule;
            out_port(1)             <= local_TTC_Control.enable_TTS;
            out_port(0)             <= local_TTC_Control.enable_BCX_reset;
          when to_integer(TTC_CONTROL_2                    ) =>
            out_port(43 downto 32)  <= std_logic_vector(local_TTC_Control.bc_rollovers.l1a_software_trigger);
            out_port(27 downto 16)  <= std_logic_vector(local_TTC_Control.bc_rollovers.l1a_allowed_range);
            out_port(11 downto  0)  <= std_logic_vector(local_TTC_Control.bc_rollovers.normal);
          when to_integer(TTC_message                      ) =>
            out_port(31 downto  0)  <= local_TTC_Control.chB_message.data;
            out_port(32)            <= local_TTC_Control.chB_message.mtype;
            out_port(36)            <= TTC_in.chB_full;
            out_port(37)            <= local_TTC_Control.chb_message.bc_time_en;
            out_port(51 downto 40)  <= local_TTC_Control.chb_message.bc_time;
          when to_integer(TTC_Trigger_Control              ) =>
            out_port(0)             <= local_TTC_control.enable_periodic;  
            out_port(1)             <= local_TTC_control.enable_random;    
            out_port(2)             <= local_TTC_control.enable_external;  
          when to_integer(TTC_Trigger_Periodic             ) =>
            out_port(15 downto 0)  <= std_logic_vector(local_TTC_control.period);  
          when to_integer(TTC_Trigger_Random               ) =>
            out_port(15 downto 0)  <= std_logic_vector(local_TTC_control.random_timescale);  
          when to_integer(TTC_Trigger_Software_on_bc       ) =>
            out_port(27 downto 16) <= std_logic_vector(local_TTC_control.software_bcn);  

          when to_integer(TTC_L1A_total                    ) =>
            out_port(63 downto 32) <= std_logic_vector(TTC_in.Skipped_Triggers.total);
            out_port(31 downto 0)  <= std_logic_vector(TTC_in.Triggers.total);
          when to_integer(TTC_L1A_periodic                 ) =>
            out_port(63 downto 32) <= std_logic_vector(TTC_in.Skipped_Triggers.periodic);
            out_port(31 downto 0)  <= std_logic_vector(TTC_in.Triggers.periodic);
          when to_integer(TTC_L1A_random                   ) =>
            out_port(63 downto 32) <= std_logic_vector(TTC_in.Skipped_Triggers.random);
            out_port(31 downto 0)  <= std_logic_vector(TTC_in.Triggers.random);
          when to_integer(TTC_L1A_software                 ) =>
            out_port(63 downto 32) <= std_logic_vector(TTC_in.Skipped_Triggers.software);
            out_port(31 downto 0)  <= std_logic_vector(TTC_in.Triggers.software);
          when to_integer(TTC_L1A_software_on_bc           ) =>
            out_port(63 downto 32) <= std_logic_vector(TTC_in.Skipped_Triggers.software_on_bc);
            out_port(31 downto 0)  <= std_logic_vector(TTC_in.Triggers.software_on_bc);
          when to_integer(TTC_L1A_external                 ) =>
            out_port(63 downto 32) <= std_logic_vector(TTC_in.Skipped_Triggers.external);
            out_port(31 downto 0)  <= std_logic_vector(TTC_in.Triggers.external);
          when to_integer(TTC_L1A_TTS                      ) =>
            out_port(63 downto 32) <= std_logic_vector(TTC_in.Skipped_Triggers.tts);

          when to_integer(unsigned(TCDS_COUNTERS_START)) to to_integer(unsigned(TCDS_COUNTERS_END)) =>            
            out_port(63 downto  0) <= std_logic_vector(TCDS_in.count_TCDS_states(to_integer(unsigned(address) - unsigned(TCDS_COUNTERS_START))));
          when others => null;
        end case;
      end if;
    end if;
  end process read_access;
  
  -----------------------------------------------------------------------------
  -- write
  -----------------------------------------------------------------------------
  
  write_access: process (clk, reset)
  begin  -- process read_access
    if reset = '1' then                 -- asynchronous reset (active high)
      local_TTC_Control <= TTC_Control_DEFAULT;
      TCDS_out <= TCDS_Control_DEFAULT;
      scratch_reg <= x"DEADBEEFABADCAFE";
    elsif clk'event and clk = '1' then  -- rising clock edge
      TCDS_out.reset_bad_10b_codes                            <= '0';
      TCDS_out.reset_disparity_errors                         <= '0';
      TCDS_out.reset_idle_char_alignment_errors               <= '0';
      TCDS_out.reset_overflowed_10b_chars                      <= '0';
      TCDS_out.reset_8b10b_data_chars                         <= '0';
      TCDS_out.reset_8b10b_k_chars                            <= '0';
      TCDS_out.reset_all                                      <= '0';
      TCDS_out.reset_DCM                                      <= '0';
      local_TTC_Control.chB_wr                                <= '0';
      local_TTC_Control.software_trigger                      <= '0';
      local_TTC_Control.software_on_bc_trigger                <= '0';
      local_TTC_Control.chB_L1A_trigger                       <= '0';



      if in_port_wr = '1' then
        case unsigned(address) is
          when SCRATCH =>
            scratch_reg <= in_port;
          when TTC_CONTROL_1                  =>
            local_TTC_Control.bc_end_triggers              <= in_port(59 downto 48);
            local_TTC_Control.bc_start_triggers            <= in_port(43 downto 32);
            local_TTC_Control.enable_trigger_rule          <= in_port(7 downto 4);
            local_TTC_Control.enable_TTS                   <= in_port(1);
            local_TTC_Control.enable_BCX_reset             <= in_port(0);
          when TTC_CONTROL_2                    =>
            local_TTC_Control.bc_rollovers.l1a_software_trigger <= unsigned(in_port(43 downto 32));
            local_TTC_Control.bc_rollovers.l1a_allowed_range    <= unsigned(in_port(27 downto 16));
            local_TTC_Control.bc_rollovers.normal               <= unsigned(in_port(11 downto  0));
          when TTC_message                      =>
            local_TTC_Control.chB_message.data             <= in_port(31 downto  0);
            local_TTC_Control.chB_message.mtype            <= in_port(32);
            local_TTC_Control.chb_message.bc_time_en       <= in_port(37);
            local_TTC_Control.chb_message.bc_time          <= in_port(51 downto 40);
            if in_port(36) = '1' then
              local_TTC_Control.chB_wr                        <= '1';              
            end if;

          when TTC_Trigger_Control              =>
            local_TTC_control.enable_periodic              <= in_port(0);
            local_TTC_control.enable_random                <= in_port(1);
            local_TTC_control.enable_external              <= in_port(2);
          when TTC_Trigger_Periodic             =>
            local_TTC_control.period                       <= unsigned(in_port(15 downto 0));
          when TTC_Trigger_Random               =>
            local_TTC_control.random_timescale             <= unsigned(in_port(15 downto 0));
          when TTC_Trigger_Software_on_bc       =>
            local_TTC_control.software_bcn                 <= in_port(27 downto 16);
            if in_port(0) = '1' then
              local_TTC_Control.software_on_bc_trigger        <= '1';
            end if;


          when TCDS_status                      =>
            TCDS_out.reset_all <= in_port(63);
            TCDS_out.reset_DCM <= in_port(62);
          when TCDS_bad_10b_codes               =>
            TCDS_out.reset_bad_10b_codes                      <= '1';
          when  TCDS_disparity_errors           =>
            TCDS_out.reset_disparity_errors                   <= '1';
          when TCDS_idle_char_alignment_errors  =>
            TCDS_out.reset_idle_char_alignment_errors         <= '1';
          when TCDS_overflowed_10b_chars        =>
            TCDS_out.reset_overflowed_10b_chars               <= '1';
          when TCDS_8b10b_data_chars            =>
            TCDS_out.reset_8b10b_data_chars                   <= '1';
          when TCDS_8b10b_k_chars               =>
            TCDS_out.reset_8b10b_k_chars                      <= '1';
          when TTC_Trigger_Software             =>
            if in_port(0) = '1' then
              local_TTC_Control.software_trigger              <= '1';
            end if;
          when TTC_advanced_triggering          =>
            if in_port(0) = '1' then
              local_TTC_Control.chB_L1A_trigger        <= '1';
            end if;            

            
          when others => null;
        end case;        
      end if;      
    end if;
  end process write_access;

end architecture arch;
