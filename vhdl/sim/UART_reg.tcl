restart
source vhdl/sim/UART_helper.tcl

#set initial conditions for UART
isim force add {/top/UART_rx} 1

#set clock on /top/clk
isim force add {/top/clk_local_in} 1 -radix bin -value 0 -radix bin -time 12.5 ns -repeat 25 ns 

#let everything start up
run 2 ms

#send daq_reg_rd command
sendUART_str daq_reg_rd 115200 /top/UART_rx;
#send space (0x20)
sendUART_hex 20 115200 /top/UART_rx;
sendUART_str 01 115200 /top/UART_rx;

#send CR (0x0D)
sendUART_hex 0D 115200 /top/UART_rx;

run 2 ms
