----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
-- 
-- Create Date:    
-- Design Name: 
-- Target Devices: XC6SLX9-TQFP144
-- Tool versions: 13.3 (lin64)
-- Description: 
--
-- Dependencies: 
--
-- Revision:
-- Revision 0.1 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

--This package conatains the record structure for a generic TTC channel B message
package BC_tuning is
  type BC_tune is record
    normal               : unsigned(11 downto 0);
    l1a_allowed_range    : unsigned(11 downto 0);
    l1a_software_trigger : unsigned(11 downto 0);
  end record BC_tune;
    
  type BC_tune_rollover is record
    normal               : unsigned(11 downto 0);
    l1a_allowed_range    : unsigned(11 downto 0);
    l1a_software_trigger : unsigned(11 downto 0);
  end record BC_tune_rollover;

  constant BUNCH_ROLLOVER  : unsigned(11 downto 0) := x"DEB";
  constant DEFAULT_TUNES : BC_tune_rollover := (BUNCH_ROLLOVER,x"DEB",x"000");
  constant TUNE_START : BC_tune := (x"000",x"000",x"000");
  
end BC_tuning;
