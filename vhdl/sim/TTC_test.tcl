restart 

isim force add {/top/clk_local_in} 0 -radix hex -value 1 -radix hex -time 12500 ps -repeat 25 ns
run 1 ms
isim force add {/top/reg_addr} 21 -radix hex
isim force add {/top/reg_data_out} FFFFFFFFF120A055 -radix hex
isim force add {/top/reg_wr} 1
run 25ns
isim force add {/top/reg_wr} 0
run 1ms
