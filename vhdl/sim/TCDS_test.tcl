restart 
source vhdl/sim/TCDS_helper.tcl

proc send_TCDS_idle { disp stream_p stream_n count TTS} {    
    for { set i 0 } {$i < $count } {incr i} {
	set disp [send_TCDS $disp $stream_p $stream_n $TTS ]
#	run 1ps
    }
    return $disp
}


isim force add {/top/TCDS_interface_1/TCDS_capture_1/phase_change} '1'
isim force remove {/top/TCDS_interface_1/TCDS_capture_1/phase_change}

set disp "-1"
isim force add {/top/clk_local_in} 0 -radix hex -value 1 -radix hex -time 12500 ps -repeat 25 ns
run 12.5 ns
set disp [send_TCDS_idle $disp {/top/sfp_rx_p[0]} {/top/sfp_rx_n[0]} 200 01]
set disp [send_TCDS_idle $disp {/top/sfp_rx_p[0]} {/top/sfp_rx_n[0]} 200 02]
set disp [send_TCDS_idle $disp {/top/sfp_rx_p[0]} {/top/sfp_rx_n[0]} 200 04]
set disp [send_TCDS_idle $disp {/top/sfp_rx_p[0]} {/top/sfp_rx_n[0]} 200 08]
run 1us
