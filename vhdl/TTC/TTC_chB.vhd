----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
-- 
-- Create Date:    
-- Design Name: 
-- Module Name: TTC Message B structure - Behavioral
-- Project Name: TTTT
-- Target Devices: XC6SLX9-TQFP144
-- Tool versions: 13.3 (lin64)
-- Description: 
--
-- Dependencies: 
--
-- Revision:
-- Revision 0.1 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

--This package conatains the record structure for a generic TTC channel B message
package TTC_chB is
  type Message is
  record
    mtype      : std_logic;                         -- broadcast -> 0 ; addressed -> 1
    data       : std_logic_vector(31 downto 0);     -- data for ch b
    bc_time_en : std_logic;                         -- enable BC0 time use
    bc_time    : std_logic_vector(11 downto 0);     -- BC0 time for sending mesg
  end record;
  constant BLANK_TTC_CHB_MESSAGE : Message := (data => X"00000000",bc_time=> x"000",others =>'0');
end TTC_chB;
