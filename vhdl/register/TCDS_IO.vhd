----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;


package TCDS  is

  type counter_array_t is array (0 to 255) of unsigned(63 downto 0);
  
  type TCDS_Monitor is

  record
    DCM_phase                        : signed(7 downto 0);
    DCM_status                       : std_logic_vector(2 downto 0);
    DCM_input_stopped                : std_logic;
    DCM_locked                       : std_logic;
    DCM_valid                        : std_logic;

    locked                           : std_logic;
    TTS_state                        : std_logic_vector(3 downto 0);
   
    count_bad_10b_codes              : unsigned(63 downto 0);
    count_disparity_errors           : unsigned(63 downto 0);
    count_idle_char_alignment_errors : unsigned(63 downto 0);
    count_overflowed_10b_chars       : unsigned(63 downto 0);
    count_8b10b_data_chars           : unsigned(63 downto 0);
    count_8b10b_k_chars              : unsigned(63 downto 0);       

    count_TCDS_states                : counter_array_t;               
    
  end record;

  type TCDS_Control is record
    reset_bad_10b_codes              : std_logic;
    reset_disparity_errors           : std_logic;
    reset_idle_char_alignment_errors : std_logic;
    reset_overflowed_10b_chars       : std_logic;
    reset_8b10b_data_chars           : std_logic;
    reset_8b10b_k_chars              : std_logic;
    reset_all                        : std_logic;
    reset_DCM                        : std_logic;
  end record;

  constant TCDS_Control_DEFAULT : TCDS_Control := (
    reset_bad_10b_codes              => '0',
    reset_disparity_errors           => '0',
    reset_idle_char_alignment_errors => '0',
    reset_overflowed_10b_chars       => '0',
    reset_8b10b_data_chars           => '0',
    reset_8b10b_k_chars              => '0',
    reset_all                        => '0',
    reset_DCM                        => '0');      
end TCDS;
