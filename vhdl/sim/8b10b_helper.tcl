#----------------------------------------
# send a bit string, lsb first
#----------------------------------------
proc send10b { bit_stream stream_p stream_n } {
    set n [string length $bit_stream]
    for { set i 0 } {$i < $n } {incr i} {
        set bit [string index $bit_stream $i ]
	if {$bit == "1"} {
	    isim force add $stream_p 1
	    isim force add $stream_n 0
	} else {
	    isim force add $stream_p 0
	    isim force add $stream_n 1
	}
	run 2.5 ns
    }
}

proc gen_8b10b_data_code {code disparity stream_p stream_n} {
    case $code {
	00 { if {$disparity == "1"} {
	    send10b 0110001011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1001110100 $stream_p $stream_n 
	    return "1" 
	}
	}
	
	01 { if {$disparity == "1"} {
	    send10b 1000101011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0111010100 $stream_p $stream_n 
	    return "1" 
	}
	}
	
	02 { if {$disparity == "1"} {
	    send10b 0100101011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1011010100 $stream_p $stream_n 
	    return "1" 
	}
	}

	03 { if {$disparity == "1"} {
	    send10b 1100010100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1100011011 $stream_p $stream_n 
	    return "1" 
	}
	}

	04 { if {$disparity == "1"} {
	    send10b 0010101011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1101010100 $stream_p $stream_n 
	    return "1" 
	}
	}

	05 { if {$disparity == "1"} {
	    send10b 1010010100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1010011011 $stream_p $stream_n 
	    return "1" 
	}
	}

	06 { if {$disparity == "1"} {
	    send10b 0110010100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0110011011 $stream_p $stream_n 
	    return "1" 
	}
	}

	07 { if {$disparity == "1"} {
	    send10b 0001110100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1110001011 $stream_p $stream_n 
	    return "1" 
	}
	}

	08 { if {$disparity == "1"} {
	    send10b 0001101011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1110010100 $stream_p $stream_n 
	    return "1" 
	}
	}

	09 { if {$disparity == "1"} {
	    send10b 1001010100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1001011011 $stream_p $stream_n 
	    return "1" 
	}
	}

	0A { if {$disparity == "1"} {
	    send10b 0101010100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0101011011 $stream_p $stream_n 
	    return "1" 
	}
	}

	0B { if {$disparity == "1"} {
	    send10b 1101000100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1101001011 $stream_p $stream_n 
	    return "1" 
	}
	}

	0C { if {$disparity == "1"} {
	    send10b 0011010100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0011011011 $stream_p $stream_n 
	    return "1" 
	}
	}

	0D { if {$disparity == "1"} {
	    send10b 1011000100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1011001011 $stream_p $stream_n 
	    return "1" 
	}
	}

	0E { if {$disparity == "1"} {
	    send10b 0111000100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0111001011 $stream_p $stream_n 
	    return "1" 
	}
	}

	0F { if {$disparity == "1"} {
	    send10b 1010001011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0101110100 $stream_p $stream_n 
	    return "1" 
	}
	}

	10 { if {$disparity == "1"} {
	    send10b 1001001011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0110110100 $stream_p $stream_n 
	    return "1" 
	}
	}

	11 { if {$disparity == "1"} {
	    send10b 1000110100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1000111011 $stream_p $stream_n 
	    return "1" 
	}
	}

	12 { if {$disparity == "1"} {
	    send10b 0100110100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0100111011 $stream_p $stream_n 
	    return "1" 
	}
	}

	13 { if {$disparity == "1"} {
	    send10b 1100100100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1100101011 $stream_p $stream_n 
	    return "1" 
	}
	}

	14 { if {$disparity == "1"} {
	    send10b 0010110100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0010111011 $stream_p $stream_n 
	    return "1" 
	}
	}

	15 { if {$disparity == "1"} {
	    send10b 1010100100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1010101011 $stream_p $stream_n 
	    return "1" 
	}
	}

	16 { if {$disparity == "1"} {
	    send10b 0110100100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0110101011 $stream_p $stream_n 
	    return "1" 
	}
	}

	17 { if {$disparity == "1"} {
	    send10b 0001011011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1110100100 $stream_p $stream_n 
	    return "1" 
	}
	}

	18 { if {$disparity == "1"} {
	    send10b 0011001011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1100110100 $stream_p $stream_n 
	    return "1" 
	}
	}

	19 { if {$disparity == "1"} {
	    send10b 1001100100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1001101011 $stream_p $stream_n 
	    return "1" 
	}
	}

	1A { if {$disparity == "1"} {
	    send10b 0101100100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0101101011 $stream_p $stream_n 
	    return "1" 
	}
	}

	1B { if {$disparity == "1"} {
	    send10b 0010011011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1101100100 $stream_p $stream_n 
	    return "1" 
	}
	}

	1C { if {$disparity == "1"} {
	    send10b 0011100100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0011101011 $stream_p $stream_n 
	    return "1" 
	}
	}

	1D { if {$disparity == "1"} {
	    send10b 0100011011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1011100100 $stream_p $stream_n 
	    return "1" 
	}
	}

	1E { if {$disparity == "1"} {
	    send10b 1000011011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0111100100 $stream_p $stream_n 
	    return "1" 
	}
	}

	1F { if {$disparity == "1"} {
	    send10b 0101001011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1010110100 $stream_p $stream_n 
	    return "1" 
	}
	}

	20 { if {$disparity == "1"} {
	    send10b 0110001001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1001111001 $stream_p $stream_n 
	    return "1" 
	}
	}

	21 { if {$disparity == "1"} {
	    send10b 1000101001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0111011001 $stream_p $stream_n 
	    return "1" 
	}
	}

	22 { if {$disparity == "1"} {
	    send10b 0100101001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1011011001 $stream_p $stream_n 
	    return "1" 
	}
	}

	23 { if {$disparity == "1"} {
	    send10b 1100011001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1100011001 $stream_p $stream_n 
	    return "1" 
	}
	}

	24 { if {$disparity == "1"} {
	    send10b 0010101001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1101011001 $stream_p $stream_n 
	    return "1" 
	}
	}

	25 { if {$disparity == "1"} {
	    send10b 1010011001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1010011001 $stream_p $stream_n 
	    return "1" 
	}
	}

	26 { if {$disparity == "1"} {
	    send10b 0110011001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0110011001 $stream_p $stream_n 
	    return "1" 
	}
	}

	27 { if {$disparity == "1"} {
	    send10b 0001111001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1110001001 $stream_p $stream_n 
	    return "1" 
	}
	}

	28 { if {$disparity == "1"} {
	    send10b 0001101001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1110011001 $stream_p $stream_n 
	    return "1" 
	}
	}

	29 { if {$disparity == "1"} {
	    send10b 1001011001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1001011001 $stream_p $stream_n 
	    return "1" 
	}
	}

	2A { if {$disparity == "1"} {
	    send10b 0101011001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0101011001 $stream_p $stream_n 
	    return "1" 
	}
	}

	2B { if {$disparity == "1"} {
	    send10b 1101001001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1101001001 $stream_p $stream_n 
	    return "1" 
	}
	}

	2C { if {$disparity == "1"} {
	    send10b 0011011001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0011011001 $stream_p $stream_n 
	    return "1" 
	}
	}

	2D { if {$disparity == "1"} {
	    send10b 1011001001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1011001001 $stream_p $stream_n 
	    return "1" 
	}
	}

	2E { if {$disparity == "1"} {
	    send10b 0111001001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0111001001 $stream_p $stream_n 
	    return "1" 
	}
	}

	2F { if {$disparity == "1"} {
	    send10b 1010001001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0101111001 $stream_p $stream_n 
	    return "1" 
	}
	}

	30 { if {$disparity == "1"} {
	    send10b 1001001001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0110111001 $stream_p $stream_n 
	    return "1" 
	}
	}

	31 { if {$disparity == "1"} {
	    send10b 1000111001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1000111001 $stream_p $stream_n 
	    return "1" 
	}
	}

	32 { if {$disparity == "1"} {
	    send10b 0100111001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0100111001 $stream_p $stream_n 
	    return "1" 
	}
	}

	33 { if {$disparity == "1"} {
	    send10b 1100101001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1100101001 $stream_p $stream_n 
	    return "1" 
	}
	}

	34 { if {$disparity == "1"} {
	    send10b 0010111001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0010111001 $stream_p $stream_n 
	    return "1" 
	}
	}

	35 { if {$disparity == "1"} {
	    send10b 1010101001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1010101001 $stream_p $stream_n 
	    return "1" 
	}
	}

	36 { if {$disparity == "1"} {
	    send10b 0110101001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0110101001 $stream_p $stream_n 
	    return "1" 
	}
	}

	37 { if {$disparity == "1"} {
	    send10b 0001011001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1110101001 $stream_p $stream_n 
	    return "1" 
	}
	}

	38 { if {$disparity == "1"} {
	    send10b 0011001001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1100111001 $stream_p $stream_n 
	    return "1" 
	}
	}

	39 { if {$disparity == "1"} {
	    send10b 1001101001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1001101001 $stream_p $stream_n 
	    return "1" 
	}
	}

	3A { if {$disparity == "1"} {
	    send10b 0101101001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0101101001 $stream_p $stream_n 
	    return "1" 
	}
	}

	3B { if {$disparity == "1"} {
	    send10b 0010011001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1101101001 $stream_p $stream_n 
	    return "1" 
	}
	}

	3C { if {$disparity == "1"} {
	    send10b 0011101001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0011101001 $stream_p $stream_n 
	    return "1" 
	}
	}

	3D { if {$disparity == "1"} {
	    send10b 0100011001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1011101001 $stream_p $stream_n 
	    return "1" 
	}
	}

	3E { if {$disparity == "1"} {
	    send10b 1000011001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0111101001 $stream_p $stream_n 
	    return "1" 
	}
	}

	3F { if {$disparity == "1"} {
	    send10b 0101001001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1010111001 $stream_p $stream_n 
	    return "1" 
	}
	}

	40 { if {$disparity == "1"} {
	    send10b 0110000101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1001110101 $stream_p $stream_n 
	    return "1" 
	}
	}

	41 { if {$disparity == "1"} {
	    send10b 1000100101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0111010101 $stream_p $stream_n 
	    return "1" 
	}
	}

	42 { if {$disparity == "1"} {
	    send10b 0100100101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1011010101 $stream_p $stream_n 
	    return "1" 
	}
	}

	43 { if {$disparity == "1"} {
	    send10b 1100010101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1100010101 $stream_p $stream_n 
	    return "1" 
	}
	}

	44 { if {$disparity == "1"} {
	    send10b 0010100101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1101010101 $stream_p $stream_n 
	    return "1" 
	}
	}

	45 { if {$disparity == "1"} {
	    send10b 1010010101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1010010101 $stream_p $stream_n 
	    return "1" 
	}
	}

	46 { if {$disparity == "1"} {
	    send10b 0110010101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0110010101 $stream_p $stream_n 
	    return "1" 
	}
	}

	47 { if {$disparity == "1"} {
	    send10b 0001110101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1110000101 $stream_p $stream_n 
	    return "1" 
	}
	}

	48 { if {$disparity == "1"} {
	    send10b 0001100101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1110010101 $stream_p $stream_n 
	    return "1" 
	}
	}

	49 { if {$disparity == "1"} {
	    send10b 1001010101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1001010101 $stream_p $stream_n 
	    return "1" 
	}
	}

	4A { if {$disparity == "1"} {
	    send10b 0101010101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0101010101 $stream_p $stream_n 
	    return "1" 
	}
	}

	4B { if {$disparity == "1"} {
	    send10b 1101000101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1101000101 $stream_p $stream_n 
	    return "1" 
	}
	}

	4C { if {$disparity == "1"} {
	    send10b 0011010101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0011010101 $stream_p $stream_n 
	    return "1" 
	}
	}

	4D { if {$disparity == "1"} {
	    send10b 1011000101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1011000101 $stream_p $stream_n 
	    return "1" 
	}
	}

	4E { if {$disparity == "1"} {
	    send10b 0111000101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0111000101 $stream_p $stream_n 
	    return "1" 
	}
	}

	4F { if {$disparity == "1"} {
	    send10b 1010000101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0101110101 $stream_p $stream_n 
	    return "1" 
	}
	}

	50 { if {$disparity == "1"} {
	    send10b 1001000101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0110110101 $stream_p $stream_n 
	    return "1" 
	}
	}

	51 { if {$disparity == "1"} {
	    send10b 1000110101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1000110101 $stream_p $stream_n 
	    return "1" 
	}
	}

	52 { if {$disparity == "1"} {
	    send10b 0100110101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0100110101 $stream_p $stream_n 
	    return "1" 
	}
	}

	53 { if {$disparity == "1"} {
	    send10b 1100100101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1100100101 $stream_p $stream_n 
	    return "1" 
	}
	}

	54 { if {$disparity == "1"} {
	    send10b 0010110101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0010110101 $stream_p $stream_n 
	    return "1" 
	}
	}

	55 { if {$disparity == "1"} {
	    send10b 1010100101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1010100101 $stream_p $stream_n 
	    return "1" 
	}
	}

	56 { if {$disparity == "1"} {
	    send10b 0110100101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0110100101 $stream_p $stream_n 
	    return "1" 
	}
	}

	57 { if {$disparity == "1"} {
	    send10b 0001010101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1110100101 $stream_p $stream_n 
	    return "1" 
	}
	}

	58 { if {$disparity == "1"} {
	    send10b 0011000101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1100110101 $stream_p $stream_n 
	    return "1" 
	}
	}

	59 { if {$disparity == "1"} {
	    send10b 1001100101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1001100101 $stream_p $stream_n 
	    return "1" 
	}
	}

	5A { if {$disparity == "1"} {
	    send10b 0101100101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0101100101 $stream_p $stream_n 
	    return "1" 
	}
	}

	5B { if {$disparity == "1"} {
	    send10b 0010010101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1101100101 $stream_p $stream_n 
	    return "1" 
	}
	}

	5C { if {$disparity == "1"} {
	    send10b 0011100101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0011100101 $stream_p $stream_n 
	    return "1" 
	}
	}

	5D { if {$disparity == "1"} {
	    send10b 0100010101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1011100101 $stream_p $stream_n 
	    return "1" 
	}
	}

	5E { if {$disparity == "1"} {
	    send10b 1000010101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0111100101 $stream_p $stream_n 
	    return "1" 
	}
	}

	5F { if {$disparity == "1"} {
	    send10b 0101000101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1010110101 $stream_p $stream_n 
	    return "1" 
	}
	}

	60 { if {$disparity == "1"} {
	    send10b 0110001100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1001110011 $stream_p $stream_n 
	    return "1" 
	}
	}

	61 { if {$disparity == "1"} {
	    send10b 1000101100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0111010011 $stream_p $stream_n 
	    return "1" 
	}
	}

	62 { if {$disparity == "1"} {
	    send10b 0100101100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1011010011 $stream_p $stream_n 
	    return "1" 
	}
	}

	63 { if {$disparity == "1"} {
	    send10b 1100010011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1100011100 $stream_p $stream_n 
	    return "1" 
	}
	}

	64 { if {$disparity == "1"} {
	    send10b 0010101100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1101010011 $stream_p $stream_n 
	    return "1" 
	}
	}

	65 { if {$disparity == "1"} {
	    send10b 1010010011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1010011100 $stream_p $stream_n 
	    return "1" 
	}
	}

	66 { if {$disparity == "1"} {
	    send10b 0110010011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0110011100 $stream_p $stream_n 
	    return "1" 
	}
	}

	67 { if {$disparity == "1"} {
	    send10b 0001110011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1110001100 $stream_p $stream_n 
	    return "1" 
	}
	}

	68 { if {$disparity == "1"} {
	    send10b 0001101100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1110010011 $stream_p $stream_n 
	    return "1" 
	}
	}

	69 { if {$disparity == "1"} {
	    send10b 1001010011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1001011100 $stream_p $stream_n 
	    return "1" 
	}
	}

	6A { if {$disparity == "1"} {
	    send10b 0101010011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0101011100 $stream_p $stream_n 
	    return "1" 
	}
	}

	6B { if {$disparity == "1"} {
	    send10b 1101000011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1101001100 $stream_p $stream_n 
	    return "1" 
	}
	}

	6C { if {$disparity == "1"} {
	    send10b 0011010011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0011011100 $stream_p $stream_n 
	    return "1" 
	}
	}

	6D { if {$disparity == "1"} {
	    send10b 1011000011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1011001100 $stream_p $stream_n 
	    return "1" 
	}
	}

	6E { if {$disparity == "1"} {
	    send10b 0111000011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0111001100 $stream_p $stream_n 
	    return "1" 
	}
	}

	6F { if {$disparity == "1"} {
	    send10b 1010001100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0101110011 $stream_p $stream_n 
	    return "1" 
	}
	}

	70 { if {$disparity == "1"} {
	    send10b 1001001100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0110110011 $stream_p $stream_n 
	    return "1" 
	}
	}

	71 { if {$disparity == "1"} {
	    send10b 1000110011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1000111100 $stream_p $stream_n 
	    return "1" 
	}
	}

	72 { if {$disparity == "1"} {
	    send10b 0100110011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0100111100 $stream_p $stream_n 
	    return "1" 
	}
	}

	73 { if {$disparity == "1"} {
	    send10b 1100100011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1100101100 $stream_p $stream_n 
	    return "1" 
	}
	}

	74 { if {$disparity == "1"} {
	    send10b 0010110011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0010111100 $stream_p $stream_n 
	    return "1" 
	}
	}

	75 { if {$disparity == "1"} {
	    send10b 1010100011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1010101100 $stream_p $stream_n 
	    return "1" 
	}
	}

	76 { if {$disparity == "1"} {
	    send10b 0110100011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0110101100 $stream_p $stream_n 
	    return "1" 
	}
	}

	77 { if {$disparity == "1"} {
	    send10b 0001011100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1110100011 $stream_p $stream_n 
	    return "1" 
	}
	}

	78 { if {$disparity == "1"} {
	    send10b 0011001100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1100110011 $stream_p $stream_n 
	    return "1" 
	}
	}

	79 { if {$disparity == "1"} {
	    send10b 1001100011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1001101100 $stream_p $stream_n 
	    return "1" 
	}
	}

	7A { if {$disparity == "1"} {
	    send10b 0101100011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0101101100 $stream_p $stream_n 
	    return "1" 
	}
	}

	7B { if {$disparity == "1"} {
	    send10b 0010011100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1101100011 $stream_p $stream_n 
	    return "1" 
	}
	}

	7C { if {$disparity == "1"} {
	    send10b 0011100011 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0011101100 $stream_p $stream_n 
	    return "1" 
	}
	}

	7D { if {$disparity == "1"} {
	    send10b 0100011100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1011100011 $stream_p $stream_n 
	    return "1" 
	}
	}

	7E { if {$disparity == "1"} {
	    send10b 1000011100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0111100011 $stream_p $stream_n 
	    return "1" 
	}
	}

	7F { if {$disparity == "1"} {
	    send10b 0101001100 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1010110011 $stream_p $stream_n 
	    return "1" 
	}
	}

	80 { if {$disparity == "1"} {
	    send10b 0110001101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1001110010 $stream_p $stream_n 
	    return "1" 
	}
	}

	81 { if {$disparity == "1"} {
	    send10b 1000101101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0111010010 $stream_p $stream_n 
	    return "1" 
	}
	}

	82 { if {$disparity == "1"} {
	    send10b 0100101101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1011010010 $stream_p $stream_n 
	    return "1" 
	}
	}

	83 { if {$disparity == "1"} {
	    send10b 1100010010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1100011101 $stream_p $stream_n 
	    return "1" 
	}
	}

	84 { if {$disparity == "1"} {
	    send10b 0010101101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1101010010 $stream_p $stream_n 
	    return "1" 
	}
	}

	85 { if {$disparity == "1"} {
	    send10b 1010010010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1010011101 $stream_p $stream_n 
	    return "1" 
	}
	}

	86 { if {$disparity == "1"} {
	    send10b 0110010010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0110011101 $stream_p $stream_n 
	    return "1" 
	}
	}

	87 { if {$disparity == "1"} {
	    send10b 0001110010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1110001101 $stream_p $stream_n 
	    return "1" 
	}
	}

	88 { if {$disparity == "1"} {
	    send10b 0001101101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1110010010 $stream_p $stream_n 
	    return "1" 
	}
	}

	89 { if {$disparity == "1"} {
	    send10b 1001010010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1001011101 $stream_p $stream_n 
	    return "1" 
	}
	}

	8A { if {$disparity == "1"} {
	    send10b 0101010010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0101011101 $stream_p $stream_n 
	    return "1" 
	}
	}

	8B { if {$disparity == "1"} {
	    send10b 1101000010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1101001101 $stream_p $stream_n 
	    return "1" 
	}
	}

	8C { if {$disparity == "1"} {
	    send10b 0011010010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0011011101 $stream_p $stream_n 
	    return "1" 
	}
	}

	8D { if {$disparity == "1"} {
	    send10b 1011000010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1011001101 $stream_p $stream_n 
	    return "1" 
	}
	}

	8E { if {$disparity == "1"} {
	    send10b 0111000010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0111001101 $stream_p $stream_n 
	    return "1" 
	}
	}

	8F { if {$disparity == "1"} {
	    send10b 1010001101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0101110010 $stream_p $stream_n 
	    return "1" 
	}
	}

	90 { if {$disparity == "1"} {
	    send10b 1001001101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0110110010 $stream_p $stream_n 
	    return "1" 
	}
	}

	91 { if {$disparity == "1"} {
	    send10b 1000110010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1000111101 $stream_p $stream_n 
	    return "1" 
	}
	}

	92 { if {$disparity == "1"} {
	    send10b 0100110010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0100111101 $stream_p $stream_n 
	    return "1" 
	}
	}

	93 { if {$disparity == "1"} {
	    send10b 1100100010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1100101101 $stream_p $stream_n 
	    return "1" 
	}
	}

	94 { if {$disparity == "1"} {
	    send10b 0010110010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0010111101 $stream_p $stream_n 
	    return "1" 
	}
	}

	95 { if {$disparity == "1"} {
	    send10b 1010100010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1010101101 $stream_p $stream_n 
	    return "1" 
	}
	}

	96 { if {$disparity == "1"} {
	    send10b 0110100010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0110101101 $stream_p $stream_n 
	    return "1" 
	}
	}

	97 { if {$disparity == "1"} {
	    send10b 0001011101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1110100010 $stream_p $stream_n 
	    return "1" 
	}
	}

	98 { if {$disparity == "1"} {
	    send10b 0011001101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1100110010 $stream_p $stream_n 
	    return "1" 
	}
	}

	99 { if {$disparity == "1"} {
	    send10b 1001100010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1001101101 $stream_p $stream_n 
	    return "1" 
	}
	}

	9A { if {$disparity == "1"} {
	    send10b 0101100010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0101101101 $stream_p $stream_n 
	    return "1" 
	}
	}

	9B { if {$disparity == "1"} {
	    send10b 0010011101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1101100010 $stream_p $stream_n 
	    return "1" 
	}
	}

	9C { if {$disparity == "1"} {
	    send10b 0011100010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0011101101 $stream_p $stream_n 
	    return "1" 
	}
	}

	9D { if {$disparity == "1"} {
	    send10b 0100011101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1011100010 $stream_p $stream_n 
	    return "1" 
	}
	}

	9E { if {$disparity == "1"} {
	    send10b 1000011101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0111100010 $stream_p $stream_n 
	    return "1" 
	}
	}

	9F { if {$disparity == "1"} {
	    send10b 0101001101 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1010110010 $stream_p $stream_n 
	    return "1" 
	}
	}

	A0 { if {$disparity == "1"} {
	    send10b 0110001010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1001111010 $stream_p $stream_n 
	    return "1" 
	}
	}

	A1 { if {$disparity == "1"} {
	    send10b 1000101010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0111011010 $stream_p $stream_n 
	    return "1" 
	}
	}

	A2 { if {$disparity == "1"} {
	    send10b 0100101010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1011011010 $stream_p $stream_n 
	    return "1" 
	}
	}

	A3 { if {$disparity == "1"} {
	    send10b 1100011010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1100011010 $stream_p $stream_n 
	    return "1" 
	}
	}

	A4 { if {$disparity == "1"} {
	    send10b 0010101010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1101011010 $stream_p $stream_n 
	    return "1" 
	}
	}

	A5 { if {$disparity == "1"} {
	    send10b 1010011010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1010011010 $stream_p $stream_n 
	    return "1" 
	}
	}

	A6 { if {$disparity == "1"} {
	    send10b 0110011010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0110011010 $stream_p $stream_n 
	    return "1" 
	}
	}

	A7 { if {$disparity == "1"} {
	    send10b 0001111010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1110001010 $stream_p $stream_n 
	    return "1" 
	}
	}

	A8 { if {$disparity == "1"} {
	    send10b 0001101010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1110011010 $stream_p $stream_n 
	    return "1" 
	}
	}

	A9 { if {$disparity == "1"} {
	    send10b 1001011010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1001011010 $stream_p $stream_n 
	    return "1" 
	}
	}

	AA { if {$disparity == "1"} {
	    send10b 0101011010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0101011010 $stream_p $stream_n 
	    return "1" 
	}
	}

	AB { if {$disparity == "1"} {
	    send10b 1101001010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1101001010 $stream_p $stream_n 
	    return "1" 
	}
	}

	AC { if {$disparity == "1"} {
	    send10b 0011011010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0011011010 $stream_p $stream_n 
	    return "1" 
	}
	}

	AD { if {$disparity == "1"} {
	    send10b 1011001010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1011001010 $stream_p $stream_n 
	    return "1" 
	}
	}

	AE { if {$disparity == "1"} {
	    send10b 0111001010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0111001010 $stream_p $stream_n 
	    return "1" 
	}
	}

	AF { if {$disparity == "1"} {
	    send10b 1010001010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0101111010 $stream_p $stream_n 
	    return "1" 
	}
	}

	B0 { if {$disparity == "1"} {
	    send10b 1001001010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0110111010 $stream_p $stream_n 
	    return "1" 
	}
	}

	B1 { if {$disparity == "1"} {
	    send10b 1000111010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1000111010 $stream_p $stream_n 
	    return "1" 
	}
	}

	B2 { if {$disparity == "1"} {
	    send10b 0100111010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0100111010 $stream_p $stream_n 
	    return "1" 
	}
	}

	B3 { if {$disparity == "1"} {
	    send10b 1100101010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1100101010 $stream_p $stream_n 
	    return "1" 
	}
	}

	B4 { if {$disparity == "1"} {
	    send10b 0010111010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0010111010 $stream_p $stream_n 
	    return "1" 
	}
	}

	B5 { if {$disparity == "1"} {
	    send10b 1010101010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1010101010 $stream_p $stream_n 
	    return "1" 
	}
	}

	B6 { if {$disparity == "1"} {
	    send10b 0110101010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0110101010 $stream_p $stream_n 
	    return "1" 
	}
	}

	B7 { if {$disparity == "1"} {
	    send10b 0001011010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1110101010 $stream_p $stream_n 
	    return "1" 
	}
	}

	B8 { if {$disparity == "1"} {
	    send10b 0011001010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1100111010 $stream_p $stream_n 
	    return "1" 
	}
	}

	B9 { if {$disparity == "1"} {
	    send10b 1001101010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1001101010 $stream_p $stream_n 
	    return "1" 
	}
	}

	BA { if {$disparity == "1"} {
	    send10b 0101101010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0101101010 $stream_p $stream_n 
	    return "1" 
	}
	}

	BB { if {$disparity == "1"} {
	    send10b 0010011010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1101101010 $stream_p $stream_n 
	    return "1" 
	}
	}

	BC { if {$disparity == "1"} {
	    send10b 0011101010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0011101010 $stream_p $stream_n 
	    return "1" 
	}
	}

	BD { if {$disparity == "1"} {
	    send10b 0100011010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1011101010 $stream_p $stream_n 
	    return "1" 
	}
	}

	BE { if {$disparity == "1"} {
	    send10b 1000011010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0111101010 $stream_p $stream_n 
	    return "1" 
	}
	}

	BF { if {$disparity == "1"} {
	    send10b 0101001010 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1010111010 $stream_p $stream_n 
	    return "1" 
	}
	}

	C0 { if {$disparity == "1"} {
	    send10b 0110000110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1001110110 $stream_p $stream_n 
	    return "1" 
	}
	}

	C1 { if {$disparity == "1"} {
	    send10b 1000100110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0111010110 $stream_p $stream_n 
	    return "1" 
	}
	}

	C2 { if {$disparity == "1"} {
	    send10b 0100100110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1011010110 $stream_p $stream_n 
	    return "1" 
	}
	}

	C3 { if {$disparity == "1"} {
	    send10b 1100010110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1100010110 $stream_p $stream_n 
	    return "1" 
	}
	}

	C4 { if {$disparity == "1"} {
	    send10b 0010100110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1101010110 $stream_p $stream_n 
	    return "1" 
	}
	}

	C5 { if {$disparity == "1"} {
	    send10b 1010010110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1010010110 $stream_p $stream_n 
	    return "1" 
	}
	}

	C6 { if {$disparity == "1"} {
	    send10b 0110010110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0110010110 $stream_p $stream_n 
	    return "1" 
	}
	}

	C7 { if {$disparity == "1"} {
	    send10b 0001110110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1110000110 $stream_p $stream_n 
	    return "1" 
	}
	}

	C8 { if {$disparity == "1"} {
	    send10b 0001100110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1110010110 $stream_p $stream_n 
	    return "1" 
	}
	}

	C9 { if {$disparity == "1"} {
	    send10b 1001010110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1001010110 $stream_p $stream_n 
	    return "1" 
	}
	}

	CA { if {$disparity == "1"} {
	    send10b 0101010110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0101010110 $stream_p $stream_n 
	    return "1" 
	}
	}

	CB { if {$disparity == "1"} {
	    send10b 1101000110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1101000110 $stream_p $stream_n 
	    return "1" 
	}
	}

	CC { if {$disparity == "1"} {
	    send10b 0011010110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0011010110 $stream_p $stream_n 
	    return "1" 
	}
	}

	CD { if {$disparity == "1"} {
	    send10b 1011000110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1011000110 $stream_p $stream_n 
	    return "1" 
	}
	}

	CE { if {$disparity == "1"} {
	    send10b 0111000110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0111000110 $stream_p $stream_n 
	    return "1" 
	}
	}

	CF { if {$disparity == "1"} {
	    send10b 1010000110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0101110110 $stream_p $stream_n 
	    return "1" 
	}
	}

	D0 { if {$disparity == "1"} {
	    send10b 1001000110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0110110110 $stream_p $stream_n 
	    return "1" 
	}
	}

	D1 { if {$disparity == "1"} {
	    send10b 1000110110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1000110110 $stream_p $stream_n 
	    return "1" 
	}
	}

	D2 { if {$disparity == "1"} {
	    send10b 0100110110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0100110110 $stream_p $stream_n 
	    return "1" 
	}
	}

	D3 { if {$disparity == "1"} {
	    send10b 1100100110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1100100110 $stream_p $stream_n 
	    return "1" 
	}
	}

	D4 { if {$disparity == "1"} {
	    send10b 0010110110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0010110110 $stream_p $stream_n 
	    return "1" 
	}
	}

	D5 { if {$disparity == "1"} {
	    send10b 1010100110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1010100110 $stream_p $stream_n 
	    return "1" 
	}
	}

	D6 { if {$disparity == "1"} {
	    send10b 0110100110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0110100110 $stream_p $stream_n 
	    return "1" 
	}
	}

	D7 { if {$disparity == "1"} {
	    send10b 0001010110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1110100110 $stream_p $stream_n 
	    return "1" 
	}
	}

	D8 { if {$disparity == "1"} {
	    send10b 0011000110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1100110110 $stream_p $stream_n 
	    return "1" 
	}
	}

	D9 { if {$disparity == "1"} {
	    send10b 1001100110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1001100110 $stream_p $stream_n 
	    return "1" 
	}
	}

	DA { if {$disparity == "1"} {
	    send10b 0101100110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0101100110 $stream_p $stream_n 
	    return "1" 
	}
	}

	DB { if {$disparity == "1"} {
	    send10b 0010010110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1101100110 $stream_p $stream_n 
	    return "1" 
	}
	}

	DC { if {$disparity == "1"} {
	    send10b 0011100110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0011100110 $stream_p $stream_n 
	    return "1" 
	}
	}

	DD { if {$disparity == "1"} {
	    send10b 0100010110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1011100110 $stream_p $stream_n 
	    return "1" 
	}
	}

	DE { if {$disparity == "1"} {
	    send10b 1000010110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0111100110 $stream_p $stream_n 
	    return "1" 
	}
	}

	DF { if {$disparity == "1"} {
	    send10b 0101000110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1010110110 $stream_p $stream_n 
	    return "1" 
	}
	}

	E0 { if {$disparity == "1"} {
	    send10b 0110001110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1001110001 $stream_p $stream_n 
	    return "1" 
	}
	}

	E1 { if {$disparity == "1"} {
	    send10b 1000101110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0111010001 $stream_p $stream_n 
	    return "1" 
	}
	}

	E2 { if {$disparity == "1"} {
	    send10b 0100101110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1011010001 $stream_p $stream_n 
	    return "1" 
	}
	}

	E3 { if {$disparity == "1"} {
	    send10b 1100010001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1100011110 $stream_p $stream_n 
	    return "1" 
	}
	}

	E4 { if {$disparity == "1"} {
	    send10b 0010101110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1101010001 $stream_p $stream_n 
	    return "1" 
	}
	}

	E5 { if {$disparity == "1"} {
	    send10b 1010010001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1010011110 $stream_p $stream_n 
	    return "1" 
	}
	}

	E6 { if {$disparity == "1"} {
	    send10b 0110010001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0110011110 $stream_p $stream_n 
	    return "1" 
	}
	}

	E7 { if {$disparity == "1"} {
	    send10b 0001110001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1110001110 $stream_p $stream_n 
	    return "1" 
	}
	}

	E8 { if {$disparity == "1"} {
	    send10b 0001101110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1110010001 $stream_p $stream_n 
	    return "1" 
	}
	}

	E9 { if {$disparity == "1"} {
	    send10b 1001010001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1001011110 $stream_p $stream_n 
	    return "1" 
	}
	}

	EA { if {$disparity == "1"} {
	    send10b 0101010001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0101011110 $stream_p $stream_n 
	    return "1" 
	}
	}

	EB { if {$disparity == "1"} {
	    send10b 1101001000 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1101001110 $stream_p $stream_n 
	    return "1" 
	}
	}

	EC { if {$disparity == "1"} {
	    send10b 0011010001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0011011110 $stream_p $stream_n 
	    return "1" 
	}
	}

	ED { if {$disparity == "1"} {
	    send10b 1011001000 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1011001110 $stream_p $stream_n 
	    return "1" 
	}
	}

	EE { if {$disparity == "1"} {
	    send10b 0111001000 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0111001110 $stream_p $stream_n 
	    return "1" 
	}
	}

	EF { if {$disparity == "1"} {
	    send10b 1010001110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0101110001 $stream_p $stream_n 
	    return "1" 
	}
	}

	F0 { if {$disparity == "1"} {
	    send10b 1001001110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0110110001 $stream_p $stream_n 
	    return "1" 
	}
	}

	F1 { if {$disparity == "1"} {
	    send10b 1000110001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1000110111 $stream_p $stream_n 
	    return "1" 
	}
	}

	F2 { if {$disparity == "1"} {
	    send10b 0100110001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0100110111 $stream_p $stream_n 
	    return "1" 
	}
	}

	F3 { if {$disparity == "1"} {
	    send10b 1100100001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1100101110 $stream_p $stream_n 
	    return "1" 
	}
	}

	F4 { if {$disparity == "1"} {
	    send10b 0010110001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0010110111 $stream_p $stream_n 
	    return "1" 
	}
	}

	F5 { if {$disparity == "1"} {
	    send10b 1010100001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1010101110 $stream_p $stream_n 
	    return "1" 
	}
	}

	F6 { if {$disparity == "1"} {
	    send10b 0110100001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0110101110 $stream_p $stream_n 
	    return "1" 
	}
	}

	F7 { if {$disparity == "1"} {
	    send10b 0001011110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1110100001 $stream_p $stream_n 
	    return "1" 
	}
	}

	F8 { if {$disparity == "1"} {
	    send10b 0011001110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1100110001 $stream_p $stream_n 
	    return "1" 
	}
	}

	F9 { if {$disparity == "1"} {
	    send10b 1001100001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1001101110 $stream_p $stream_n 
	    return "1" 
	}
	}

	FA { if {$disparity == "1"} {
	    send10b 0101100001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0101101110 $stream_p $stream_n 
	    return "1" 
	}
	}

	FB { if {$disparity == "1"} {
	    send10b 0010011110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1101100001 $stream_p $stream_n 
	    return "1" 
	}
	}

	FC { if {$disparity == "1"} {
	    send10b 0011100001 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 0011101110 $stream_p $stream_n 
	    return "1" 
	}
	}

	FD { if {$disparity == "1"} {
	    send10b 0100011110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1011100001 $stream_p $stream_n 
	    return "1" 
	}
	}

	FE { if {$disparity == "1"} {
	    send10b 1000011110 $stream_p $stream_n 
	    return "1"
	} else { 
	    send10b 0111100001 $stream_p $stream_n 
	    return "-1" 
	}
	}

	FF { if {$disparity == "1"} {
	    send10b 0101001110 $stream_p $stream_n 
	    return "-1"
	} else { 
	    send10b 1010110001 $stream_p $stream_n 
	    return "1" 
	}
	}


	default {
	    send10b 0000000000 $stream_p $stream_n
	}
    }	
}
proc gen_8b10b_control_code {code disparity stream_p stream_n} {
    case $code {
	BC {
	    if {$disparity == "1"} {
		send10b 1100000101 $stream_p $stream_n
		return "-1"
	    } else {
		send10b 0011111010 $stream_p $stream_n
		return "1"
	    }
	}
	3C {
	    if {$disparity == "1"} {
		send10b 1100000110 $stream_p $stream_n
		return "-1"
	    } else {
		send10b 0011111001 $stream_p $stream_n
		return "11"
	    }
	}
	default {
	    send10b 0000000000 $stream_p $stream_n
	}
    }	
}





