----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

use work.TTC_chB.all;
use work.BC_tuning.all;

package TTC  is
  type bpmc_symbol_t is array (0 to 3) of unsigned(3 downto 0);

  type L1A_triggers is record
    total          : unsigned(31 downto 0);
    periodic       : unsigned(31 downto 0);
    random         : unsigned(31 downto 0);
    software       : unsigned(31 downto 0);
    software_on_bc : unsigned(31 downto 0);
    external       : unsigned(31 downto 0);
    tts            : unsigned(31 downto 0);
  end record L1A_triggers;

  type TTC_Monitor is record
    chB_full                         : std_logic;
    BX_counter                       : unsigned(11 downto 0);
    orbit_number                     : unsigned(15 downto 0);
    Triggers                         : L1A_triggers;
    Skipped_Triggers                 : L1A_triggers;
    TTC_overflow_count               : unsigned(7 downto 0);
    TTC_underflow_count              : unsigned(7 downto 0);
    bpmc_histo                       : bpmc_symbol_t;
  end record;

  type TTC_Control is record
    bc_rollovers                     : BC_tune_rollover;
    
    chB_message                      : Message;
    chB_wr                           : std_logic;
  
    enable_BCX_reset                 : std_logic;
    enable_TTS                       : std_logic;
    
    bc_start_triggers                : std_logic_vector(11 downto 0);
    bc_end_triggers                  : std_logic_vector(11 downto 0);

    enable_trigger_rule              : std_logic_vector(3 downto 0);
    
    enable_periodic                  : std_logic;
    enable_random                    : std_logic; 
    enable_external                  : std_logic;
                                     
    period                           : unsigned(15 downto 0);
    random_timescale                 : unsigned(15 downto 0);
    software_trigger                 : std_logic;
    software_on_bc_trigger           : std_logic;
    software_bcn                     : std_logic_vector(11 downto 0);

    chB_L1A_trigger                  : std_logic;
    
  end record;

  constant TTC_CONTROL_DEFAULT : TTC_Control := (bc_rollovers => DEFAULT_TUNES,
                                                 chB_message => BLANK_TTC_CHB_MESSAGE,
                                                 chB_wr => '0', 
                                                 enable_BCX_reset => '1', 
                                                 enable_TTS => '0',
                                                 enable_trigger_rule => X"F",
                                                 bc_start_triggers => x"000",
                                                 bc_end_triggers => x"DEB",
                                                 enable_periodic  => '0',
                                                 enable_random => '0',
                                                 enable_external => '0',
                                                 period => x"0000",
                                                 random_timescale => x"0000", -- random timescale
                                                 software_trigger => '0',   -- software trigger
                                                 software_on_bc_trigger => '0',   -- software on BC trigger
                                                 software_bcn => X"FFF",  -- default software
                                                                          -- on BC trigger BC
                                                 chB_L1A_trigger => '0');    -- coordinated chb +
                                                                             -- L1a trigger
 
end TTC;
