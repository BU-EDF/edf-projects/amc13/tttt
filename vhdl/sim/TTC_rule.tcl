restart
source vhdl/sim/UART_helper.tcl

#set initial conditions for UART
isim force add {/top/UART_rx} 1

#set clock on /top/clk
isim force add {/top/clk_local_in} 1 -radix bin -value 0 -radix bin -time 12.5 ns -repeat 25 ns

#get random number generator into a useful state
#isim force /top/TTC_Interface_1/Trigger_Generator_1/urand_1/m1/U0/ 

run 100 us

isim force add {/top/reg_addr} 20 -radix hex
run 100ns
isim force add {/top/reg_data_out} 0DEA0000000F1000 -radix hex
isim force add {/top/reg_wr} 1 -radix bin
run 25ns
isim force add {/top/reg_wr} 0 -radix bin
isim force add {/top/reg_addr} 21 -radix hex
run 100ns
isim force add {/top/reg_wr} 1 -radix bin
isim force add {/top/reg_data_out} 0100000000000001 -radix hex
run 25ns
isim force add {/top/reg_wr} 0 -radix bin

run 500 us
isim force add {/top/reg_addr} 20 -radix hex
run 100ns
isim force add {/top/reg_data_out} 0DEA000000071000 -radix hex
isim force add {/top/reg_wr} 1 -radix bin
run 25ns
isim force add {/top/reg_wr} 0 -radix bin

run 500 us
isim force add {/top/reg_addr} 20 -radix hex
run 100ns
isim force add {/top/reg_data_out} 0DEA000000031000 -radix hex
isim force add {/top/reg_wr} 1 -radix bin
run 25ns
isim force add {/top/reg_wr} 0 -radix bin

run 500 us
isim force add {/top/reg_addr} 20 -radix hex
run 100ns
isim force add {/top/reg_data_out} 0DEA000000011000 -radix hex
isim force add {/top/reg_wr} 1 -radix bin
run 25ns
isim force add {/top/reg_wr} 0 -radix bin

run 500 us
isim force add {/top/reg_addr} 20 -radix hex
run 100ns
isim force add {/top/reg_data_out} 0DEA000000001000 -radix hex
isim force add {/top/reg_wr} 1 -radix bin
run 25ns
isim force add {/top/reg_wr} 0 -radix bin

run 1ms
