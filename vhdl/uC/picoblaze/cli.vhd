--
-------------------------------------------------------------------------------------------
-- Copyright � 2010-2013, Xilinx, Inc.
-- This file contains confidential and proprietary information of Xilinx, Inc. and is
-- protected under U.S. and international copyright and other intellectual property laws.
-------------------------------------------------------------------------------------------
--
-- Disclaimer:
-- This disclaimer is not a license and does not grant any rights to the materials
-- distributed herewith. Except as otherwise provided in a valid license issued to
-- you by Xilinx, and to the maximum extent permitted by applicable law: (1) THESE
-- MATERIALS ARE MADE AVAILABLE "AS IS" AND WITH ALL FAULTS, AND XILINX HEREBY
-- DISCLAIMS ALL WARRANTIES AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY,
-- INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT,
-- OR FITNESS FOR ANY PARTICULAR PURPOSE; and (2) Xilinx shall not be liable
-- (whether in contract or tort, including negligence, or under any other theory
-- of liability) for any loss or damage of any kind or nature related to, arising
-- under or in connection with these materials, including for any direct, or any
-- indirect, special, incidental, or consequential loss or damage (including loss
-- of data, profits, goodwill, or any type of loss or damage suffered as a result
-- of any action brought by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the possibility of the same.
--
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-safe, or for use in any
-- application requiring fail-safe performance, such as life-support or safety
-- devices or systems, Class III medical devices, nuclear facilities, applications
-- related to the deployment of airbags, or any other applications that could lead
-- to death, personal injury, or severe property or environmental damage
-- (individually and collectively, "Critical Applications"). Customer assumes the
-- sole risk and liability of any use of Xilinx products in Critical Applications,
-- subject only to applicable laws and regulations governing limitations on product
-- liability.
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS FILE AT ALL TIMES.
--
-------------------------------------------------------------------------------------------
--
--
-- Definition of a program memory for KCPSM6 including generic parameters for the 
-- convenient selection of device family, program memory size and the ability to include 
-- the JTAG Loader hardware for rapid software development.
--
-- This file is primarily for use during code development and it is recommended that the 
-- appropriate simplified program memory definition be used in a final production design. 
--
--    Generic                  Values             Comments
--    Parameter                Supported
--  
--    C_FAMILY                 "S6"               Spartan-6 device
--                             "V6"               Virtex-6 device
--                             "7S"               7-Series device 
--                                                  (Artix-7, Kintex-7, Virtex-7 or Zynq)
--
--    C_RAM_SIZE_KWORDS        1, 2 or 4          Size of program memory in K-instructions
--
--    C_JTAG_LOADER_ENABLE     0 or 1             Set to '1' to include JTAG Loader
--
-- Notes
--
-- If your design contains MULTIPLE KCPSM6 instances then only one should have the 
-- JTAG Loader enabled at a time (i.e. make sure that C_JTAG_LOADER_ENABLE is only set to 
-- '1' on one instance of the program memory). Advanced users may be interested to know 
-- that it is possible to connect JTAG Loader to multiple memories and then to use the 
-- JTAG Loader utility to specify which memory contents are to be modified. However, 
-- this scheme does require some effort to set up and the additional connectivity of the 
-- multiple BRAMs can impact the placement, routing and performance of the complete 
-- design. Please contact the author at Xilinx for more detailed information. 
--
-- Regardless of the size of program memory specified by C_RAM_SIZE_KWORDS, the complete 
-- 12-bit address bus is connected to KCPSM6. This enables the generic to be modified 
-- without requiring changes to the fundamental hardware definition. However, when the 
-- program memory is 1K then only the lower 10-bits of the address are actually used and 
-- the valid address range is 000 to 3FF hex. Likewise, for a 2K program only the lower 
-- 11-bits of the address are actually used and the valid address range is 000 to 7FF hex.
--
-- Programs are stored in Block Memory (BRAM) and the number of BRAM used depends on the 
-- size of the program and the device family. 
--
-- In a Spartan-6 device a BRAM is capable of holding 1K instructions. Hence a 2K program 
-- will require 2 BRAMs to be used and a 4K program will require 4 BRAMs to be used. It 
-- should be noted that a 4K program is not such a natural fit in a Spartan-6 device and 
-- the implementation also requires a small amount of logic resulting in slightly lower 
-- performance. A Spartan-6 BRAM can also be split into two 9k-bit memories suggesting 
-- that a program containing up to 512 instructions could be implemented. However, there 
-- is a silicon errata which makes this unsuitable and therefore it is not supported by 
-- this file.
--
-- In a Virtex-6 or any 7-Series device a BRAM is capable of holding 2K instructions so 
-- obviously a 2K program requires only a single BRAM. Each BRAM can also be divided into 
-- 2 smaller memories supporting programs of 1K in half of a 36k-bit BRAM (generally 
-- reported as being an 18k-bit BRAM). For a program of 4K instructions, 2 BRAMs are used.
--
--
-- Program defined by 'Z:\home\dan\work\AMC13\TTTT\firmware\SimpleTTTT\trunk\vhdl\uC\picoblaze\cli.psm'.
--
-- Generated by KCPSM6 Assembler: 30 Aug 2022 - 09:59:08. 
--
-- Assembler used ROM_form template: ROM_form_JTAGLoader_14March13.vhd
--
-- Standard IEEE libraries
--
--
package jtag_loader_pkg is
 function addr_width_calc (size_in_k: integer) return integer;
end jtag_loader_pkg;
--
package body jtag_loader_pkg is
  function addr_width_calc (size_in_k: integer) return integer is
   begin
    if (size_in_k = 1) then return 10;
      elsif (size_in_k = 2) then return 11;
      elsif (size_in_k = 4) then return 12;
      else report "Invalid BlockRAM size. Please set to 1, 2 or 4 K words." severity FAILURE;
    end if;
    return 0;
  end function addr_width_calc;
end package body;
--
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.jtag_loader_pkg.ALL;
--
-- The Unisim Library is used to define Xilinx primitives. It is also used during
-- simulation. The source can be viewed at %XILINX%\vhdl\src\unisims\unisim_VCOMP.vhd
--  
library unisim;
use unisim.vcomponents.all;
--
--
entity cli is
  generic(             C_FAMILY : string := "S6"; 
              C_RAM_SIZE_KWORDS : integer := 1;
           C_JTAG_LOADER_ENABLE : integer := 0);
  Port (      address : in std_logic_vector(11 downto 0);
          instruction : out std_logic_vector(17 downto 0);
               enable : in std_logic;
                  rdl : out std_logic;                    
                  clk : in std_logic);
  end cli;
--
architecture low_level_definition of cli is
--
signal       address_a : std_logic_vector(15 downto 0);
signal        pipe_a11 : std_logic;
signal       data_in_a : std_logic_vector(35 downto 0);
signal      data_out_a : std_logic_vector(35 downto 0);
signal    data_out_a_l : std_logic_vector(35 downto 0);
signal    data_out_a_h : std_logic_vector(35 downto 0);
signal   data_out_a_ll : std_logic_vector(35 downto 0);
signal   data_out_a_lh : std_logic_vector(35 downto 0);
signal   data_out_a_hl : std_logic_vector(35 downto 0);
signal   data_out_a_hh : std_logic_vector(35 downto 0);
signal       address_b : std_logic_vector(15 downto 0);
signal       data_in_b : std_logic_vector(35 downto 0);
signal     data_in_b_l : std_logic_vector(35 downto 0);
signal    data_in_b_ll : std_logic_vector(35 downto 0);
signal    data_in_b_hl : std_logic_vector(35 downto 0);
signal      data_out_b : std_logic_vector(35 downto 0);
signal    data_out_b_l : std_logic_vector(35 downto 0);
signal   data_out_b_ll : std_logic_vector(35 downto 0);
signal   data_out_b_hl : std_logic_vector(35 downto 0);
signal     data_in_b_h : std_logic_vector(35 downto 0);
signal    data_in_b_lh : std_logic_vector(35 downto 0);
signal    data_in_b_hh : std_logic_vector(35 downto 0);
signal    data_out_b_h : std_logic_vector(35 downto 0);
signal   data_out_b_lh : std_logic_vector(35 downto 0);
signal   data_out_b_hh : std_logic_vector(35 downto 0);
signal        enable_b : std_logic;
signal           clk_b : std_logic;
signal            we_b : std_logic_vector(7 downto 0);
signal          we_b_l : std_logic_vector(3 downto 0);
signal          we_b_h : std_logic_vector(3 downto 0);
-- 
signal       jtag_addr : std_logic_vector(11 downto 0);
signal         jtag_we : std_logic;
signal       jtag_we_l : std_logic;
signal       jtag_we_h : std_logic;
signal        jtag_clk : std_logic;
signal        jtag_din : std_logic_vector(17 downto 0);
signal       jtag_dout : std_logic_vector(17 downto 0);
signal     jtag_dout_1 : std_logic_vector(17 downto 0);
signal         jtag_en : std_logic_vector(0 downto 0);
-- 
signal picoblaze_reset : std_logic_vector(0 downto 0);
signal         rdl_bus : std_logic_vector(0 downto 0);
--
constant BRAM_ADDRESS_WIDTH  : integer := addr_width_calc(C_RAM_SIZE_KWORDS);
--
--
component jtag_loader_6
generic(                C_JTAG_LOADER_ENABLE : integer := 1;
                                    C_FAMILY : string  := "V6";
                             C_NUM_PICOBLAZE : integer := 1;
                       C_BRAM_MAX_ADDR_WIDTH : integer := 10;
          C_PICOBLAZE_INSTRUCTION_DATA_WIDTH : integer := 18;
                                C_JTAG_CHAIN : integer := 2;
                              C_ADDR_WIDTH_0 : integer := 10;
                              C_ADDR_WIDTH_1 : integer := 10;
                              C_ADDR_WIDTH_2 : integer := 10;
                              C_ADDR_WIDTH_3 : integer := 10;
                              C_ADDR_WIDTH_4 : integer := 10;
                              C_ADDR_WIDTH_5 : integer := 10;
                              C_ADDR_WIDTH_6 : integer := 10;
                              C_ADDR_WIDTH_7 : integer := 10);
port(              picoblaze_reset : out std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
                           jtag_en : out std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
                          jtag_din : out STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                         jtag_addr : out STD_LOGIC_VECTOR(C_BRAM_MAX_ADDR_WIDTH-1 downto 0);
                          jtag_clk : out std_logic;
                           jtag_we : out std_logic;
                       jtag_dout_0 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_1 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_2 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_3 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_4 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_5 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_6 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
                       jtag_dout_7 : in STD_LOGIC_VECTOR(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0));
end component;
--
begin
  --
  --  
  ram_1k_generate : if (C_RAM_SIZE_KWORDS = 1) generate
 
    s6: if (C_FAMILY = "S6") generate 
      --
      address_a(13 downto 0) <= address(9 downto 0) & "0000";
      instruction <= data_out_a(33 downto 32) & data_out_a(15 downto 0);
      data_in_a <= "0000000000000000000000000000000000" & address(11 downto 10);
      jtag_dout <= data_out_b(33 downto 32) & data_out_b(15 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b <= "00" & data_out_b(33 downto 32) & "0000000000000000" & data_out_b(15 downto 0);
        address_b(13 downto 0) <= "00000000000000";
        we_b(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b <= "00" & jtag_din(17 downto 16) & "0000000000000000" & jtag_din(15 downto 0);
        address_b(13 downto 0) <= jtag_addr(9 downto 0) & "0000";
        we_b(3 downto 0) <= jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom: RAMB16BWER
      generic map ( DATA_WIDTH_A => 18,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 18,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D004900024BB0081",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"D20100020041003013005000F023F0201000500000600135B001B03100875000",
                    INIT_09 => X"20A7D3FFD1010002D20100020041A030D00110200002D001103A0002D1010002",
                    INIT_0A => X"0210B12BB227A0B3D004B023017950000059208C00596098D003300313010030",
                    INIT_0B => X"D2021340120020BAD0089002D1021108D104D0035000008150000179048F0440",
                    INIT_0C => X"13011210D405A430D20213401200D104D003500060BFD20813011201E4309405",
                    INIT_0D => X"114020E9D1015000E0D8D1409101E0101000110711405000D002108060CBD280",
                    INIT_0E => X"11011001E310A30012001140100420FC60E1D204120111011001E310A3001200",
                    INIT_0F => X"607CD001B0235000A0F5D204120111011001E310A30012009008A0ECD2041201",
                    INIT_10 => X"93010053A03013071340D00110780002D0011030000200B6A1101101A010112C",
                    INIT_11 => X"00DD10301101211E1100211DD003B02300D5A07CD002B02350000059E10DD340",
                    INIT_12 => X"11561120115411541154115411201153114D1143500000C7A1101101A010112C",
                    INIT_13 => X"0060D001102E00020053007000B6100100641A261B0111001120113A11721165",
                    INIT_14 => X"1166117511425000005900530040D001102E000200530050D001102E00020053",
                    INIT_15 => X"1174116E1175116F1163112011641172116F115711001120113A117211651166",
                    INIT_16 => X"11201120113A11731165117A11691173112011641172116F115711001120113A",
                    INIT_17 => X"E185C120B220110000641A4D1B011100112011641172116F1157110011201120",
                    INIT_18 => X"0002D201000200410020D00110280002D00110200002217E1101D001A0100002",
                    INIT_19 => X"B423D1010002D20100020041B023000200641A561B010059D00110290002D101",
                    INIT_1A => X"0002D20100020041A01001301124D00110400002D00110200002E1B3C3401300",
                    INIT_1B => X"01301128D00110200002E1C8C3401300B42300641A631B01005921A11301D101",
                    INIT_1C => X"1A731B010059E1EBC3401300B423005921B91301D1010002D20100020041A010",
                    INIT_1D => X"0650152C4506450613010530D00110200002D1010002D2010002004100300064",
                    INIT_1E => X"4BA01A151B035000005921E19601D1010002D20100020041A060A1CBC6501603",
                    INIT_1F => X"B0235000005921EF3B001A03005921EF3B001A01D101000221FDD1FF21F9D100",
                    INIT_20 => X"300FA02011011201E010A0201102E01030FEA010122C114000B61039607CD003",
                    INIT_21 => X"E010A02012301101E01010201104E010A0201234114000B6103300C71039E010",
                    INIT_22 => X"1038500000C71034F0405001B04000B6103400C71033E010300FA02011011201",
                    INIT_23 => X"00590053400E400E400E400EB04000B61031500000C71038F0405001B04000B6",
                    INIT_24 => X"1031F04040104106410641064106300FB12CB04000B61031607CD001B0235000",
                    INIT_25 => X"1036E200A21011011001E200A210112C104000B61036607CD001B023500000C7",
                    INIT_26 => X"1037E200A21011011001E200A210112C104000B61037607CD001B023500000C7",
                    INIT_27 => X"11025000029011015000028B1101500000590053300FB04000B61035500000C7",
                    INIT_28 => X"500002A4F12CF02310015000029011045000028B11045000029011025000028B",
                    INIT_29 => X"210031F0B14070FF300FB02C00B61035607CD001B02350000295F12CF0231001",
                    INIT_2A => X"1035F140410031F0B140300FB02C00B61035607CD001B023500000C71035F140",
                    INIT_2B => X"320FA21010011101E200A210112C1004104000B61031607CD002B023500000C7",
                    INIT_2C => X"E300432033F0A300320FA21010011101E200A21011301001E300432033F0A300",
                    INIT_2D => X"00D562D8D110A1001004104000B61033E07CD003207CD000B023500000C71031",
                    INIT_2E => X"11011001E21011051140A200103022F8D001B023E010110411401010F040B02C",
                    INIT_2F => X"00B61033607CD001B023500000C71033E1005120A10010041040E210320FA200",
                    INIT_30 => X"114010116308D03011011001E210A2001140102C00D562FED110A10010041040",
                    INIT_31 => X"11A91100110011671172116111691174116C1175116D500000C71033E0101104",
                    INIT_32 => X"110011741165117311651172118B110011001170116D11751164116D1165116D",
                    INIT_33 => X"1100117311791173113511011100116E116F1169117311721165117611B41100",
                    INIT_34 => X"117211721114110111001177117211ED110111001170116C1165116811791101",
                    INIT_35 => X"116711731165116D116111D311021100116711731165116D116211FD11001100",
                    INIT_36 => X"11611131116C11B2110211001167116E1172115F11611131116C11FB11021100",
                    INIT_37 => X"119511021100117311691164115F11611131116C11A411021100116E1165115F",
                    INIT_38 => X"11611131116C117911021100117211651170115F116E1165115F11611131116C",
                    INIT_39 => X"1165115F11611131116C117C11021100117211651170115F117311691164115F",
                    INIT_3A => X"117311691164115F11611131116C117F110211001164116E11611172115F116E",
                    INIT_3B => X"1165115F116E1165115F11611131116C1182110211001164116E11611172115F",
                    INIT_3C => X"117411781165115F117311691164115F11611131116C11851102110011741178",
                    INIT_3D => X"11611131116C115211021100117211651170115F11611131116C118811021100",
                    INIT_3E => X"1165116C11751172115F1174116511731162110211001164116E11611172115F",
                    INIT_3F => X"11371102110011731165116C11751172115F1174116511671141110211001173",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"D2D560755806D56036D6128D5882AD588308AAAA434AAB44DAA88A2AA82A2AAA",
                   INITP_02 => X"D082AAAAAAAAAAAAAAAAAAAAAAA2288A228882AAAAAAAA108234B4AD61228A10",
                   INITP_03 => X"2A5A5ADD82A6AA351548AAA20B429AA848B420A6AA1228B42AA2828AAA228A62",
                   INITP_04 => X"28A2A08A2160234A2160234A215408D2A552288228822205818602220586008D",
                   INITP_05 => X"8D28812059034908B048DD28800581800581234A200234A20008D2A8AA28A28A",
                   INITP_06 => X"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA890D580B04",
                   INITP_07 => X"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a(31 downto 0),
                  DOPA => data_out_a(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b(31 downto 0),
                  DOPB => data_out_b(35 downto 32), 
                   DIB => data_in_b(31 downto 0),
                  DIPB => data_in_b(35 downto 32), 
                   WEB => we_b(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
    --               
    end generate s6;
    --
    --
    v6 : if (C_FAMILY = "V6") generate
      --
      address_a(13 downto 0) <= address(9 downto 0) & "1111";
      instruction <= data_out_a(17 downto 0);
      data_in_a(17 downto 0) <= "0000000000000000" & address(11 downto 10);
      jtag_dout <= data_out_b(17 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b(17 downto 0) <= data_out_b(17 downto 0);
        address_b(13 downto 0) <= "11111111111111";
        we_b(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b(17 downto 0) <= jtag_din(17 downto 0);
        address_b(13 downto 0) <= jtag_addr(9 downto 0) & "1111";
        we_b(3 downto 0) <= jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      -- 
      kcpsm6_rom: RAMB18E1
      generic map ( READ_WIDTH_A => 18,
                    WRITE_WIDTH_A => 18,
                    DOA_REG => 0,
                    INIT_A => "000000000000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 18,
                    WRITE_WIDTH_B => 18,
                    DOB_REG => 0,
                    INIT_B => X"000000000000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    SIM_DEVICE => "VIRTEX6",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D004900024BB0081",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"D20100020041003013005000F023F0201000500000600135B001B03100875000",
                    INIT_09 => X"20A7D3FFD1010002D20100020041A030D00110200002D001103A0002D1010002",
                    INIT_0A => X"0210B12BB227A0B3D004B023017950000059208C00596098D003300313010030",
                    INIT_0B => X"D2021340120020BAD0089002D1021108D104D0035000008150000179048F0440",
                    INIT_0C => X"13011210D405A430D20213401200D104D003500060BFD20813011201E4309405",
                    INIT_0D => X"114020E9D1015000E0D8D1409101E0101000110711405000D002108060CBD280",
                    INIT_0E => X"11011001E310A30012001140100420FC60E1D204120111011001E310A3001200",
                    INIT_0F => X"607CD001B0235000A0F5D204120111011001E310A30012009008A0ECD2041201",
                    INIT_10 => X"93010053A03013071340D00110780002D0011030000200B6A1101101A010112C",
                    INIT_11 => X"00DD10301101211E1100211DD003B02300D5A07CD002B02350000059E10DD340",
                    INIT_12 => X"11561120115411541154115411201153114D1143500000C7A1101101A010112C",
                    INIT_13 => X"0060D001102E00020053007000B6100100641A261B0111001120113A11721165",
                    INIT_14 => X"1166117511425000005900530040D001102E000200530050D001102E00020053",
                    INIT_15 => X"1174116E1175116F1163112011641172116F115711001120113A117211651166",
                    INIT_16 => X"11201120113A11731165117A11691173112011641172116F115711001120113A",
                    INIT_17 => X"E185C120B220110000641A4D1B011100112011641172116F1157110011201120",
                    INIT_18 => X"0002D201000200410020D00110280002D00110200002217E1101D001A0100002",
                    INIT_19 => X"B423D1010002D20100020041B023000200641A561B010059D00110290002D101",
                    INIT_1A => X"0002D20100020041A01001301124D00110400002D00110200002E1B3C3401300",
                    INIT_1B => X"01301128D00110200002E1C8C3401300B42300641A631B01005921A11301D101",
                    INIT_1C => X"1A731B010059E1EBC3401300B423005921B91301D1010002D20100020041A010",
                    INIT_1D => X"0650152C4506450613010530D00110200002D1010002D2010002004100300064",
                    INIT_1E => X"4BA01A151B035000005921E19601D1010002D20100020041A060A1CBC6501603",
                    INIT_1F => X"B0235000005921EF3B001A03005921EF3B001A01D101000221FDD1FF21F9D100",
                    INIT_20 => X"300FA02011011201E010A0201102E01030FEA010122C114000B61039607CD003",
                    INIT_21 => X"E010A02012301101E01010201104E010A0201234114000B6103300C71039E010",
                    INIT_22 => X"1038500000C71034F0405001B04000B6103400C71033E010300FA02011011201",
                    INIT_23 => X"00590053400E400E400E400EB04000B61031500000C71038F0405001B04000B6",
                    INIT_24 => X"1031F04040104106410641064106300FB12CB04000B61031607CD001B0235000",
                    INIT_25 => X"1036E200A21011011001E200A210112C104000B61036607CD001B023500000C7",
                    INIT_26 => X"1037E200A21011011001E200A210112C104000B61037607CD001B023500000C7",
                    INIT_27 => X"11025000029011015000028B1101500000590053300FB04000B61035500000C7",
                    INIT_28 => X"500002A4F12CF02310015000029011045000028B11045000029011025000028B",
                    INIT_29 => X"210031F0B14070FF300FB02C00B61035607CD001B02350000295F12CF0231001",
                    INIT_2A => X"1035F140410031F0B140300FB02C00B61035607CD001B023500000C71035F140",
                    INIT_2B => X"320FA21010011101E200A210112C1004104000B61031607CD002B023500000C7",
                    INIT_2C => X"E300432033F0A300320FA21010011101E200A21011301001E300432033F0A300",
                    INIT_2D => X"00D562D8D110A1001004104000B61033E07CD003207CD000B023500000C71031",
                    INIT_2E => X"11011001E21011051140A200103022F8D001B023E010110411401010F040B02C",
                    INIT_2F => X"00B61033607CD001B023500000C71033E1005120A10010041040E210320FA200",
                    INIT_30 => X"114010116308D03011011001E210A2001140102C00D562FED110A10010041040",
                    INIT_31 => X"11A91100110011671172116111691174116C1175116D500000C71033E0101104",
                    INIT_32 => X"110011741165117311651172118B110011001170116D11751164116D1165116D",
                    INIT_33 => X"1100117311791173113511011100116E116F1169117311721165117611B41100",
                    INIT_34 => X"117211721114110111001177117211ED110111001170116C1165116811791101",
                    INIT_35 => X"116711731165116D116111D311021100116711731165116D116211FD11001100",
                    INIT_36 => X"11611131116C11B2110211001167116E1172115F11611131116C11FB11021100",
                    INIT_37 => X"119511021100117311691164115F11611131116C11A411021100116E1165115F",
                    INIT_38 => X"11611131116C117911021100117211651170115F116E1165115F11611131116C",
                    INIT_39 => X"1165115F11611131116C117C11021100117211651170115F117311691164115F",
                    INIT_3A => X"117311691164115F11611131116C117F110211001164116E11611172115F116E",
                    INIT_3B => X"1165115F116E1165115F11611131116C1182110211001164116E11611172115F",
                    INIT_3C => X"117411781165115F117311691164115F11611131116C11851102110011741178",
                    INIT_3D => X"11611131116C115211021100117211651170115F11611131116C118811021100",
                    INIT_3E => X"1165116C11751172115F1174116511731162110211001164116E11611172115F",
                    INIT_3F => X"11371102110011731165116C11751172115F1174116511671141110211001173",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"D2D560755806D56036D6128D5882AD588308AAAA434AAB44DAA88A2AA82A2AAA",
                   INITP_02 => X"D082AAAAAAAAAAAAAAAAAAAAAAA2288A228882AAAAAAAA108234B4AD61228A10",
                   INITP_03 => X"2A5A5ADD82A6AA351548AAA20B429AA848B420A6AA1228B42AA2828AAA228A62",
                   INITP_04 => X"28A2A08A2160234A2160234A215408D2A552288228822205818602220586008D",
                   INITP_05 => X"8D28812059034908B048DD28800581800581234A200234A20008D2A8AA28A28A",
                   INITP_06 => X"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA890D580B04",
                   INITP_07 => X"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
      port map(   ADDRARDADDR => address_a(13 downto 0),
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a(15 downto 0),
                      DOPADOP => data_out_a(17 downto 16), 
                        DIADI => data_in_a(15 downto 0),
                      DIPADIP => data_in_a(17 downto 16), 
                          WEA => "00",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b(13 downto 0),
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b(15 downto 0),
                      DOPBDOP => data_out_b(17 downto 16), 
                        DIBDI => data_in_b(15 downto 0),
                      DIPBDIP => data_in_b(17 downto 16), 
                        WEBWE => we_b(3 downto 0),
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0');
      --
    end generate v6;
    --
    --
    akv7 : if (C_FAMILY = "7S") generate
      --
      address_a(13 downto 0) <= address(9 downto 0) & "1111";
      instruction <= data_out_a(17 downto 0);
      data_in_a(17 downto 0) <= "0000000000000000" & address(11 downto 10);
      jtag_dout <= data_out_b(17 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b(17 downto 0) <= data_out_b(17 downto 0);
        address_b(13 downto 0) <= "11111111111111";
        we_b(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b(17 downto 0) <= jtag_din(17 downto 0);
        address_b(13 downto 0) <= jtag_addr(9 downto 0) & "1111";
        we_b(3 downto 0) <= jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      -- 
      kcpsm6_rom: RAMB18E1
      generic map ( READ_WIDTH_A => 18,
                    WRITE_WIDTH_A => 18,
                    DOA_REG => 0,
                    INIT_A => "000000000000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 18,
                    WRITE_WIDTH_B => 18,
                    DOB_REG => 0,
                    INIT_B => X"000000000000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    SIM_DEVICE => "7SERIES",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D004900024BB0081",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"D20100020041003013005000F023F0201000500000600135B001B03100875000",
                    INIT_09 => X"20A7D3FFD1010002D20100020041A030D00110200002D001103A0002D1010002",
                    INIT_0A => X"0210B12BB227A0B3D004B023017950000059208C00596098D003300313010030",
                    INIT_0B => X"D2021340120020BAD0089002D1021108D104D0035000008150000179048F0440",
                    INIT_0C => X"13011210D405A430D20213401200D104D003500060BFD20813011201E4309405",
                    INIT_0D => X"114020E9D1015000E0D8D1409101E0101000110711405000D002108060CBD280",
                    INIT_0E => X"11011001E310A30012001140100420FC60E1D204120111011001E310A3001200",
                    INIT_0F => X"607CD001B0235000A0F5D204120111011001E310A30012009008A0ECD2041201",
                    INIT_10 => X"93010053A03013071340D00110780002D0011030000200B6A1101101A010112C",
                    INIT_11 => X"00DD10301101211E1100211DD003B02300D5A07CD002B02350000059E10DD340",
                    INIT_12 => X"11561120115411541154115411201153114D1143500000C7A1101101A010112C",
                    INIT_13 => X"0060D001102E00020053007000B6100100641A261B0111001120113A11721165",
                    INIT_14 => X"1166117511425000005900530040D001102E000200530050D001102E00020053",
                    INIT_15 => X"1174116E1175116F1163112011641172116F115711001120113A117211651166",
                    INIT_16 => X"11201120113A11731165117A11691173112011641172116F115711001120113A",
                    INIT_17 => X"E185C120B220110000641A4D1B011100112011641172116F1157110011201120",
                    INIT_18 => X"0002D201000200410020D00110280002D00110200002217E1101D001A0100002",
                    INIT_19 => X"B423D1010002D20100020041B023000200641A561B010059D00110290002D101",
                    INIT_1A => X"0002D20100020041A01001301124D00110400002D00110200002E1B3C3401300",
                    INIT_1B => X"01301128D00110200002E1C8C3401300B42300641A631B01005921A11301D101",
                    INIT_1C => X"1A731B010059E1EBC3401300B423005921B91301D1010002D20100020041A010",
                    INIT_1D => X"0650152C4506450613010530D00110200002D1010002D2010002004100300064",
                    INIT_1E => X"4BA01A151B035000005921E19601D1010002D20100020041A060A1CBC6501603",
                    INIT_1F => X"B0235000005921EF3B001A03005921EF3B001A01D101000221FDD1FF21F9D100",
                    INIT_20 => X"300FA02011011201E010A0201102E01030FEA010122C114000B61039607CD003",
                    INIT_21 => X"E010A02012301101E01010201104E010A0201234114000B6103300C71039E010",
                    INIT_22 => X"1038500000C71034F0405001B04000B6103400C71033E010300FA02011011201",
                    INIT_23 => X"00590053400E400E400E400EB04000B61031500000C71038F0405001B04000B6",
                    INIT_24 => X"1031F04040104106410641064106300FB12CB04000B61031607CD001B0235000",
                    INIT_25 => X"1036E200A21011011001E200A210112C104000B61036607CD001B023500000C7",
                    INIT_26 => X"1037E200A21011011001E200A210112C104000B61037607CD001B023500000C7",
                    INIT_27 => X"11025000029011015000028B1101500000590053300FB04000B61035500000C7",
                    INIT_28 => X"500002A4F12CF02310015000029011045000028B11045000029011025000028B",
                    INIT_29 => X"210031F0B14070FF300FB02C00B61035607CD001B02350000295F12CF0231001",
                    INIT_2A => X"1035F140410031F0B140300FB02C00B61035607CD001B023500000C71035F140",
                    INIT_2B => X"320FA21010011101E200A210112C1004104000B61031607CD002B023500000C7",
                    INIT_2C => X"E300432033F0A300320FA21010011101E200A21011301001E300432033F0A300",
                    INIT_2D => X"00D562D8D110A1001004104000B61033E07CD003207CD000B023500000C71031",
                    INIT_2E => X"11011001E21011051140A200103022F8D001B023E010110411401010F040B02C",
                    INIT_2F => X"00B61033607CD001B023500000C71033E1005120A10010041040E210320FA200",
                    INIT_30 => X"114010116308D03011011001E210A2001140102C00D562FED110A10010041040",
                    INIT_31 => X"11A91100110011671172116111691174116C1175116D500000C71033E0101104",
                    INIT_32 => X"110011741165117311651172118B110011001170116D11751164116D1165116D",
                    INIT_33 => X"1100117311791173113511011100116E116F1169117311721165117611B41100",
                    INIT_34 => X"117211721114110111001177117211ED110111001170116C1165116811791101",
                    INIT_35 => X"116711731165116D116111D311021100116711731165116D116211FD11001100",
                    INIT_36 => X"11611131116C11B2110211001167116E1172115F11611131116C11FB11021100",
                    INIT_37 => X"119511021100117311691164115F11611131116C11A411021100116E1165115F",
                    INIT_38 => X"11611131116C117911021100117211651170115F116E1165115F11611131116C",
                    INIT_39 => X"1165115F11611131116C117C11021100117211651170115F117311691164115F",
                    INIT_3A => X"117311691164115F11611131116C117F110211001164116E11611172115F116E",
                    INIT_3B => X"1165115F116E1165115F11611131116C1182110211001164116E11611172115F",
                    INIT_3C => X"117411781165115F117311691164115F11611131116C11851102110011741178",
                    INIT_3D => X"11611131116C115211021100117211651170115F11611131116C118811021100",
                    INIT_3E => X"1165116C11751172115F1174116511731162110211001164116E11611172115F",
                    INIT_3F => X"11371102110011731165116C11751172115F1174116511671141110211001173",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"D2D560755806D56036D6128D5882AD588308AAAA434AAB44DAA88A2AA82A2AAA",
                   INITP_02 => X"D082AAAAAAAAAAAAAAAAAAAAAAA2288A228882AAAAAAAA108234B4AD61228A10",
                   INITP_03 => X"2A5A5ADD82A6AA351548AAA20B429AA848B420A6AA1228B42AA2828AAA228A62",
                   INITP_04 => X"28A2A08A2160234A2160234A215408D2A552288228822205818602220586008D",
                   INITP_05 => X"8D28812059034908B048DD28800581800581234A200234A20008D2A8AA28A28A",
                   INITP_06 => X"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA890D580B04",
                   INITP_07 => X"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
      port map(   ADDRARDADDR => address_a(13 downto 0),
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a(15 downto 0),
                      DOPADOP => data_out_a(17 downto 16), 
                        DIADI => data_in_a(15 downto 0),
                      DIPADIP => data_in_a(17 downto 16), 
                          WEA => "00",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b(13 downto 0),
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b(15 downto 0),
                      DOPBDOP => data_out_b(17 downto 16), 
                        DIBDI => data_in_b(15 downto 0),
                      DIPBDIP => data_in_b(17 downto 16), 
                        WEBWE => we_b(3 downto 0),
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0');
      --
    end generate akv7;
    --
  end generate ram_1k_generate;
  --
  --
  --
  ram_2k_generate : if (C_RAM_SIZE_KWORDS = 2) generate
    --
    --
    s6: if (C_FAMILY = "S6") generate
      --
      address_a(13 downto 0) <= address(10 downto 0) & "000";
      instruction <= data_out_a_h(32) & data_out_a_h(7 downto 0) & data_out_a_l(32) & data_out_a_l(7 downto 0);
      data_in_a <= "00000000000000000000000000000000000" & address(11);
      jtag_dout <= data_out_b_h(32) & data_out_b_h(7 downto 0) & data_out_b_l(32) & data_out_b_l(7 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b_l <= "000" & data_out_b_l(32) & "000000000000000000000000" & data_out_b_l(7 downto 0);
        data_in_b_h <= "000" & data_out_b_h(32) & "000000000000000000000000" & data_out_b_h(7 downto 0);
        address_b(13 downto 0) <= "00000000000000";
        we_b(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b_h <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_l <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        address_b(13 downto 0) <= jtag_addr(10 downto 0) & "000";
        we_b(3 downto 0) <= jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom_l: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"002C2F200002000010000004000020000001000008000600080002000400BB81",
                    INIT_01 => X"000E0040100A3D2A40100A3D11400A3D30000E0000300E01002C2F1006060606",
                    INIT_02 => X"00010A02010D020001020102410007525241370F00074B4B41370E0E0E0E0000",
                    INIT_03 => X"59646D00002E73746E656D75677261206461420064000101026C00A000013E02",
                    INIT_04 => X"A7FF010201024130012002013A02010201024130000023200000603501318700",
                    INIT_05 => X"024000BA080202080403008100798F40102B27B304237900598C599803030130",
                    INIT_06 => X"40E90100D8400110000740000280CB8001100530024000040300BF0801013005",
                    INIT_07 => X"7C012300F50401010110000008EC040101011000004004FCE104010101100000",
                    INIT_08 => X"DD30011E001D0323D57C022300590D400153300740017802013002B61001102C",
                    INIT_09 => X"60012E025370B60164260100203A726556205454545420534D4300C71001102C",
                    INIT_0A => X"746E756F632064726F5700203A72656666754200595340012E025350012E0253",
                    INIT_0B => X"85202000644D01002064726F5700202020203A73657A69732064726F5700203A",
                    INIT_0C => X"2301020102412302645601590129020102010241200128020120027E01011002",
                    INIT_0D => X"3028012002C840002364630159A1010102010241103024014002012002B34000",
                    INIT_0E => X"502C0606013001200201020102413064730159EB40002359B901010201024110",
                    INIT_0F => X"230059EF000359EF00010102FDFFF900A015030059E101010201024160CB5003",
                    INIT_10 => X"1020300110200410203440B633C739100F20010110200210FE102C40B6397C03",
                    INIT_11 => X"59530E0E0E0E40B63100C738400140B63800C734400140B634C733100F200101",
                    INIT_12 => X"360010010100102C40B6367C012300C7314010060606060F2C40B6317C012300",
                    INIT_13 => X"02009001008B010059530F40B63500C7370010010100102C40B6377C012300C7",
                    INIT_14 => X"00F040FF0F2CB6357C012300952C230100A42C2301009004008B04009002008B",
                    INIT_15 => X"0F10010100102C0440B6317C022300C7354000F0400F2CB6357C012300C73540",
                    INIT_16 => X"D5D810000440B6337C037C002300C7310020F0000F100101001030010020F000",
                    INIT_17 => X"B6337C012300C7330020000440100F0001011005400030F8012310044010402C",
                    INIT_18 => X"A9000067726169746C756D00C73310044011083001011000402CD5FE10000440",
                    INIT_19 => X"007379733501006E6F6973726576B4000074657365728B0000706D75646D656D",
                    INIT_1A => X"6773656D61D302006773656D62FD000072721401007772ED0100706C65687901",
                    INIT_1B => X"9502007369645F61316CA402006E655F61316CB20200676E725F61316CFB0200",
                    INIT_1C => X"655F61316C7C02007265705F7369645F61316C7902007265705F6E655F61316C",
                    INIT_1D => X"655F6E655F61316C820200646E61725F7369645F61316C7F0200646E61725F6E",
                    INIT_1E => X"61316C5202007265705F61316C8802007478655F7369645F61316C8502007478",
                    INIT_1F => X"37020073656C75725F74656741020073656C75725F746573620200646E61725F",
                    INIT_20 => X"012310202300203CFF2F00A0001503FF2F020061316CFF01007664615F61316C",
                    INIT_21 => X"0060778700608710A0000110A000018F40140000032300012B003CFFA0140001",
                    INIT_22 => X"2058015F2F205F20105810200820100023236A05012301422F01206A10202300",
                    INIT_23 => X"6F7272450059646B0400646E616D6D6F63206461420042000128230010012423",
                    INIT_24 => X"4028004024BA00230092200110002C04380059010201024120647C0400203A72",
                    INIT_25 => X"10002001060098702F60985001A701701C01600160B3509802012C0606405010",
                    INIT_26 => X"0AF00D000120BB60875964C804590021776F6C667265764FBB11DAD220200120",
                    INIT_27 => X"012002010802060220000C0120012059000C000102000C00200120EB20F708F0",
                    INIT_28 => X"0000000000000000000000000000000000000000000C00200120200120010802",
                    INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"0160B416A660850843844202600208101FFFE536008278000000008F80000000",
                   INITP_01 => X"19AFA5047C405CE0C717060740210018D3FFFFFFFFFFE000003FFFCD3C03980D",
                   INITP_02 => X"00E098183000F12F12007801E00421249200110011001E800000000212202210",
                   INITP_03 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1A88C",
                   INITP_04 => X"DA00100AB00BFF0F6002203CE032101FF0FFF91781BD800400CC125A00B3FFFF",
                   INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000006",
                   INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_l(31 downto 0),
                  DOPA => data_out_a_l(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_l(31 downto 0),
                  DOPB => data_out_b_l(35 downto 32), 
                   DIB => data_in_b_l(31 downto 0),
                  DIPB => data_in_b_l(35 downto 32), 
                   WEB => we_b(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
      -- 
      kcpsm6_rom_h: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"01F0000028684828684828684828684828684828684810C86848108868481200",
                    INIT_01 => X"28A008D0E888D0C8D0E888D0C8D0E8D0C828A0082800A00881F00000A1A1A1A1",
                    INIT_02 => X"2868080068080028680069000028C890F0E8881800C990F0E989A1A1A1A10128",
                    INIT_03 => X"00000D0D08080808080808080808080808080828109D8D680090E82528680800",
                    INIT_04 => X"90E9680069000050680800680800680069000000092878780828000058580028",
                    INIT_05 => X"69090990684868086868280028000202815859D0E8580028001000B0E8188900",
                    INIT_06 => X"0890E828F0E8C870088808286808B0E989896A52690909686828B0E98989724A",
                    INIT_07 => X"B0E85828D0E9898888715109C8D0E9898888715109088810B0E9898888715109",
                    INIT_08 => X"000808100890E85800D0E8582800F0E9C9005089096808006808000050885008",
                    INIT_09 => X"0068080000000008000D0D080808080808080808080808080808280050885008",
                    INIT_0A => X"0808080808080808080808080808080808080828000000680800000068080000",
                    INIT_0B => X"F0E05908000D0D08080808080808080808080808080808080808080808080808",
                    INIT_0C => X"5A68006900005800000D0D006808006800690000006808006808001088685000",
                    INIT_0D => X"8008680800F0E1095A000D0D0010896800690000508008680800680800F0E109",
                    INIT_0E => X"038AA2A28902680800680069000000000D0D00F0E1095A001089680069000050",
                    INIT_0F => X"582800109D8D00109D8D680090E890E8250D0D280010CB680069000050D0E38B",
                    INIT_10 => X"705009887008887050090800080008701850888970508870185009080008B0E8",
                    INIT_11 => X"0000A0A0A0A05800082800087828580008280008782858000800087018508889",
                    INIT_12 => X"0871518888715108080008B0E8582800087820A0A0A0A01858580008B0E85828",
                    INIT_13 => X"082801082801082800001858000828000871518888715108080008B0E8582800",
                    INIT_14 => X"1018583818580008B0E858280178780828017878082801082801082801082801",
                    INIT_15 => X"1951888871510888080008B0E8582800087820185818580008B0E85828000878",
                    INIT_16 => X"00B1685088080008F0E890E85828000871211951195188887151088871211951",
                    INIT_17 => X"0008B0E85828000870285088087119518888718808510891E858708808087858",
                    INIT_18 => X"080808080808080808080828000870880808B1E888887151080800B168508808",
                    INIT_19 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1A => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1B => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1C => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1D => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1E => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1F => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_20 => X"89B2E050F2E15892E892E825090D0D0808080808080808080808080808080808",
                    INIT_21 => X"2800020028000021259D8D01259D8D020212099D8D129D8D92E892E825129D8D",
                    INIT_22 => X"011289F2005092E101D2E15889017180087892E88858C9F2008950F2E1587808",
                    INIT_23 => X"080808082800000D0D080808080808080808080808281271C88858C150C88858",
                    INIT_24 => X"8008528008F2E2580AD2E08870080889092800680069000000000D0D08080808",
                    INIT_25 => X"70885848002812700050B2E38B128B7000CB508B51D2E3D2CB8A8BA3A3038353",
                    INIT_26 => X"E892E850C85812000000000D0D000808080808080808080812C202F2E8788858",
                    INIT_27 => X"680800680800D2C85828A00878C8580028A008680028A00878C858F2E892E892",
                    INIT_28 => X"000000000000000000000000000000000000000028A00878C85878C858680800",
                    INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"98442184591A29E292FF13F0BEB7E77FCFFFF9DDDBFD83019295486069249333",
                   INITP_01 => X"733A9DF402FD31BE2C4DF16C7D9BF5B589FFFFFFFFFFFD6B5A9FFFF094CE45B0",
                   INITP_02 => X"A6842122C2A680880853414D029EF6DB6DCB445344534029C16969508915090A",
                   INITP_03 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA2230",
                   INITP_04 => X"B6499C9543E7FFF48FA5850004487F4FFE7FFF005A426192FF89C4AC4951FFFF",
                   INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000925",
                   INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_h(31 downto 0),
                  DOPA => data_out_a_h(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_h(31 downto 0),
                  DOPB => data_out_b_h(35 downto 32), 
                   DIB => data_in_b_h(31 downto 0),
                  DIPB => data_in_b_h(35 downto 32), 
                   WEB => we_b(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
    --
    end generate s6;
    --
    --
    v6 : if (C_FAMILY = "V6") generate
      --
      address_a <= '1' & address(10 downto 0) & "1111";
      instruction <= data_out_a(33 downto 32) & data_out_a(15 downto 0);
      data_in_a <= "00000000000000000000000000000000000" & address(11);
      jtag_dout <= data_out_b(33 downto 32) & data_out_b(15 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b <= "00" & data_out_b(33 downto 32) & "0000000000000000" & data_out_b(15 downto 0);
        address_b <= "1111111111111111";
        we_b <= "00000000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b <= "00" & jtag_din(17 downto 16) & "0000000000000000" & jtag_din(15 downto 0);
        address_b <= '1' & jtag_addr(10 downto 0) & "1111";
        we_b <= jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom: RAMB36E1
      generic map ( READ_WIDTH_A => 18,
                    WRITE_WIDTH_A => 18,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 18,
                    WRITE_WIDTH_B => 18,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "VIRTEX6",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D004900024BB0081",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"D20100020041003013005000F023F0201000500000600135B001B03100875000",
                    INIT_09 => X"20A7D3FFD1010002D20100020041A030D00110200002D001103A0002D1010002",
                    INIT_0A => X"0210B12BB227A0B3D004B023017950000059208C00596098D003300313010030",
                    INIT_0B => X"D2021340120020BAD0089002D1021108D104D0035000008150000179048F0440",
                    INIT_0C => X"13011210D405A430D20213401200D104D003500060BFD20813011201E4309405",
                    INIT_0D => X"114020E9D1015000E0D8D1409101E0101000110711405000D002108060CBD280",
                    INIT_0E => X"11011001E310A30012001140100420FC60E1D204120111011001E310A3001200",
                    INIT_0F => X"607CD001B0235000A0F5D204120111011001E310A30012009008A0ECD2041201",
                    INIT_10 => X"93010053A03013071340D00110780002D0011030000200B6A1101101A010112C",
                    INIT_11 => X"00DD10301101211E1100211DD003B02300D5A07CD002B02350000059E10DD340",
                    INIT_12 => X"11561120115411541154115411201153114D1143500000C7A1101101A010112C",
                    INIT_13 => X"0060D001102E00020053007000B6100100641A261B0111001120113A11721165",
                    INIT_14 => X"1166117511425000005900530040D001102E000200530050D001102E00020053",
                    INIT_15 => X"1174116E1175116F1163112011641172116F115711001120113A117211651166",
                    INIT_16 => X"11201120113A11731165117A11691173112011641172116F115711001120113A",
                    INIT_17 => X"E185C120B220110000641A4D1B011100112011641172116F1157110011201120",
                    INIT_18 => X"0002D201000200410020D00110280002D00110200002217E1101D001A0100002",
                    INIT_19 => X"B423D1010002D20100020041B023000200641A561B010059D00110290002D101",
                    INIT_1A => X"0002D20100020041A01001301124D00110400002D00110200002E1B3C3401300",
                    INIT_1B => X"01301128D00110200002E1C8C3401300B42300641A631B01005921A11301D101",
                    INIT_1C => X"1A731B010059E1EBC3401300B423005921B91301D1010002D20100020041A010",
                    INIT_1D => X"0650152C4506450613010530D00110200002D1010002D2010002004100300064",
                    INIT_1E => X"4BA01A151B035000005921E19601D1010002D20100020041A060A1CBC6501603",
                    INIT_1F => X"B0235000005921EF3B001A03005921EF3B001A01D101000221FDD1FF21F9D100",
                    INIT_20 => X"300FA02011011201E010A0201102E01030FEA010122C114000B61039607CD003",
                    INIT_21 => X"E010A02012301101E01010201104E010A0201234114000B6103300C71039E010",
                    INIT_22 => X"1038500000C71034F0405001B04000B6103400C71033E010300FA02011011201",
                    INIT_23 => X"00590053400E400E400E400EB04000B61031500000C71038F0405001B04000B6",
                    INIT_24 => X"1031F04040104106410641064106300FB12CB04000B61031607CD001B0235000",
                    INIT_25 => X"1036E200A21011011001E200A210112C104000B61036607CD001B023500000C7",
                    INIT_26 => X"1037E200A21011011001E200A210112C104000B61037607CD001B023500000C7",
                    INIT_27 => X"11025000029011015000028B1101500000590053300FB04000B61035500000C7",
                    INIT_28 => X"500002A4F12CF02310015000029011045000028B11045000029011025000028B",
                    INIT_29 => X"210031F0B14070FF300FB02C00B61035607CD001B02350000295F12CF0231001",
                    INIT_2A => X"1035F140410031F0B140300FB02C00B61035607CD001B023500000C71035F140",
                    INIT_2B => X"320FA21010011101E200A210112C1004104000B61031607CD002B023500000C7",
                    INIT_2C => X"E300432033F0A300320FA21010011101E200A21011301001E300432033F0A300",
                    INIT_2D => X"00D562D8D110A1001004104000B61033E07CD003207CD000B023500000C71031",
                    INIT_2E => X"11011001E21011051140A200103022F8D001B023E010110411401010F040B02C",
                    INIT_2F => X"00B61033607CD001B023500000C71033E1005120A10010041040E210320FA200",
                    INIT_30 => X"114010116308D03011011001E210A2001140102C00D562FED110A10010041040",
                    INIT_31 => X"11A91100110011671172116111691174116C1175116D500000C71033E0101104",
                    INIT_32 => X"110011741165117311651172118B110011001170116D11751164116D1165116D",
                    INIT_33 => X"1100117311791173113511011100116E116F1169117311721165117611B41100",
                    INIT_34 => X"117211721114110111001177117211ED110111001170116C1165116811791101",
                    INIT_35 => X"116711731165116D116111D311021100116711731165116D116211FD11001100",
                    INIT_36 => X"11611131116C11B2110211001167116E1172115F11611131116C11FB11021100",
                    INIT_37 => X"119511021100117311691164115F11611131116C11A411021100116E1165115F",
                    INIT_38 => X"11611131116C117911021100117211651170115F116E1165115F11611131116C",
                    INIT_39 => X"1165115F11611131116C117C11021100117211651170115F117311691164115F",
                    INIT_3A => X"117311691164115F11611131116C117F110211001164116E11611172115F116E",
                    INIT_3B => X"1165115F116E1165115F11611131116C1182110211001164116E11611172115F",
                    INIT_3C => X"117411781165115F117311691164115F11611131116C11851102110011741178",
                    INIT_3D => X"11611131116C115211021100117211651170115F11611131116C118811021100",
                    INIT_3E => X"1165116C11751172115F1174116511731162110211001164116E11611172115F",
                    INIT_3F => X"11371102110011731165116C11751172115F1174116511671141110211001173",
                    INIT_40 => X"112F1102110011611131116C11FF11011100117611641161115F11611131116C",
                    INIT_41 => X"12016423C010A020E423C200B020243CD1FF242FD1004BA012001A151B0311FF",
                    INIT_42 => X"0440241412003B001A0324233B001A01242BD100243CD1FF4BA024143B001A01",
                    INIT_43 => X"500000600477008750000060008742104BA03B001A0102104BA03B001A01048F",
                    INIT_44 => X"1123F023246AD0051001B0239201E442002F1201A020E46AC210B120F0231000",
                    INIT_45 => X"032024581201E45F002FA020245FC3200310A458C310B12013080320E2100100",
                    INIT_46 => X"1163112011641161114250002442E30090011028B0238300A01091011124B123",
                    INIT_47 => X"116F1172117211455000005900641A6B1B0411001164116E1161116D116D116F",
                    INIT_48 => X"123850000059D1010002D20100020041002000641A7C1B0411001120113A1172",
                    INIT_49 => X"01401128A50000401024E4BAC400B0231400A492C1201101E0101000112C1204",
                    INIT_4A => X"001C9601A1601601A260A4B3C650A49896021401172C4706470607400650A610",
                    INIT_4B => X"E0101100B1209001000650002498E070002FA0606498C650160124A71701E070",
                    INIT_4C => X"1177116F116C1166117211651176114F24BB841104DAE4D2D120F1201101B120",
                    INIT_4D => X"D10A24F0D10DA1009001B02024BB00600087005900641AC81B04005911001121",
                    INIT_4E => X"5000400C1000D10100025000400C1000F0209001B020E4EBD12024F7D10824F0",
                    INIT_4F => X"D10111200002D10111080002A5069002B0205000400C1001F0209001B0200059",
                    INIT_50 => X"00000000000000005000400C1000F0209001B020F0209001B020D10111080002",
                    INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"D2D560755806D56036D6128D5882AD588308AAAA434AAB44DAA88A2AA82A2AAA",
                   INITP_02 => X"D082AAAAAAAAAAAAAAAAAAAAAAA2288A228882AAAAAAAA108234B4AD61228A10",
                   INITP_03 => X"2A5A5ADD82A6AA351548AAA20B429AA848B420A6AA1228B42AA2828AAA228A62",
                   INITP_04 => X"28A2A08A2160234A2160234A215408D2A552288228822205818602220586008D",
                   INITP_05 => X"8D28812059034908B048DD28800581800581234A200234A20008D2A8AA28A28A",
                   INITP_06 => X"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA890D580B04",
                   INITP_07 => X"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
                   INITP_08 => X"AAA82AAAAAAA5114278D34492D479348AAAA9496A165DDA574D37602AAAAAAAA",
                   INITP_09 => X"8A2D249292A49377744AA82AAAAABB6490AA8D6691375544413435812AAA20AA",
                   INITP_0A => X"0000000000000000000000000000000000000000000000000000000000924922",
                   INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a(31 downto 0),
                      DOPADOP => data_out_a(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b(31 downto 0),
                      DOPBDOP => data_out_b(35 downto 32), 
                        DIBDI => data_in_b(31 downto 0),
                      DIPBDIP => data_in_b(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
    end generate v6;
    --
    --
    akv7 : if (C_FAMILY = "7S") generate
      --
      address_a <= '1' & address(10 downto 0) & "1111";
      instruction <= data_out_a(33 downto 32) & data_out_a(15 downto 0);
      data_in_a <= "00000000000000000000000000000000000" & address(11);
      jtag_dout <= data_out_b(33 downto 32) & data_out_b(15 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b <= "00" & data_out_b(33 downto 32) & "0000000000000000" & data_out_b(15 downto 0);
        address_b <= "1111111111111111";
        we_b <= "00000000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b <= "00" & jtag_din(17 downto 16) & "0000000000000000" & jtag_din(15 downto 0);
        address_b <= '1' & jtag_addr(10 downto 0) & "1111";
        we_b <= jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom: RAMB36E1
      generic map ( READ_WIDTH_A => 18,
                    WRITE_WIDTH_A => 18,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 18,
                    WRITE_WIDTH_B => 18,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "7SERIES",
                    INIT_00 => X"5000D00190005000D008900020069000D008900020021000D004900024BB0081",
                    INIT_01 => X"0300E02C002F00205000D00290005000D01090005000D00490005000D0209000",
                    INIT_02 => X"90305000400E100050000030400E10010300E02C002F00104306430643064306",
                    INIT_03 => X"5000400E1000A040D010100AA03D902AA040D010100AA03D9011A040D00AA03D",
                    INIT_04 => X"E052D1411137310F01009207204BE04BD2411237420E420E420E420E02005000",
                    INIT_05 => X"5000D001100A0002D001100D00025000D1010002D20100020041500091072052",
                    INIT_06 => X"116411611142500020643B001A01D1010002206CD1004BA05000D101113E0002",
                    INIT_07 => X"005900641A6D1B001100112E11731174116E1165116D11751167117211611120",
                    INIT_08 => X"D20100020041003013005000F023F0201000500000600135B001B03100875000",
                    INIT_09 => X"20A7D3FFD1010002D20100020041A030D00110200002D001103A0002D1010002",
                    INIT_0A => X"0210B12BB227A0B3D004B023017950000059208C00596098D003300313010030",
                    INIT_0B => X"D2021340120020BAD0089002D1021108D104D0035000008150000179048F0440",
                    INIT_0C => X"13011210D405A430D20213401200D104D003500060BFD20813011201E4309405",
                    INIT_0D => X"114020E9D1015000E0D8D1409101E0101000110711405000D002108060CBD280",
                    INIT_0E => X"11011001E310A30012001140100420FC60E1D204120111011001E310A3001200",
                    INIT_0F => X"607CD001B0235000A0F5D204120111011001E310A30012009008A0ECD2041201",
                    INIT_10 => X"93010053A03013071340D00110780002D0011030000200B6A1101101A010112C",
                    INIT_11 => X"00DD10301101211E1100211DD003B02300D5A07CD002B02350000059E10DD340",
                    INIT_12 => X"11561120115411541154115411201153114D1143500000C7A1101101A010112C",
                    INIT_13 => X"0060D001102E00020053007000B6100100641A261B0111001120113A11721165",
                    INIT_14 => X"1166117511425000005900530040D001102E000200530050D001102E00020053",
                    INIT_15 => X"1174116E1175116F1163112011641172116F115711001120113A117211651166",
                    INIT_16 => X"11201120113A11731165117A11691173112011641172116F115711001120113A",
                    INIT_17 => X"E185C120B220110000641A4D1B011100112011641172116F1157110011201120",
                    INIT_18 => X"0002D201000200410020D00110280002D00110200002217E1101D001A0100002",
                    INIT_19 => X"B423D1010002D20100020041B023000200641A561B010059D00110290002D101",
                    INIT_1A => X"0002D20100020041A01001301124D00110400002D00110200002E1B3C3401300",
                    INIT_1B => X"01301128D00110200002E1C8C3401300B42300641A631B01005921A11301D101",
                    INIT_1C => X"1A731B010059E1EBC3401300B423005921B91301D1010002D20100020041A010",
                    INIT_1D => X"0650152C4506450613010530D00110200002D1010002D2010002004100300064",
                    INIT_1E => X"4BA01A151B035000005921E19601D1010002D20100020041A060A1CBC6501603",
                    INIT_1F => X"B0235000005921EF3B001A03005921EF3B001A01D101000221FDD1FF21F9D100",
                    INIT_20 => X"300FA02011011201E010A0201102E01030FEA010122C114000B61039607CD003",
                    INIT_21 => X"E010A02012301101E01010201104E010A0201234114000B6103300C71039E010",
                    INIT_22 => X"1038500000C71034F0405001B04000B6103400C71033E010300FA02011011201",
                    INIT_23 => X"00590053400E400E400E400EB04000B61031500000C71038F0405001B04000B6",
                    INIT_24 => X"1031F04040104106410641064106300FB12CB04000B61031607CD001B0235000",
                    INIT_25 => X"1036E200A21011011001E200A210112C104000B61036607CD001B023500000C7",
                    INIT_26 => X"1037E200A21011011001E200A210112C104000B61037607CD001B023500000C7",
                    INIT_27 => X"11025000029011015000028B1101500000590053300FB04000B61035500000C7",
                    INIT_28 => X"500002A4F12CF02310015000029011045000028B11045000029011025000028B",
                    INIT_29 => X"210031F0B14070FF300FB02C00B61035607CD001B02350000295F12CF0231001",
                    INIT_2A => X"1035F140410031F0B140300FB02C00B61035607CD001B023500000C71035F140",
                    INIT_2B => X"320FA21010011101E200A210112C1004104000B61031607CD002B023500000C7",
                    INIT_2C => X"E300432033F0A300320FA21010011101E200A21011301001E300432033F0A300",
                    INIT_2D => X"00D562D8D110A1001004104000B61033E07CD003207CD000B023500000C71031",
                    INIT_2E => X"11011001E21011051140A200103022F8D001B023E010110411401010F040B02C",
                    INIT_2F => X"00B61033607CD001B023500000C71033E1005120A10010041040E210320FA200",
                    INIT_30 => X"114010116308D03011011001E210A2001140102C00D562FED110A10010041040",
                    INIT_31 => X"11A91100110011671172116111691174116C1175116D500000C71033E0101104",
                    INIT_32 => X"110011741165117311651172118B110011001170116D11751164116D1165116D",
                    INIT_33 => X"1100117311791173113511011100116E116F1169117311721165117611B41100",
                    INIT_34 => X"117211721114110111001177117211ED110111001170116C1165116811791101",
                    INIT_35 => X"116711731165116D116111D311021100116711731165116D116211FD11001100",
                    INIT_36 => X"11611131116C11B2110211001167116E1172115F11611131116C11FB11021100",
                    INIT_37 => X"119511021100117311691164115F11611131116C11A411021100116E1165115F",
                    INIT_38 => X"11611131116C117911021100117211651170115F116E1165115F11611131116C",
                    INIT_39 => X"1165115F11611131116C117C11021100117211651170115F117311691164115F",
                    INIT_3A => X"117311691164115F11611131116C117F110211001164116E11611172115F116E",
                    INIT_3B => X"1165115F116E1165115F11611131116C1182110211001164116E11611172115F",
                    INIT_3C => X"117411781165115F117311691164115F11611131116C11851102110011741178",
                    INIT_3D => X"11611131116C115211021100117211651170115F11611131116C118811021100",
                    INIT_3E => X"1165116C11751172115F1174116511731162110211001164116E11611172115F",
                    INIT_3F => X"11371102110011731165116C11751172115F1174116511671141110211001173",
                    INIT_40 => X"112F1102110011611131116C11FF11011100117611641161115F11611131116C",
                    INIT_41 => X"12016423C010A020E423C200B020243CD1FF242FD1004BA012001A151B0311FF",
                    INIT_42 => X"0440241412003B001A0324233B001A01242BD100243CD1FF4BA024143B001A01",
                    INIT_43 => X"500000600477008750000060008742104BA03B001A0102104BA03B001A01048F",
                    INIT_44 => X"1123F023246AD0051001B0239201E442002F1201A020E46AC210B120F0231000",
                    INIT_45 => X"032024581201E45F002FA020245FC3200310A458C310B12013080320E2100100",
                    INIT_46 => X"1163112011641161114250002442E30090011028B0238300A01091011124B123",
                    INIT_47 => X"116F1172117211455000005900641A6B1B0411001164116E1161116D116D116F",
                    INIT_48 => X"123850000059D1010002D20100020041002000641A7C1B0411001120113A1172",
                    INIT_49 => X"01401128A50000401024E4BAC400B0231400A492C1201101E0101000112C1204",
                    INIT_4A => X"001C9601A1601601A260A4B3C650A49896021401172C4706470607400650A610",
                    INIT_4B => X"E0101100B1209001000650002498E070002FA0606498C650160124A71701E070",
                    INIT_4C => X"1177116F116C1166117211651176114F24BB841104DAE4D2D120F1201101B120",
                    INIT_4D => X"D10A24F0D10DA1009001B02024BB00600087005900641AC81B04005911001121",
                    INIT_4E => X"5000400C1000D10100025000400C1000F0209001B020E4EBD12024F7D10824F0",
                    INIT_4F => X"D10111200002D10111080002A5069002B0205000400C1001F0209001B0200059",
                    INIT_50 => X"00000000000000005000400C1000F0209001B020F0209001B020D10111080002",
                    INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"A0AAAAAAAA96B6A2A28AAAA7D41F5552935DD7776484785538820820820B0B0A",
                   INITP_01 => X"D2D560755806D56036D6128D5882AD588308AAAA434AAB44DAA88A2AA82A2AAA",
                   INITP_02 => X"D082AAAAAAAAAAAAAAAAAAAAAAA2288A228882AAAAAAAA108234B4AD61228A10",
                   INITP_03 => X"2A5A5ADD82A6AA351548AAA20B429AA848B420A6AA1228B42AA2828AAA228A62",
                   INITP_04 => X"28A2A08A2160234A2160234A215408D2A552288228822205818602220586008D",
                   INITP_05 => X"8D28812059034908B048DD28800581800581234A200234A20008D2A8AA28A28A",
                   INITP_06 => X"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA890D580B04",
                   INITP_07 => X"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
                   INITP_08 => X"AAA82AAAAAAA5114278D34492D479348AAAA9496A165DDA574D37602AAAAAAAA",
                   INITP_09 => X"8A2D249292A49377744AA82AAAAABB6490AA8D6691375544413435812AAA20AA",
                   INITP_0A => X"0000000000000000000000000000000000000000000000000000000000924922",
                   INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a(31 downto 0),
                      DOPADOP => data_out_a(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b(31 downto 0),
                      DOPBDOP => data_out_b(35 downto 32), 
                        DIBDI => data_in_b(31 downto 0),
                      DIPBDIP => data_in_b(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
    end generate akv7;
    --
  end generate ram_2k_generate;
  --
  --	
  ram_4k_generate : if (C_RAM_SIZE_KWORDS = 4) generate
    s6: if (C_FAMILY = "S6") generate
      --
      address_a(13 downto 0) <= address(10 downto 0) & "000";
      data_in_a <= "000000000000000000000000000000000000";
      --
      s6_a11_flop: FD
      port map (  D => address(11),
                  Q => pipe_a11,
                  C => clk);
      --
      s6_4k_mux0_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(0),
                I1 => data_out_a_hl(0),
                I2 => data_out_a_ll(1),
                I3 => data_out_a_hl(1),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(0),
                O6 => instruction(1));
      --
      s6_4k_mux2_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(2),
                I1 => data_out_a_hl(2),
                I2 => data_out_a_ll(3),
                I3 => data_out_a_hl(3),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(2),
                O6 => instruction(3));
      --
      s6_4k_mux4_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(4),
                I1 => data_out_a_hl(4),
                I2 => data_out_a_ll(5),
                I3 => data_out_a_hl(5),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(4),
                O6 => instruction(5));
      --
      s6_4k_mux6_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(6),
                I1 => data_out_a_hl(6),
                I2 => data_out_a_ll(7),
                I3 => data_out_a_hl(7),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(6),
                O6 => instruction(7));
      --
      s6_4k_mux8_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_ll(32),
                I1 => data_out_a_hl(32),
                I2 => data_out_a_lh(0),
                I3 => data_out_a_hh(0),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(8),
                O6 => instruction(9));
      --
      s6_4k_mux10_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_lh(1),
                I1 => data_out_a_hh(1),
                I2 => data_out_a_lh(2),
                I3 => data_out_a_hh(2),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(10),
                O6 => instruction(11));
      --
      s6_4k_mux12_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_lh(3),
                I1 => data_out_a_hh(3),
                I2 => data_out_a_lh(4),
                I3 => data_out_a_hh(4),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(12),
                O6 => instruction(13));
      --
      s6_4k_mux14_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_lh(5),
                I1 => data_out_a_hh(5),
                I2 => data_out_a_lh(6),
                I3 => data_out_a_hh(6),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(14),
                O6 => instruction(15));
      --
      s6_4k_mux16_lut: LUT6_2
      generic map (INIT => X"FF00F0F0CCCCAAAA")
      port map( I0 => data_out_a_lh(7),
                I1 => data_out_a_hh(7),
                I2 => data_out_a_lh(32),
                I3 => data_out_a_hh(32),
                I4 => pipe_a11,
                I5 => '1',
                O5 => instruction(16),
                O6 => instruction(17));
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b_ll <= "000" & data_out_b_ll(32) & "000000000000000000000000" & data_out_b_ll(7 downto 0);
        data_in_b_lh <= "000" & data_out_b_lh(32) & "000000000000000000000000" & data_out_b_lh(7 downto 0);
        data_in_b_hl <= "000" & data_out_b_hl(32) & "000000000000000000000000" & data_out_b_hl(7 downto 0);
        data_in_b_hh <= "000" & data_out_b_hh(32) & "000000000000000000000000" & data_out_b_hh(7 downto 0);
        address_b(13 downto 0) <= "00000000000000";
        we_b_l(3 downto 0) <= "0000";
        we_b_h(3 downto 0) <= "0000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
        jtag_dout <= data_out_b_lh(32) & data_out_b_lh(7 downto 0) & data_out_b_ll(32) & data_out_b_ll(7 downto 0);
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b_lh <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_ll <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        data_in_b_hh <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_hl <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        address_b(13 downto 0) <= jtag_addr(10 downto 0) & "000";
        --
        s6_4k_jtag_we_lut: LUT6_2
        generic map (INIT => X"8000000020000000")
        port map( I0 => jtag_we,
                  I1 => jtag_addr(11),
                  I2 => '1',
                  I3 => '1',
                  I4 => '1',
                  I5 => '1',
                  O5 => jtag_we_l,
                  O6 => jtag_we_h);
        --
        we_b_l(3 downto 0) <= jtag_we_l & jtag_we_l & jtag_we_l & jtag_we_l;
        we_b_h(3 downto 0) <= jtag_we_h & jtag_we_h & jtag_we_h & jtag_we_h;
        --
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
        --
        s6_4k_jtag_mux0_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(0),
                  I1 => data_out_b_hl(0),
                  I2 => data_out_b_ll(1),
                  I3 => data_out_b_hl(1),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(0),
                  O6 => jtag_dout(1));
        --
        s6_4k_jtag_mux2_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(2),
                  I1 => data_out_b_hl(2),
                  I2 => data_out_b_ll(3),
                  I3 => data_out_b_hl(3),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(2),
                  O6 => jtag_dout(3));
        --
        s6_4k_jtag_mux4_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(4),
                  I1 => data_out_b_hl(4),
                  I2 => data_out_b_ll(5),
                  I3 => data_out_b_hl(5),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(4),
                  O6 => jtag_dout(5));
        --
        s6_4k_jtag_mux6_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(6),
                  I1 => data_out_b_hl(6),
                  I2 => data_out_b_ll(7),
                  I3 => data_out_b_hl(7),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(6),
                  O6 => jtag_dout(7));
        --
        s6_4k_jtag_mux8_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_ll(32),
                  I1 => data_out_b_hl(32),
                  I2 => data_out_b_lh(0),
                  I3 => data_out_b_hh(0),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(8),
                  O6 => jtag_dout(9));
        --
        s6_4k_jtag_mux10_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_lh(1),
                  I1 => data_out_b_hh(1),
                  I2 => data_out_b_lh(2),
                  I3 => data_out_b_hh(2),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(10),
                  O6 => jtag_dout(11));
        --
        s6_4k_jtag_mux12_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_lh(3),
                  I1 => data_out_b_hh(3),
                  I2 => data_out_b_lh(4),
                  I3 => data_out_b_hh(4),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(12),
                  O6 => jtag_dout(13));
        --
        s6_4k_jtag_mux14_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_lh(5),
                  I1 => data_out_b_hh(5),
                  I2 => data_out_b_lh(6),
                  I3 => data_out_b_hh(6),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(14),
                  O6 => jtag_dout(15));
        --
        s6_4k_jtag_mux16_lut: LUT6_2
        generic map (INIT => X"FF00F0F0CCCCAAAA")
        port map( I0 => data_out_b_lh(7),
                  I1 => data_out_b_hh(7),
                  I2 => data_out_b_lh(32),
                  I3 => data_out_b_hh(32),
                  I4 => jtag_addr(11),
                  I5 => '1',
                  O5 => jtag_dout(16),
                  O6 => jtag_dout(17));
      --
      end generate loader;
      --
      kcpsm6_rom_ll: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"002C2F200002000010000004000020000001000008000600080002000400BB81",
                    INIT_01 => X"000E0040100A3D2A40100A3D11400A3D30000E0000300E01002C2F1006060606",
                    INIT_02 => X"00010A02010D020001020102410007525241370F00074B4B41370E0E0E0E0000",
                    INIT_03 => X"59646D00002E73746E656D75677261206461420064000101026C00A000013E02",
                    INIT_04 => X"A7FF010201024130012002013A02010201024130000023200000603501318700",
                    INIT_05 => X"024000BA080202080403008100798F40102B27B304237900598C599803030130",
                    INIT_06 => X"40E90100D8400110000740000280CB8001100530024000040300BF0801013005",
                    INIT_07 => X"7C012300F50401010110000008EC040101011000004004FCE104010101100000",
                    INIT_08 => X"DD30011E001D0323D57C022300590D400153300740017802013002B61001102C",
                    INIT_09 => X"60012E025370B60164260100203A726556205454545420534D4300C71001102C",
                    INIT_0A => X"746E756F632064726F5700203A72656666754200595340012E025350012E0253",
                    INIT_0B => X"85202000644D01002064726F5700202020203A73657A69732064726F5700203A",
                    INIT_0C => X"2301020102412302645601590129020102010241200128020120027E01011002",
                    INIT_0D => X"3028012002C840002364630159A1010102010241103024014002012002B34000",
                    INIT_0E => X"502C0606013001200201020102413064730159EB40002359B901010201024110",
                    INIT_0F => X"230059EF000359EF00010102FDFFF900A015030059E101010201024160CB5003",
                    INIT_10 => X"1020300110200410203440B633C739100F20010110200210FE102C40B6397C03",
                    INIT_11 => X"59530E0E0E0E40B63100C738400140B63800C734400140B634C733100F200101",
                    INIT_12 => X"360010010100102C40B6367C012300C7314010060606060F2C40B6317C012300",
                    INIT_13 => X"02009001008B010059530F40B63500C7370010010100102C40B6377C012300C7",
                    INIT_14 => X"00F040FF0F2CB6357C012300952C230100A42C2301009004008B04009002008B",
                    INIT_15 => X"0F10010100102C0440B6317C022300C7354000F0400F2CB6357C012300C73540",
                    INIT_16 => X"D5D810000440B6337C037C002300C7310020F0000F100101001030010020F000",
                    INIT_17 => X"B6337C012300C7330020000440100F0001011005400030F8012310044010402C",
                    INIT_18 => X"A9000067726169746C756D00C73310044011083001011000402CD5FE10000440",
                    INIT_19 => X"007379733501006E6F6973726576B4000074657365728B0000706D75646D656D",
                    INIT_1A => X"6773656D61D302006773656D62FD000072721401007772ED0100706C65687901",
                    INIT_1B => X"9502007369645F61316CA402006E655F61316CB20200676E725F61316CFB0200",
                    INIT_1C => X"655F61316C7C02007265705F7369645F61316C7902007265705F6E655F61316C",
                    INIT_1D => X"655F6E655F61316C820200646E61725F7369645F61316C7F0200646E61725F6E",
                    INIT_1E => X"61316C5202007265705F61316C8802007478655F7369645F61316C8502007478",
                    INIT_1F => X"37020073656C75725F74656741020073656C75725F746573620200646E61725F",
                    INIT_20 => X"012310202300203CFF2F00A0001503FF2F020061316CFF01007664615F61316C",
                    INIT_21 => X"0060778700608710A0000110A000018F40140000032300012B003CFFA0140001",
                    INIT_22 => X"2058015F2F205F20105810200820100023236A05012301422F01206A10202300",
                    INIT_23 => X"6F7272450059646B0400646E616D6D6F63206461420042000128230010012423",
                    INIT_24 => X"4028004024BA00230092200110002C04380059010201024120647C0400203A72",
                    INIT_25 => X"10002001060098702F60985001A701701C01600160B3509802012C0606405010",
                    INIT_26 => X"0AF00D000120BB60875964C804590021776F6C667265764FBB11DAD220200120",
                    INIT_27 => X"012002010802060220000C0120012059000C000102000C00200120EB20F708F0",
                    INIT_28 => X"0000000000000000000000000000000000000000000C00200120200120010802",
                    INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"0160B416A660850843844202600208101FFFE536008278000000008F80000000",
                   INITP_01 => X"19AFA5047C405CE0C717060740210018D3FFFFFFFFFFE000003FFFCD3C03980D",
                   INITP_02 => X"00E098183000F12F12007801E00421249200110011001E800000000212202210",
                   INITP_03 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1A88C",
                   INITP_04 => X"DA00100AB00BFF0F6002203CE032101FF0FFF91781BD800400CC125A00B3FFFF",
                   INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000006",
                   INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_ll(31 downto 0),
                  DOPA => data_out_a_ll(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_ll(31 downto 0),
                  DOPB => data_out_b_ll(35 downto 32), 
                   DIB => data_in_b_ll(31 downto 0),
                  DIPB => data_in_b_ll(35 downto 32), 
                   WEB => we_b_l(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
      -- 
      kcpsm6_rom_lh: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"01F0000028684828684828684828684828684828684810C86848108868481200",
                    INIT_01 => X"28A008D0E888D0C8D0E888D0C8D0E8D0C828A0082800A00881F00000A1A1A1A1",
                    INIT_02 => X"2868080068080028680069000028C890F0E8881800C990F0E989A1A1A1A10128",
                    INIT_03 => X"00000D0D08080808080808080808080808080828109D8D680090E82528680800",
                    INIT_04 => X"90E9680069000050680800680800680069000000092878780828000058580028",
                    INIT_05 => X"69090990684868086868280028000202815859D0E8580028001000B0E8188900",
                    INIT_06 => X"0890E828F0E8C870088808286808B0E989896A52690909686828B0E98989724A",
                    INIT_07 => X"B0E85828D0E9898888715109C8D0E9898888715109088810B0E9898888715109",
                    INIT_08 => X"000808100890E85800D0E8582800F0E9C9005089096808006808000050885008",
                    INIT_09 => X"0068080000000008000D0D080808080808080808080808080808280050885008",
                    INIT_0A => X"0808080808080808080808080808080808080828000000680800000068080000",
                    INIT_0B => X"F0E05908000D0D08080808080808080808080808080808080808080808080808",
                    INIT_0C => X"5A68006900005800000D0D006808006800690000006808006808001088685000",
                    INIT_0D => X"8008680800F0E1095A000D0D0010896800690000508008680800680800F0E109",
                    INIT_0E => X"038AA2A28902680800680069000000000D0D00F0E1095A001089680069000050",
                    INIT_0F => X"582800109D8D00109D8D680090E890E8250D0D280010CB680069000050D0E38B",
                    INIT_10 => X"705009887008887050090800080008701850888970508870185009080008B0E8",
                    INIT_11 => X"0000A0A0A0A05800082800087828580008280008782858000800087018508889",
                    INIT_12 => X"0871518888715108080008B0E8582800087820A0A0A0A01858580008B0E85828",
                    INIT_13 => X"082801082801082800001858000828000871518888715108080008B0E8582800",
                    INIT_14 => X"1018583818580008B0E858280178780828017878082801082801082801082801",
                    INIT_15 => X"1951888871510888080008B0E8582800087820185818580008B0E85828000878",
                    INIT_16 => X"00B1685088080008F0E890E85828000871211951195188887151088871211951",
                    INIT_17 => X"0008B0E85828000870285088087119518888718808510891E858708808087858",
                    INIT_18 => X"080808080808080808080828000870880808B1E888887151080800B168508808",
                    INIT_19 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1A => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1B => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1C => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1D => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1E => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1F => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_20 => X"89B2E050F2E15892E892E825090D0D0808080808080808080808080808080808",
                    INIT_21 => X"2800020028000021259D8D01259D8D020212099D8D129D8D92E892E825129D8D",
                    INIT_22 => X"011289F2005092E101D2E15889017180087892E88858C9F2008950F2E1587808",
                    INIT_23 => X"080808082800000D0D080808080808080808080808281271C88858C150C88858",
                    INIT_24 => X"8008528008F2E2580AD2E08870080889092800680069000000000D0D08080808",
                    INIT_25 => X"70885848002812700050B2E38B128B7000CB508B51D2E3D2CB8A8BA3A3038353",
                    INIT_26 => X"E892E850C85812000000000D0D000808080808080808080812C202F2E8788858",
                    INIT_27 => X"680800680800D2C85828A00878C8580028A008680028A00878C858F2E892E892",
                    INIT_28 => X"000000000000000000000000000000000000000028A00878C85878C858680800",
                    INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"98442184591A29E292FF13F0BEB7E77FCFFFF9DDDBFD83019295486069249333",
                   INITP_01 => X"733A9DF402FD31BE2C4DF16C7D9BF5B589FFFFFFFFFFFD6B5A9FFFF094CE45B0",
                   INITP_02 => X"A6842122C2A680880853414D029EF6DB6DCB445344534029C16969508915090A",
                   INITP_03 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA2230",
                   INITP_04 => X"B6499C9543E7FFF48FA5850004487F4FFE7FFF005A426192FF89C4AC4951FFFF",
                   INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000925",
                   INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_lh(31 downto 0),
                  DOPA => data_out_a_lh(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_lh(31 downto 0),
                  DOPB => data_out_b_lh(35 downto 32), 
                   DIB => data_in_b_lh(31 downto 0),
                  DIPB => data_in_b_lh(35 downto 32), 
                   WEB => we_b_l(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
      --
      kcpsm6_rom_hl: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_hl(31 downto 0),
                  DOPA => data_out_a_hl(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_hl(31 downto 0),
                  DOPB => data_out_b_hl(35 downto 32), 
                   DIB => data_in_b_hl(31 downto 0),
                  DIPB => data_in_b_hl(35 downto 32), 
                   WEB => we_b_h(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
      -- 
      kcpsm6_rom_hh: RAMB16BWER
      generic map ( DATA_WIDTH_A => 9,
                    DOA_REG => 0,
                    EN_RSTRAM_A => FALSE,
                    INIT_A => X"000000000",
                    RST_PRIORITY_A => "CE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    DATA_WIDTH_B => 9,
                    DOB_REG => 0,
                    EN_RSTRAM_B => FALSE,
                    INIT_B => X"000000000",
                    RST_PRIORITY_B => "CE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    RSTTYPE => "SYNC",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    SIM_DEVICE => "SPARTAN6",
                    INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(  ADDRA => address_a(13 downto 0),
                   ENA => enable,
                  CLKA => clk,
                   DOA => data_out_a_hh(31 downto 0),
                  DOPA => data_out_a_hh(35 downto 32), 
                   DIA => data_in_a(31 downto 0),
                  DIPA => data_in_a(35 downto 32), 
                   WEA => "0000",
                REGCEA => '0',
                  RSTA => '0',
                 ADDRB => address_b(13 downto 0),
                   ENB => enable_b,
                  CLKB => clk_b,
                   DOB => data_out_b_hh(31 downto 0),
                  DOPB => data_out_b_hh(35 downto 32), 
                   DIB => data_in_b_hh(31 downto 0),
                  DIPB => data_in_b_hh(35 downto 32), 
                   WEB => we_b_h(3 downto 0),
                REGCEB => '0',
                  RSTB => '0');
    --
    end generate s6;
    --
    --
    v6 : if (C_FAMILY = "V6") generate
      --
      address_a <= '1' & address(11 downto 0) & "111";
      instruction <= data_out_a_h(32) & data_out_a_h(7 downto 0) & data_out_a_l(32) & data_out_a_l(7 downto 0);
      data_in_a <= "000000000000000000000000000000000000";
      jtag_dout <= data_out_b_h(32) & data_out_b_h(7 downto 0) & data_out_b_l(32) & data_out_b_l(7 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b_l <= "000" & data_out_b_l(32) & "000000000000000000000000" & data_out_b_l(7 downto 0);
        data_in_b_h <= "000" & data_out_b_h(32) & "000000000000000000000000" & data_out_b_h(7 downto 0);
        address_b <= "1111111111111111";
        we_b <= "00000000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b_h <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_l <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        address_b <= '1' & jtag_addr(11 downto 0) & "111";
        we_b <= jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom_l: RAMB36E1
      generic map ( READ_WIDTH_A => 9,
                    WRITE_WIDTH_A => 9,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 9,
                    WRITE_WIDTH_B => 9,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "VIRTEX6",
                    INIT_00 => X"002C2F200002000010000004000020000001000008000600080002000400BB81",
                    INIT_01 => X"000E0040100A3D2A40100A3D11400A3D30000E0000300E01002C2F1006060606",
                    INIT_02 => X"00010A02010D020001020102410007525241370F00074B4B41370E0E0E0E0000",
                    INIT_03 => X"59646D00002E73746E656D75677261206461420064000101026C00A000013E02",
                    INIT_04 => X"A7FF010201024130012002013A02010201024130000023200000603501318700",
                    INIT_05 => X"024000BA080202080403008100798F40102B27B304237900598C599803030130",
                    INIT_06 => X"40E90100D8400110000740000280CB8001100530024000040300BF0801013005",
                    INIT_07 => X"7C012300F50401010110000008EC040101011000004004FCE104010101100000",
                    INIT_08 => X"DD30011E001D0323D57C022300590D400153300740017802013002B61001102C",
                    INIT_09 => X"60012E025370B60164260100203A726556205454545420534D4300C71001102C",
                    INIT_0A => X"746E756F632064726F5700203A72656666754200595340012E025350012E0253",
                    INIT_0B => X"85202000644D01002064726F5700202020203A73657A69732064726F5700203A",
                    INIT_0C => X"2301020102412302645601590129020102010241200128020120027E01011002",
                    INIT_0D => X"3028012002C840002364630159A1010102010241103024014002012002B34000",
                    INIT_0E => X"502C0606013001200201020102413064730159EB40002359B901010201024110",
                    INIT_0F => X"230059EF000359EF00010102FDFFF900A015030059E101010201024160CB5003",
                    INIT_10 => X"1020300110200410203440B633C739100F20010110200210FE102C40B6397C03",
                    INIT_11 => X"59530E0E0E0E40B63100C738400140B63800C734400140B634C733100F200101",
                    INIT_12 => X"360010010100102C40B6367C012300C7314010060606060F2C40B6317C012300",
                    INIT_13 => X"02009001008B010059530F40B63500C7370010010100102C40B6377C012300C7",
                    INIT_14 => X"00F040FF0F2CB6357C012300952C230100A42C2301009004008B04009002008B",
                    INIT_15 => X"0F10010100102C0440B6317C022300C7354000F0400F2CB6357C012300C73540",
                    INIT_16 => X"D5D810000440B6337C037C002300C7310020F0000F100101001030010020F000",
                    INIT_17 => X"B6337C012300C7330020000440100F0001011005400030F8012310044010402C",
                    INIT_18 => X"A9000067726169746C756D00C73310044011083001011000402CD5FE10000440",
                    INIT_19 => X"007379733501006E6F6973726576B4000074657365728B0000706D75646D656D",
                    INIT_1A => X"6773656D61D302006773656D62FD000072721401007772ED0100706C65687901",
                    INIT_1B => X"9502007369645F61316CA402006E655F61316CB20200676E725F61316CFB0200",
                    INIT_1C => X"655F61316C7C02007265705F7369645F61316C7902007265705F6E655F61316C",
                    INIT_1D => X"655F6E655F61316C820200646E61725F7369645F61316C7F0200646E61725F6E",
                    INIT_1E => X"61316C5202007265705F61316C8802007478655F7369645F61316C8502007478",
                    INIT_1F => X"37020073656C75725F74656741020073656C75725F746573620200646E61725F",
                    INIT_20 => X"012310202300203CFF2F00A0001503FF2F020061316CFF01007664615F61316C",
                    INIT_21 => X"0060778700608710A0000110A000018F40140000032300012B003CFFA0140001",
                    INIT_22 => X"2058015F2F205F20105810200820100023236A05012301422F01206A10202300",
                    INIT_23 => X"6F7272450059646B0400646E616D6D6F63206461420042000128230010012423",
                    INIT_24 => X"4028004024BA00230092200110002C04380059010201024120647C0400203A72",
                    INIT_25 => X"10002001060098702F60985001A701701C01600160B3509802012C0606405010",
                    INIT_26 => X"0AF00D000120BB60875964C804590021776F6C667265764FBB11DAD220200120",
                    INIT_27 => X"012002010802060220000C0120012059000C000102000C00200120EB20F708F0",
                    INIT_28 => X"0000000000000000000000000000000000000000000C00200120200120010802",
                    INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_40 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_41 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_42 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_43 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_44 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_45 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_46 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_47 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_48 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_49 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"0160B416A660850843844202600208101FFFE536008278000000008F80000000",
                   INITP_01 => X"19AFA5047C405CE0C717060740210018D3FFFFFFFFFFE000003FFFCD3C03980D",
                   INITP_02 => X"00E098183000F12F12007801E00421249200110011001E800000000212202210",
                   INITP_03 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1A88C",
                   INITP_04 => X"DA00100AB00BFF0F6002203CE032101FF0FFF91781BD800400CC125A00B3FFFF",
                   INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000006",
                   INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a_l(31 downto 0),
                      DOPADOP => data_out_a_l(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b_l(31 downto 0),
                      DOPBDOP => data_out_b_l(35 downto 32), 
                        DIBDI => data_in_b_l(31 downto 0),
                      DIPBDIP => data_in_b_l(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
      kcpsm6_rom_h: RAMB36E1
      generic map ( READ_WIDTH_A => 9,
                    WRITE_WIDTH_A => 9,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 9,
                    WRITE_WIDTH_B => 9,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "VIRTEX6",
                    INIT_00 => X"01F0000028684828684828684828684828684828684810C86848108868481200",
                    INIT_01 => X"28A008D0E888D0C8D0E888D0C8D0E8D0C828A0082800A00881F00000A1A1A1A1",
                    INIT_02 => X"2868080068080028680069000028C890F0E8881800C990F0E989A1A1A1A10128",
                    INIT_03 => X"00000D0D08080808080808080808080808080828109D8D680090E82528680800",
                    INIT_04 => X"90E9680069000050680800680800680069000000092878780828000058580028",
                    INIT_05 => X"69090990684868086868280028000202815859D0E8580028001000B0E8188900",
                    INIT_06 => X"0890E828F0E8C870088808286808B0E989896A52690909686828B0E98989724A",
                    INIT_07 => X"B0E85828D0E9898888715109C8D0E9898888715109088810B0E9898888715109",
                    INIT_08 => X"000808100890E85800D0E8582800F0E9C9005089096808006808000050885008",
                    INIT_09 => X"0068080000000008000D0D080808080808080808080808080808280050885008",
                    INIT_0A => X"0808080808080808080808080808080808080828000000680800000068080000",
                    INIT_0B => X"F0E05908000D0D08080808080808080808080808080808080808080808080808",
                    INIT_0C => X"5A68006900005800000D0D006808006800690000006808006808001088685000",
                    INIT_0D => X"8008680800F0E1095A000D0D0010896800690000508008680800680800F0E109",
                    INIT_0E => X"038AA2A28902680800680069000000000D0D00F0E1095A001089680069000050",
                    INIT_0F => X"582800109D8D00109D8D680090E890E8250D0D280010CB680069000050D0E38B",
                    INIT_10 => X"705009887008887050090800080008701850888970508870185009080008B0E8",
                    INIT_11 => X"0000A0A0A0A05800082800087828580008280008782858000800087018508889",
                    INIT_12 => X"0871518888715108080008B0E8582800087820A0A0A0A01858580008B0E85828",
                    INIT_13 => X"082801082801082800001858000828000871518888715108080008B0E8582800",
                    INIT_14 => X"1018583818580008B0E858280178780828017878082801082801082801082801",
                    INIT_15 => X"1951888871510888080008B0E8582800087820185818580008B0E85828000878",
                    INIT_16 => X"00B1685088080008F0E890E85828000871211951195188887151088871211951",
                    INIT_17 => X"0008B0E85828000870285088087119518888718808510891E858708808087858",
                    INIT_18 => X"080808080808080808080828000870880808B1E888887151080800B168508808",
                    INIT_19 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1A => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1B => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1C => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1D => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1E => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1F => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_20 => X"89B2E050F2E15892E892E825090D0D0808080808080808080808080808080808",
                    INIT_21 => X"2800020028000021259D8D01259D8D020212099D8D129D8D92E892E825129D8D",
                    INIT_22 => X"011289F2005092E101D2E15889017180087892E88858C9F2008950F2E1587808",
                    INIT_23 => X"080808082800000D0D080808080808080808080808281271C88858C150C88858",
                    INIT_24 => X"8008528008F2E2580AD2E08870080889092800680069000000000D0D08080808",
                    INIT_25 => X"70885848002812700050B2E38B128B7000CB508B51D2E3D2CB8A8BA3A3038353",
                    INIT_26 => X"E892E850C85812000000000D0D000808080808080808080812C202F2E8788858",
                    INIT_27 => X"680800680800D2C85828A00878C8580028A008680028A00878C858F2E892E892",
                    INIT_28 => X"000000000000000000000000000000000000000028A00878C85878C858680800",
                    INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_40 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_41 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_42 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_43 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_44 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_45 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_46 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_47 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_48 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_49 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"98442184591A29E292FF13F0BEB7E77FCFFFF9DDDBFD83019295486069249333",
                   INITP_01 => X"733A9DF402FD31BE2C4DF16C7D9BF5B589FFFFFFFFFFFD6B5A9FFFF094CE45B0",
                   INITP_02 => X"A6842122C2A680880853414D029EF6DB6DCB445344534029C16969508915090A",
                   INITP_03 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA2230",
                   INITP_04 => X"B6499C9543E7FFF48FA5850004487F4FFE7FFF005A426192FF89C4AC4951FFFF",
                   INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000925",
                   INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a_h(31 downto 0),
                      DOPADOP => data_out_a_h(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b_h(31 downto 0),
                      DOPBDOP => data_out_b_h(35 downto 32), 
                        DIBDI => data_in_b_h(31 downto 0),
                      DIPBDIP => data_in_b_h(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
    end generate v6;
    --
    --
    akv7 : if (C_FAMILY = "7S") generate
      --
      address_a <= '1' & address(11 downto 0) & "111";
      instruction <= data_out_a_h(32) & data_out_a_h(7 downto 0) & data_out_a_l(32) & data_out_a_l(7 downto 0);
      data_in_a <= "000000000000000000000000000000000000";
      jtag_dout <= data_out_b_h(32) & data_out_b_h(7 downto 0) & data_out_b_l(32) & data_out_b_l(7 downto 0);
      --
      no_loader : if (C_JTAG_LOADER_ENABLE = 0) generate
        data_in_b_l <= "000" & data_out_b_l(32) & "000000000000000000000000" & data_out_b_l(7 downto 0);
        data_in_b_h <= "000" & data_out_b_h(32) & "000000000000000000000000" & data_out_b_h(7 downto 0);
        address_b <= "1111111111111111";
        we_b <= "00000000";
        enable_b <= '0';
        rdl <= '0';
        clk_b <= '0';
      end generate no_loader;
      --
      loader : if (C_JTAG_LOADER_ENABLE = 1) generate
        data_in_b_h <= "000" & jtag_din(17) & "000000000000000000000000" & jtag_din(16 downto 9);
        data_in_b_l <= "000" & jtag_din(8) & "000000000000000000000000" & jtag_din(7 downto 0);
        address_b <= '1' & jtag_addr(11 downto 0) & "111";
        we_b <= jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we & jtag_we;
        enable_b <= jtag_en(0);
        rdl <= rdl_bus(0);
        clk_b <= jtag_clk;
      end generate loader;
      --
      kcpsm6_rom_l: RAMB36E1
      generic map ( READ_WIDTH_A => 9,
                    WRITE_WIDTH_A => 9,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 9,
                    WRITE_WIDTH_B => 9,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "7SERIES",
                    INIT_00 => X"002C2F200002000010000004000020000001000008000600080002000400BB81",
                    INIT_01 => X"000E0040100A3D2A40100A3D11400A3D30000E0000300E01002C2F1006060606",
                    INIT_02 => X"00010A02010D020001020102410007525241370F00074B4B41370E0E0E0E0000",
                    INIT_03 => X"59646D00002E73746E656D75677261206461420064000101026C00A000013E02",
                    INIT_04 => X"A7FF010201024130012002013A02010201024130000023200000603501318700",
                    INIT_05 => X"024000BA080202080403008100798F40102B27B304237900598C599803030130",
                    INIT_06 => X"40E90100D8400110000740000280CB8001100530024000040300BF0801013005",
                    INIT_07 => X"7C012300F50401010110000008EC040101011000004004FCE104010101100000",
                    INIT_08 => X"DD30011E001D0323D57C022300590D400153300740017802013002B61001102C",
                    INIT_09 => X"60012E025370B60164260100203A726556205454545420534D4300C71001102C",
                    INIT_0A => X"746E756F632064726F5700203A72656666754200595340012E025350012E0253",
                    INIT_0B => X"85202000644D01002064726F5700202020203A73657A69732064726F5700203A",
                    INIT_0C => X"2301020102412302645601590129020102010241200128020120027E01011002",
                    INIT_0D => X"3028012002C840002364630159A1010102010241103024014002012002B34000",
                    INIT_0E => X"502C0606013001200201020102413064730159EB40002359B901010201024110",
                    INIT_0F => X"230059EF000359EF00010102FDFFF900A015030059E101010201024160CB5003",
                    INIT_10 => X"1020300110200410203440B633C739100F20010110200210FE102C40B6397C03",
                    INIT_11 => X"59530E0E0E0E40B63100C738400140B63800C734400140B634C733100F200101",
                    INIT_12 => X"360010010100102C40B6367C012300C7314010060606060F2C40B6317C012300",
                    INIT_13 => X"02009001008B010059530F40B63500C7370010010100102C40B6377C012300C7",
                    INIT_14 => X"00F040FF0F2CB6357C012300952C230100A42C2301009004008B04009002008B",
                    INIT_15 => X"0F10010100102C0440B6317C022300C7354000F0400F2CB6357C012300C73540",
                    INIT_16 => X"D5D810000440B6337C037C002300C7310020F0000F100101001030010020F000",
                    INIT_17 => X"B6337C012300C7330020000440100F0001011005400030F8012310044010402C",
                    INIT_18 => X"A9000067726169746C756D00C73310044011083001011000402CD5FE10000440",
                    INIT_19 => X"007379733501006E6F6973726576B4000074657365728B0000706D75646D656D",
                    INIT_1A => X"6773656D61D302006773656D62FD000072721401007772ED0100706C65687901",
                    INIT_1B => X"9502007369645F61316CA402006E655F61316CB20200676E725F61316CFB0200",
                    INIT_1C => X"655F61316C7C02007265705F7369645F61316C7902007265705F6E655F61316C",
                    INIT_1D => X"655F6E655F61316C820200646E61725F7369645F61316C7F0200646E61725F6E",
                    INIT_1E => X"61316C5202007265705F61316C8802007478655F7369645F61316C8502007478",
                    INIT_1F => X"37020073656C75725F74656741020073656C75725F746573620200646E61725F",
                    INIT_20 => X"012310202300203CFF2F00A0001503FF2F020061316CFF01007664615F61316C",
                    INIT_21 => X"0060778700608710A0000110A000018F40140000032300012B003CFFA0140001",
                    INIT_22 => X"2058015F2F205F20105810200820100023236A05012301422F01206A10202300",
                    INIT_23 => X"6F7272450059646B0400646E616D6D6F63206461420042000128230010012423",
                    INIT_24 => X"4028004024BA00230092200110002C04380059010201024120647C0400203A72",
                    INIT_25 => X"10002001060098702F60985001A701701C01600160B3509802012C0606405010",
                    INIT_26 => X"0AF00D000120BB60875964C804590021776F6C667265764FBB11DAD220200120",
                    INIT_27 => X"012002010802060220000C0120012059000C000102000C00200120EB20F708F0",
                    INIT_28 => X"0000000000000000000000000000000000000000000C00200120200120010802",
                    INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_40 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_41 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_42 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_43 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_44 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_45 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_46 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_47 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_48 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_49 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"0160B416A660850843844202600208101FFFE536008278000000008F80000000",
                   INITP_01 => X"19AFA5047C405CE0C717060740210018D3FFFFFFFFFFE000003FFFCD3C03980D",
                   INITP_02 => X"00E098183000F12F12007801E00421249200110011001E800000000212202210",
                   INITP_03 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1A88C",
                   INITP_04 => X"DA00100AB00BFF0F6002203CE032101FF0FFF91781BD800400CC125A00B3FFFF",
                   INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000006",
                   INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a_l(31 downto 0),
                      DOPADOP => data_out_a_l(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b_l(31 downto 0),
                      DOPBDOP => data_out_b_l(35 downto 32), 
                        DIBDI => data_in_b_l(31 downto 0),
                      DIPBDIP => data_in_b_l(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
      kcpsm6_rom_h: RAMB36E1
      generic map ( READ_WIDTH_A => 9,
                    WRITE_WIDTH_A => 9,
                    DOA_REG => 0,
                    INIT_A => X"000000000",
                    RSTREG_PRIORITY_A => "REGCE",
                    SRVAL_A => X"000000000",
                    WRITE_MODE_A => "WRITE_FIRST",
                    READ_WIDTH_B => 9,
                    WRITE_WIDTH_B => 9,
                    DOB_REG => 0,
                    INIT_B => X"000000000",
                    RSTREG_PRIORITY_B => "REGCE",
                    SRVAL_B => X"000000000",
                    WRITE_MODE_B => "WRITE_FIRST",
                    INIT_FILE => "NONE",
                    SIM_COLLISION_CHECK => "ALL",
                    RAM_MODE => "TDP",
                    RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
                    EN_ECC_READ => FALSE,
                    EN_ECC_WRITE => FALSE,
                    RAM_EXTENSION_A => "NONE",
                    RAM_EXTENSION_B => "NONE",
                    SIM_DEVICE => "7SERIES",
                    INIT_00 => X"01F0000028684828684828684828684828684828684810C86848108868481200",
                    INIT_01 => X"28A008D0E888D0C8D0E888D0C8D0E8D0C828A0082800A00881F00000A1A1A1A1",
                    INIT_02 => X"2868080068080028680069000028C890F0E8881800C990F0E989A1A1A1A10128",
                    INIT_03 => X"00000D0D08080808080808080808080808080828109D8D680090E82528680800",
                    INIT_04 => X"90E9680069000050680800680800680069000000092878780828000058580028",
                    INIT_05 => X"69090990684868086868280028000202815859D0E8580028001000B0E8188900",
                    INIT_06 => X"0890E828F0E8C870088808286808B0E989896A52690909686828B0E98989724A",
                    INIT_07 => X"B0E85828D0E9898888715109C8D0E9898888715109088810B0E9898888715109",
                    INIT_08 => X"000808100890E85800D0E8582800F0E9C9005089096808006808000050885008",
                    INIT_09 => X"0068080000000008000D0D080808080808080808080808080808280050885008",
                    INIT_0A => X"0808080808080808080808080808080808080828000000680800000068080000",
                    INIT_0B => X"F0E05908000D0D08080808080808080808080808080808080808080808080808",
                    INIT_0C => X"5A68006900005800000D0D006808006800690000006808006808001088685000",
                    INIT_0D => X"8008680800F0E1095A000D0D0010896800690000508008680800680800F0E109",
                    INIT_0E => X"038AA2A28902680800680069000000000D0D00F0E1095A001089680069000050",
                    INIT_0F => X"582800109D8D00109D8D680090E890E8250D0D280010CB680069000050D0E38B",
                    INIT_10 => X"705009887008887050090800080008701850888970508870185009080008B0E8",
                    INIT_11 => X"0000A0A0A0A05800082800087828580008280008782858000800087018508889",
                    INIT_12 => X"0871518888715108080008B0E8582800087820A0A0A0A01858580008B0E85828",
                    INIT_13 => X"082801082801082800001858000828000871518888715108080008B0E8582800",
                    INIT_14 => X"1018583818580008B0E858280178780828017878082801082801082801082801",
                    INIT_15 => X"1951888871510888080008B0E8582800087820185818580008B0E85828000878",
                    INIT_16 => X"00B1685088080008F0E890E85828000871211951195188887151088871211951",
                    INIT_17 => X"0008B0E85828000870285088087119518888718808510891E858708808087858",
                    INIT_18 => X"080808080808080808080828000870880808B1E888887151080800B168508808",
                    INIT_19 => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1A => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1B => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1C => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1D => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1E => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_1F => X"0808080808080808080808080808080808080808080808080808080808080808",
                    INIT_20 => X"89B2E050F2E15892E892E825090D0D0808080808080808080808080808080808",
                    INIT_21 => X"2800020028000021259D8D01259D8D020212099D8D129D8D92E892E825129D8D",
                    INIT_22 => X"011289F2005092E101D2E15889017180087892E88858C9F2008950F2E1587808",
                    INIT_23 => X"080808082800000D0D080808080808080808080808281271C88858C150C88858",
                    INIT_24 => X"8008528008F2E2580AD2E08870080889092800680069000000000D0D08080808",
                    INIT_25 => X"70885848002812700050B2E38B128B7000CB508B51D2E3D2CB8A8BA3A3038353",
                    INIT_26 => X"E892E850C85812000000000D0D000808080808080808080812C202F2E8788858",
                    INIT_27 => X"680800680800D2C85828A00878C8580028A008680028A00878C858F2E892E892",
                    INIT_28 => X"000000000000000000000000000000000000000028A00878C85878C858680800",
                    INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_40 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_41 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_42 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_43 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_44 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_45 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_46 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_47 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_48 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_49 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
                    INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_00 => X"98442184591A29E292FF13F0BEB7E77FCFFFF9DDDBFD83019295486069249333",
                   INITP_01 => X"733A9DF402FD31BE2C4DF16C7D9BF5B589FFFFFFFFFFFD6B5A9FFFF094CE45B0",
                   INITP_02 => X"A6842122C2A680880853414D029EF6DB6DCB445344534029C16969508915090A",
                   INITP_03 => X"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA2230",
                   INITP_04 => X"B6499C9543E7FFF48FA5850004487F4FFE7FFF005A426192FF89C4AC4951FFFF",
                   INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000925",
                   INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
                   INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000")
      port map(   ADDRARDADDR => address_a,
                      ENARDEN => enable,
                    CLKARDCLK => clk,
                        DOADO => data_out_a_h(31 downto 0),
                      DOPADOP => data_out_a_h(35 downto 32), 
                        DIADI => data_in_a(31 downto 0),
                      DIPADIP => data_in_a(35 downto 32), 
                          WEA => "0000",
                  REGCEAREGCE => '0',
                RSTRAMARSTRAM => '0',
                RSTREGARSTREG => '0',
                  ADDRBWRADDR => address_b,
                      ENBWREN => enable_b,
                    CLKBWRCLK => clk_b,
                        DOBDO => data_out_b_h(31 downto 0),
                      DOPBDOP => data_out_b_h(35 downto 32), 
                        DIBDI => data_in_b_h(31 downto 0),
                      DIPBDIP => data_in_b_h(35 downto 32), 
                        WEBWE => we_b,
                       REGCEB => '0',
                      RSTRAMB => '0',
                      RSTREGB => '0',
                   CASCADEINA => '0',
                   CASCADEINB => '0',
                INJECTDBITERR => '0',
                INJECTSBITERR => '0');
      --
    end generate akv7;
    --
  end generate ram_4k_generate;	              
  --
  --
  --
  --
  -- JTAG Loader
  --
  instantiate_loader : if (C_JTAG_LOADER_ENABLE = 1) generate
  --
    jtag_loader_6_inst : jtag_loader_6
    generic map(              C_FAMILY => C_FAMILY,
                       C_NUM_PICOBLAZE => 1,
                  C_JTAG_LOADER_ENABLE => C_JTAG_LOADER_ENABLE,
                 C_BRAM_MAX_ADDR_WIDTH => BRAM_ADDRESS_WIDTH,
	                  C_ADDR_WIDTH_0 => BRAM_ADDRESS_WIDTH)
    port map( picoblaze_reset => rdl_bus,
                      jtag_en => jtag_en,
                     jtag_din => jtag_din,
                    jtag_addr => jtag_addr(BRAM_ADDRESS_WIDTH-1 downto 0),
                     jtag_clk => jtag_clk,
                      jtag_we => jtag_we,
                  jtag_dout_0 => jtag_dout,
                  jtag_dout_1 => jtag_dout, -- ports 1-7 are not used
                  jtag_dout_2 => jtag_dout, -- in a 1 device debug 
                  jtag_dout_3 => jtag_dout, -- session.  However, Synplify
                  jtag_dout_4 => jtag_dout, -- etc require all ports to
                  jtag_dout_5 => jtag_dout, -- be connected
                  jtag_dout_6 => jtag_dout,
                  jtag_dout_7 => jtag_dout);
    --  
  end generate instantiate_loader;
  --
end low_level_definition;
--
--
-------------------------------------------------------------------------------------------
--
-- JTAG Loader 
--
-------------------------------------------------------------------------------------------
--
--
-- JTAG Loader 6 - Version 6.00
-- Kris Chaplin 4 February 2010
-- Ken Chapman 15 August 2011 - Revised coding style
--
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
--
library unisim;
use unisim.vcomponents.all;
--
entity jtag_loader_6 is
generic(              C_JTAG_LOADER_ENABLE : integer := 1;
                                  C_FAMILY : string := "V6";
                           C_NUM_PICOBLAZE : integer := 1;
                     C_BRAM_MAX_ADDR_WIDTH : integer := 10;
        C_PICOBLAZE_INSTRUCTION_DATA_WIDTH : integer := 18;
                              C_JTAG_CHAIN : integer := 2;
                            C_ADDR_WIDTH_0 : integer := 10;
                            C_ADDR_WIDTH_1 : integer := 10;
                            C_ADDR_WIDTH_2 : integer := 10;
                            C_ADDR_WIDTH_3 : integer := 10;
                            C_ADDR_WIDTH_4 : integer := 10;
                            C_ADDR_WIDTH_5 : integer := 10;
                            C_ADDR_WIDTH_6 : integer := 10;
                            C_ADDR_WIDTH_7 : integer := 10);
port(   picoblaze_reset : out std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
                jtag_en : out std_logic_vector(C_NUM_PICOBLAZE-1 downto 0) := (others => '0');
               jtag_din : out std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0) := (others => '0');
              jtag_addr : out std_logic_vector(C_BRAM_MAX_ADDR_WIDTH-1 downto 0) := (others => '0');
               jtag_clk : out std_logic := '0';
                jtag_we : out std_logic := '0';
            jtag_dout_0 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_1 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_2 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_3 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_4 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_5 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_6 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
            jtag_dout_7 : in  std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0));
end jtag_loader_6;
--
architecture Behavioral of jtag_loader_6 is
  --
  signal num_picoblaze       : std_logic_vector(2 downto 0);
  signal picoblaze_instruction_data_width : std_logic_vector(4 downto 0);
  --
  signal drck                : std_logic;
  signal shift_clk           : std_logic;
  signal shift_din           : std_logic;
  signal shift_dout          : std_logic;
  signal shift               : std_logic;
  signal capture             : std_logic;
  --
  signal control_reg_ce      : std_logic;
  signal bram_ce             : std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
  signal bus_zero            : std_logic_vector(C_NUM_PICOBLAZE-1 downto 0) := (others => '0');
  signal jtag_en_int         : std_logic_vector(C_NUM_PICOBLAZE-1 downto 0);
  signal jtag_en_expanded    : std_logic_vector(7 downto 0) := (others => '0');
  signal jtag_addr_int       : std_logic_vector(C_BRAM_MAX_ADDR_WIDTH-1 downto 0);
  signal jtag_din_int        : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal control_din         : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0):= (others => '0');
  signal control_dout        : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0):= (others => '0');
  signal control_dout_int    : std_logic_vector(7 downto 0):= (others => '0');
  signal bram_dout_int       : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0) := (others => '0');
  signal jtag_we_int         : std_logic;
  signal jtag_clk_int        : std_logic;
  signal bram_ce_valid       : std_logic;
  signal din_load            : std_logic;
  --
  signal jtag_dout_0_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_1_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_2_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_3_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_4_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_5_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_6_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal jtag_dout_7_masked  : std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto 0);
  signal picoblaze_reset_int : std_logic_vector(C_NUM_PICOBLAZE-1 downto 0) := (others => '0');
  --        
begin
  bus_zero <= (others => '0');
  --
  jtag_loader_gen: if (C_JTAG_LOADER_ENABLE = 1) generate
    --
    -- Insert BSCAN primitive for target device architecture.
    --
    BSCAN_SPARTAN6_gen: if (C_FAMILY="S6") generate
    begin
      BSCAN_BLOCK_inst : BSCAN_SPARTAN6
      generic map ( JTAG_CHAIN => C_JTAG_CHAIN)
      port map( CAPTURE => capture,
                   DRCK => drck,
                  RESET => open,
                RUNTEST => open,
                    SEL => bram_ce_valid,
                  SHIFT => shift,
                    TCK => open,
                    TDI => shift_din,
                    TMS => open,
                 UPDATE => jtag_clk_int,
                    TDO => shift_dout);
    end generate BSCAN_SPARTAN6_gen;   
    --
    BSCAN_VIRTEX6_gen: if (C_FAMILY="V6") generate
    begin
      BSCAN_BLOCK_inst: BSCAN_VIRTEX6
      generic map(    JTAG_CHAIN => C_JTAG_CHAIN,
                    DISABLE_JTAG => FALSE)
      port map( CAPTURE => capture,
                   DRCK => drck,
                  RESET => open,
                RUNTEST => open,
                    SEL => bram_ce_valid,
                  SHIFT => shift,
                    TCK => open,
                    TDI => shift_din,
                    TMS => open,
                 UPDATE => jtag_clk_int,
                    TDO => shift_dout);
    end generate BSCAN_VIRTEX6_gen;   
    --
    BSCAN_7SERIES_gen: if (C_FAMILY="7S") generate
    begin
      BSCAN_BLOCK_inst: BSCANE2
      generic map(    JTAG_CHAIN => C_JTAG_CHAIN,
                    DISABLE_JTAG => "FALSE")
      port map( CAPTURE => capture,
                   DRCK => drck,
                  RESET => open,
                RUNTEST => open,
                    SEL => bram_ce_valid,
                  SHIFT => shift,
                    TCK => open,
                    TDI => shift_din,
                    TMS => open,
                 UPDATE => jtag_clk_int,
                    TDO => shift_dout);
    end generate BSCAN_7SERIES_gen;   
    --
    --
    -- Insert clock buffer to ensure reliable shift operations.
    --
    upload_clock: BUFG
    port map( I => drck,
              O => shift_clk);
    --        
    --        
    --  Shift Register      
    --        
    --
    control_reg_ce_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk = '1' then
        if (shift = '1') then
          control_reg_ce <= shift_din;
        end if;
      end if;
    end process control_reg_ce_shift;
    --        
    bram_ce_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk='1' then  
        if (shift = '1') then
          if(C_NUM_PICOBLAZE > 1) then
            for i in 0 to C_NUM_PICOBLAZE-2 loop
              bram_ce(i+1) <= bram_ce(i);
            end loop;
          end if;
          bram_ce(0) <= control_reg_ce;
        end if;
      end if;
    end process bram_ce_shift;
    --        
    bram_we_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk='1' then  
        if (shift = '1') then
          jtag_we_int <= bram_ce(C_NUM_PICOBLAZE-1);
        end if;
      end if;
    end process bram_we_shift;
    --        
    bram_a_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk='1' then  
        if (shift = '1') then
          for i in 0 to C_BRAM_MAX_ADDR_WIDTH-2 loop
            jtag_addr_int(i+1) <= jtag_addr_int(i);
          end loop;
          jtag_addr_int(0) <= jtag_we_int;
        end if;
      end if;
    end process bram_a_shift;
    --        
    bram_d_shift: process (shift_clk)
    begin
      if shift_clk'event and shift_clk='1' then  
        if (din_load = '1') then
          jtag_din_int <= bram_dout_int;
         elsif (shift = '1') then
          for i in 0 to C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-2 loop
            jtag_din_int(i+1) <= jtag_din_int(i);
          end loop;
          jtag_din_int(0) <= jtag_addr_int(C_BRAM_MAX_ADDR_WIDTH-1);
        end if;
      end if;
    end process bram_d_shift;
    --
    shift_dout <= jtag_din_int(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1);
    --
    --
    din_load_select:process (bram_ce, din_load, capture, bus_zero, control_reg_ce) 
    begin
      if ( bram_ce = bus_zero ) then
        din_load <= capture and control_reg_ce;
       else
        din_load <= capture;
      end if;
    end process din_load_select;
    --
    --
    -- Control Registers 
    --
    num_picoblaze <= conv_std_logic_vector(C_NUM_PICOBLAZE-1,3);
    picoblaze_instruction_data_width <= conv_std_logic_vector(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1,5);
    --	
    control_registers: process(jtag_clk_int) 
    begin
      if (jtag_clk_int'event and jtag_clk_int = '1') then
        if (bram_ce_valid = '1') and (jtag_we_int = '0') and (control_reg_ce = '1') then
          case (jtag_addr_int(3 downto 0)) is 
            when "0000" => -- 0 = version - returns (7 downto 4) illustrating number of PB
                           --               and (3 downto 0) picoblaze instruction data width
                           control_dout_int <= num_picoblaze & picoblaze_instruction_data_width;
            when "0001" => -- 1 = PicoBlaze 0 reset / status
                           if (C_NUM_PICOBLAZE >= 1) then 
                            control_dout_int <= picoblaze_reset_int(0) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_0-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0010" => -- 2 = PicoBlaze 1 reset / status
                           if (C_NUM_PICOBLAZE >= 2) then 
                             control_dout_int <= picoblaze_reset_int(1) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_1-1,5) );
                            else 
                             control_dout_int <= (others => '0');
                           end if;
            when "0011" => -- 3 = PicoBlaze 2 reset / status
                           if (C_NUM_PICOBLAZE >= 3) then 
                            control_dout_int <= picoblaze_reset_int(2) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_2-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0100" => -- 4 = PicoBlaze 3 reset / status
                           if (C_NUM_PICOBLAZE >= 4) then 
                            control_dout_int <= picoblaze_reset_int(3) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_3-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0101" => -- 5 = PicoBlaze 4 reset / status
                           if (C_NUM_PICOBLAZE >= 5) then 
                            control_dout_int <= picoblaze_reset_int(4) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_4-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0110" => -- 6 = PicoBlaze 5 reset / status
                           if (C_NUM_PICOBLAZE >= 6) then 
                            control_dout_int <= picoblaze_reset_int(5) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_5-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "0111" => -- 7 = PicoBlaze 6 reset / status
                           if (C_NUM_PICOBLAZE >= 7) then 
                            control_dout_int <= picoblaze_reset_int(6) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_6-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "1000" => -- 8 = PicoBlaze 7 reset / status
                           if (C_NUM_PICOBLAZE >= 8) then 
                            control_dout_int <= picoblaze_reset_int(7) & "00" & (conv_std_logic_vector(C_ADDR_WIDTH_7-1,5) );
                           else 
                            control_dout_int <= (others => '0');
                           end if;
            when "1111" => control_dout_int <= conv_std_logic_vector(C_BRAM_MAX_ADDR_WIDTH -1,8);
            when others => control_dout_int <= (others => '1');
          end case;
        else 
          control_dout_int <= (others => '0');
        end if;
      end if;
    end process control_registers;
    -- 
    control_dout(C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-1 downto C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-8) <= control_dout_int;
    --
    pb_reset: process(jtag_clk_int) 
    begin
      if (jtag_clk_int'event and jtag_clk_int = '1') then
        if (bram_ce_valid = '1') and (jtag_we_int = '1') and (control_reg_ce = '1') then
          picoblaze_reset_int(C_NUM_PICOBLAZE-1 downto 0) <= control_din(C_NUM_PICOBLAZE-1 downto 0);
        end if;
      end if;
    end process pb_reset;    
    --
    --
    -- Assignments 
    --
    control_dout (C_PICOBLAZE_INSTRUCTION_DATA_WIDTH-9 downto 0) <= (others => '0') when (C_PICOBLAZE_INSTRUCTION_DATA_WIDTH > 8);
    --
    -- Qualify the blockram CS signal with bscan select output
    jtag_en_int <= bram_ce when bram_ce_valid = '1' else (others => '0');
    --      
    jtag_en_expanded(C_NUM_PICOBLAZE-1 downto 0) <= jtag_en_int;
    jtag_en_expanded(7 downto C_NUM_PICOBLAZE) <= (others => '0') when (C_NUM_PICOBLAZE < 8);
    --        
    bram_dout_int <= control_dout or jtag_dout_0_masked or jtag_dout_1_masked or jtag_dout_2_masked or jtag_dout_3_masked or jtag_dout_4_masked or jtag_dout_5_masked or jtag_dout_6_masked or jtag_dout_7_masked;
    --
    control_din <= jtag_din_int;
    --        
    jtag_dout_0_masked <= jtag_dout_0 when jtag_en_expanded(0) = '1' else (others => '0');
    jtag_dout_1_masked <= jtag_dout_1 when jtag_en_expanded(1) = '1' else (others => '0');
    jtag_dout_2_masked <= jtag_dout_2 when jtag_en_expanded(2) = '1' else (others => '0');
    jtag_dout_3_masked <= jtag_dout_3 when jtag_en_expanded(3) = '1' else (others => '0');
    jtag_dout_4_masked <= jtag_dout_4 when jtag_en_expanded(4) = '1' else (others => '0');
    jtag_dout_5_masked <= jtag_dout_5 when jtag_en_expanded(5) = '1' else (others => '0');
    jtag_dout_6_masked <= jtag_dout_6 when jtag_en_expanded(6) = '1' else (others => '0');
    jtag_dout_7_masked <= jtag_dout_7 when jtag_en_expanded(7) = '1' else (others => '0');
    --
    jtag_en <= jtag_en_int;
    jtag_din <= jtag_din_int;
    jtag_addr <= jtag_addr_int;
    jtag_clk <= jtag_clk_int;
    jtag_we <= jtag_we_int;
    picoblaze_reset <= picoblaze_reset_int;
    --        
  end generate jtag_loader_gen;
--
end Behavioral;
--
--
------------------------------------------------------------------------------------
--
-- END OF FILE cli.vhd
--
------------------------------------------------------------------------------------
