
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_misc.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

entity sync_lockon is
  generic (
    SYNC_STREAM : std_logic_vector := "1100000101");
  port (
    resync : in std_logic;

    clk_in_stream : in std_logic;
    stream_in     : in std_logic_vector(1 downto 0);


    clk_out_stream  : in  std_logic;
    data_out        : out std_logic_vector(9 downto 0) := (others => '0');
    data_valid      : out std_logic                    := '0';
    alignment_error : out std_logic                    := '0';
    overflow_error  : out std_logic                    := '0'
    );
end entity sync_lockon;

architecture behavioral of sync_lockon is

  component DESERIALIZER_SYNC_FIFO is
    port (
      rst    : in  std_logic;
      wr_clk : in  std_logic;
      rd_clk : in  std_logic;
      din    : in  std_logic_vector(7 downto 0);
      wr_en  : in  std_logic;
      rd_en  : in  std_logic;
      dout   : out std_logic_vector(7 downto 0);
      full   : out std_logic;
      empty  : out std_logic;
      valid  : out std_logic);
  end component DESERIALIZER_SYNC_FIFO;

  component pacd is
    port (
      iPulseA : IN  std_logic;
      iClkA   : IN  std_logic;
      iRSTAn  : IN  std_logic;
      iClkB   : IN  std_logic;
      iRSTBn  : IN  std_logic;
      oPulseB : OUT std_logic);
  end component pacd;

  -------------------------------------------------------------------------------
  -- signals
  -------------------------------------------------------------------------------
  signal resync_local : std_logic := '1';
  
  -- build_window
  signal search_window : std_logic_vector(13 downto 0) := "00"&x"000";

  --window_search
  signal window_has_sync_stream : std_logic := '0';
  signal window_lockon_phase    : std_logic := '0';

  --window_deserializer
  signal data_locked : std_logic                    := '0';
  signal window_valid       : std_logic_vector(4 downto 0) := "00001";
  signal window_valid_error : std_logic                    := '0';

  --Latch_framed_stream
  signal latched_data_valid : std_logic                    := '0';
  signal latched_data       : std_logic_vector(9 downto 0) := (others => '0');
  signal data_overflow      : std_logic                    := '0';

  --DESERIALIZER_SYNC_FIFO
  signal fifo_read       : std_logic                    := '0';
  signal fifo_data       : std_logic_vector(9 downto 0) := (others => '0');
  signal fifo_full       : std_logic                    := '0';
  signal fifo_empty      : std_logic                    := '0';
  signal fifo_data_valid : std_logic                    := '0';

begin
  -------------------------------------------------------------------------------
  -- Keep all 5x clock domain processes on the same value of resync
  -------------------------------------------------------------------------------
  resync_buffer: process (clk_in_stream) is
  begin  -- process resync_buffer
    if clk_in_stream'event and clk_in_stream = '1' then  -- rising clock edge
      resync_local <= resync;
    end if;
  end process resync_buffer;
  
  -------------------------------------------------------------------------------
  --Shift in the incomming bits for lockon and decoding
  -------------------------------------------------------------------------------
  build_window : process (clk_in_stream) is
  begin  -- process build_window
    if clk_in_stream'event and clk_in_stream = '1' then  -- rising clock edge
      if resync_local = '1' then 
        search_window <= (others => '0');
      else      
        --shift in bits of each stream.  check stream for idle character and set
        --enable counter for capture
--        search_window <= stream_in & search_window(13 downto 2);
        search_window <= search_window(11 downto 0) & stream_in;
      end if;
    end if;
  end process build_window;

  -------------------------------------------------------------------------------  
  --Search out floating window for a sync character
  -------------------------------------------------------------------------------  
  window_search : process (clk_in_stream) is
  begin  -- process window_search
    if clk_in_stream'event and clk_in_stream = '1' then  -- rising clock edge
      if resync_local = '1' then  
        window_has_sync_stream <= '0';
      else       
        window_has_sync_stream <= '0';
        -- check both phases to see if they are an idle character
        if (search_window(11 downto 2) = SYNC_STREAM or
            search_window(11 downto 2) = (not SYNC_STREAM)) then
          window_has_sync_stream <= '1';
          window_lockon_phase    <= '0';
        elsif (search_window(10 downto 1) = SYNC_STREAM or
               search_window(10 downto 1) = (not SYNC_STREAM)) then
          window_has_sync_stream <= '1';
          window_lockon_phase    <= '1';
        end if;
      end if;
    end if;
  end process window_search;

  -------------------------------------------------------------------------------  
  -- Construct the valid window frame signals and monitor for unframed syncs
  -------------------------------------------------------------------------------
  window_deserializer : process (clk_in_stream) is
  begin  -- process window_deserializer
    if clk_in_stream'event and clk_in_stream = '1' then  -- rising clock edge
      --warn when a sync char is found when not the previously found sync boundary
      window_valid_error <= '0';

      if resync_local = '1' then           
        data_locked <= '0';
        window_valid       <= "00000";
      else        
        if window_has_sync_stream = '1' then
          -- we got a sync char, so resync our stream
          window_valid       <= "00001";
          data_locked <= '1';      -- we are locked on
          -- check if this sync falls outside of a previously found sync boundary
          if window_valid(4) = '0' and data_locked = '1' then
            window_valid_error <= '1';
          end if;
        elsif window_valid(4) = '1' then
          -- The data is valid on this clock tick, shift in another valid for the
          -- next valid char
          window_valid <= window_valid(3 downto 0) & '1';
        else
          -- this clock tick is inbetween valid word alignments
          window_valid <= window_valid(3 downto 0) & '0';
        end if;
      end if;
    end if;
  end process window_deserializer;

  -------------------------------------------------------------------------------
  -- Latch the framed data and put it into a FIFO
  -------------------------------------------------------------------------------
  Latch_framed_stream : process (clk_in_stream) is
  begin  -- process Latch_framed_stream
    if clk_in_stream'event and clk_in_stream = '1' then  -- rising clock edge
      if resync_local = '1' then   
        latched_data_valid <= '0';
        latched_data       <= (others => '0');
        data_overflow      <= '0';
      else
        data_overflow <= '0';
        latched_data_valid <= '0';
        if window_valid(4) = '1' then
          if FIFO_full = '1' then
            data_overflow <= '1';
          else
            latched_data_valid <= '1';            
            if window_lockon_phase = '1' then
--              latched_data <= search_window(12 downto 3);
            --switch bit order for 8b10b decoder              
              for iBit in 0 to 9 loop
                latched_data(iBit) <= search_window(12-iBit);
              end loop;
            else
--              latched_data <= search_window(13 downto 4);
              --switch bit order for 8b10b decoder
              for iBit in 0 to 9 loop
                latched_data(iBit) <= search_window(13-iBit);
              end loop;
            end if;
          end if;
        end if;
      end if;
    end if;
  end process Latch_framed_stream;


  -------------------------------------------------------------------------------
  -- Use a fifo to move the framed valid bitstreams to a lower frequency clock
  -- domain
  -------------------------------------------------------------------------------  
  DESERIALIZER_SYNC_FIFO_1 : entity work.DESERIALIZER_SYNC_FIFO
    port map (
      rst    => resync_local,
      wr_clk => clk_in_stream,
      rd_clk => clk_out_stream,
      din    => latched_data,
      wr_en  => latched_data_valid,
      rd_en  => fifo_read,
      dout   => fifo_data,
      full   => fifo_full,
      empty  => fifo_empty,
      valid  => fifo_data_valid);

  -------------------------------------------------------------------------------
  -- output datastream on the slower clock domain
  -------------------------------------------------------------------------------  
  data_output : process (clk_out_stream, resync) is
  begin  -- process data_output
    if resync = '1' then
      fifo_read  <= '0';
      data_valid <= '0';
    elsif clk_out_stream'event and clk_out_stream = '1' then  -- rising clock edge
      --Handle reading for next clock tick
      fifo_read <= '0';
      if fifo_empty = '0' then
        fifo_read <= '1';
      end if;
      -- handle data coming out of the fifo
      -- send it out of the module
      data_valid <= '0';
      if fifo_data_valid = '1' then
        data_valid <= '1';
        data_out   <= fifo_data;
      end if;
    end if;
  end process data_output;

  -------------------------------------------------------------------------------
  --convert errors from capture clock domain to output clock domain
  -------------------------------------------------------------------------------  
  pacd_1: entity work.pacd
    port map (
      iPulseA => window_valid_error,
      iClkA   => clk_in_stream,
      iRSTAn  => '1',
      iClkB   => clk_out_stream,
      iRSTBn  => resync,
      oPulseB => alignment_error);
  pacd_2: entity work.pacd
    port map (
      iPulseA => data_overflow,
      iClkA   => clk_in_stream,
      iRSTAn  => '1',
      iClkB   => clk_out_stream,
      iRSTBn  => resync,
      oPulseB => overflow_error);

  
end architecture behavioral;
