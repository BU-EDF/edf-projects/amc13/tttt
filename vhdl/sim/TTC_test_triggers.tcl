restart 

isim force add {/top/NIM_in[0]} 0

isim force add {/top/clk_local_in} 0 -radix hex -value 1 -radix hex -time 12500 ps -repeat 25 ns
run 1 ms

#do nothing
isim force add {/top/reg_addr} 21 -radix hex
isim force add {/top/reg_data_out} 0000000000000000 -radix hex
isim force add {/top/reg_wr} 1
run 25ns
isim force add {/top/reg_wr} 0
run 1ms
#periodic trigger
isim force add {/top/reg_addr} 21 -radix hex
isim force add {/top/reg_data_out} 0000123400000001 -radix hex
isim force add {/top/reg_wr} 1
run 25ns
isim force add {/top/reg_wr} 0
run 1ms
#random trigger
isim force add {/top/reg_addr} 21 -radix hex
isim force add {/top/reg_data_out} 4321000000000002 -radix hex
isim force add {/top/reg_wr} 1
run 25ns
isim force add {/top/reg_wr} 0
run 1ms

#software trigger
isim force add {/top/reg_addr} 21 -radix hex
isim force add {/top/reg_data_out} 0000000000000004 -radix hex
isim force add {/top/reg_wr} 1
run 25ns
isim force add {/top/reg_wr} 0
run 500us
isim force add {/top/reg_addr} 21 -radix hex
isim force add {/top/reg_data_out} 0000000000000014 -radix hex
isim force add {/top/reg_wr} 1
run 25ns
isim force add {/top/reg_wr} 0
run 500us


#random + periodic trigger
isim force add {/top/reg_addr} 21 -radix hex
isim force add {/top/reg_data_out} 0080002200000003 -radix hex
isim force add {/top/reg_wr} 1
run 25ns
isim force add {/top/reg_wr} 0
run 2ms
