----------------------------------------------------------------------------------
-- Company: Boston University EDF
-- Engineer: Dan Gastler
-- 
-- Create Date:    
-- Design Name: 
-- Module Name:    top_module - Behavioral
-- Project Name: TTTT
-- Target Devices: XC6SLX9-TQFP144
-- Tool versions: 13.3 (lin64)
-- Description: 
--
-- Dependencies: 
--
-- Revision:
-- Revision 0.1 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

use work.TCDS.all;
use work.TTC.all;
use work.TTC_chB.all;


-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity top is
  port (
    -- LHC Clock signals
    clk_local_in   : in  std_logic;     -- Local 40.08Mhz clock
--    clk_external_in  : in  std_logic;   -- External LHC 40.08MHz clock
--    clk_external_out : out std_logic;   -- Output of LHC 40.08Mhz clock
    -- GbE connections
--    GTP_REFCLK_P   : in  std_logic;     -- GTP clock p input (125Mhz)
--    GTP_REFCLK_N   : in  std_logic;     -- GTP clock n input (125Mhz)
--    GTP_TX_P       : out std_logic;     -- GTP TX P
--    GTP_TX_N       : out std_logic;     -- GTP TX N
--    GTP_RX_P       : in  std_logic;     -- GTP RX P
--    GTP_RX_N       : in  std_logic;     -- GTP RX N
--    GBE_los        : in  std_logic;
    -- TCDS signals
    SFP_Rx_P       : in  std_logic_vector(0 downto 0);
    SFP_Rx_N       : in  std_logic_vector(0 downto 0);
    SFP_Tx_P       : out std_logic_vector(0 downto 0);
    SFP_Tx_N       : out std_logic_vector(0 downto 0);
--    SFP_LOS          : in  std_logic_vector(3 downto 0);
    SFP_TX_Disable : out std_logic_vector(0 downto 0);
    SFP_Present    : in  std_logic_vector(0 downto 0);
--    SFP_Tx_Fault     : in  std_logic_vector(3 downto 0);
--    -- USB UART IO
    UART_Rx        : in  std_logic;
    UART_Tx        : out std_logic;
--    -- FLASH programming
--    FLASH_SPI_CLK    : out std_logic;
--    FLASH_SPI_DQ0    : out std_logic;
--    FLASH_SPI_DQ1    : in  std_logic;
--    FLASH_SPI_S      : out std_logic;
    -- LEDs
    LED            : out std_logic_vector(7 downto 0);
   -- GPIOs
--    GPIO             : out std_logic_vector(3 downto 0);
--    -- NIM IOs
    NIM_in           : in  std_logic_vector(0 downto 0)
--    NIM_out          : out std_logic_vector(3 downto 0)
    );
end top;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- ARCHITECTURE
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
architecture Behavioral of top is

  component uC is
    port (
      clk            : in  std_logic;
      reset          : in  std_logic;
      UART_Rx        : in  std_logic;
      UART_Tx        : out std_logic;
      reg_addr       : out std_logic_vector(15 downto 0);
      reg_data_in_rd : out std_logic;
      reg_data_in    : in  std_logic_vector(63 downto 0);
      reg_data_in_dv : in  std_logic;
      reg_data_out   : out std_logic_vector(63 downto 0);
      reg_wr         : out std_logic);
  end component uC;
  
  component register_map is
    generic (
      WIDTH : integer);
    port (
      clk              : in  std_logic;
      reset            : in  std_logic;
      address          : in  std_logic_vector(15 downto 0);
      out_port_rd      : in  std_logic := '0';
      out_port         : out std_logic_vector(WIDTH-1 downto 0);
      out_port_dv      : out std_logic := '0';
      in_port          : in  std_logic_vector(WIDTH-1 downto 0);
      in_port_wr       : in  std_logic := '0';
      Firmware_Version : in  std_logic_vector(64 downto 0);
      TCDS_in          : in  TCDS_Monitor;
      TCDS_out         : out TCDS_Control;
      TTC_in           : in  TTC_Monitor;
      TTC_out          : out TTC_Control);
  end component register_map;

  component LHC_TTC_clocking is
    port (
      CLK_IN            : in  std_logic;
      CLK_LHC_4x        : out std_logic;
      CLK_LHC           : out std_logic;
      RESET             : in  std_logic;
      STATUS            : out std_logic_vector(2 downto 0);
      INPUT_CLK_STOPPED : out std_logic;
      LOCKED            : out std_logic;
      CLK_VALID         : out std_logic);
  end component LHC_TTC_clocking;




  component TTC_Interface is
    port (
      clk_lhc     : in  std_logic;
      clk_lhc_4x  : in  std_logic;
      reset       : in  std_logic;
      TTS         : in std_logic_vector(3 downto 0);
      SFP_Tx_P    : out std_logic;
      SFP_Tx_N    : out std_logic;
      external_trigger : in std_logic;
      Monitor     : out TTC_Monitor;
      Control     : in TTC_Control);
  end component TTC_Interface;


  component TCDS_interface is
    port (
      clk_LHC     : in  std_logic;
      reset       : in  std_logic;
      TCDS_p      : in  std_logic;
      TCDS_n      : in  std_logic;
      Monitor     : out TCDS_Monitor;
      Control     : in TCDS_Control);
  end component TCDS_interface;


  signal reset      : std_logic := '0';
  signal clk_lhc    : std_logic := '0';
  signal clk_lhc_4x : std_logic := '0';


  -- TCDS data
  signal TCDS_reset      : std_logic := '0';
  signal data            : std_logic_vector(7 downto 0);
  signal data_is_k_char  : std_logic := '0';
  signal data_valid      : std_logic := '0';
  signal data_locked     : std_logic := '0';
  signal error_bad_code  : std_logic := '0';
  signal error_disparity : std_logic := '0';
  signal error_alignment : std_logic := '0';
  signal error_overflow  : std_logic := '0';


  -- ioregister
  signal TCDS_in   : TCDS_Monitor;
  signal TCDS_out  : TCDS_Control;
  signal TTC_in    : TTC_Monitor;
  signal TTC_out   : TTC_Control;

  signal reg_addr : std_logic_vector(15 downto 0) := (others => '0');
  signal reg_data_in_rd : std_logic := '0';
  signal reg_data_in : std_logic_vector(63 downto 0) := (others => '0');   
  signal reg_data_in_dv : std_logic := '0';
  signal reg_data_out : std_logic_vector(63 downto 0) := (others => '0');   
  signal reg_wr : std_logic := '0';        

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- BEGIN
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
begin

  ---------------------------------------
  -- TTTT (TCDS,TTS,TTC) stuff
  ---------------------------------------

  LHC_TTC_clocking_1 : entity work.LHC_TTC_clocking
    port map (
      CLK_IN            => clk_local_in,
      CLK_LHC_4x        => CLK_LHC_4x,
      CLK_LHC           => clk_LHC,
      RESET             => reset,
      STATUS            => open,
      INPUT_CLK_STOPPED => open,
      LOCKED            => LED(1),
      CLK_VALID         => LED(3));

  LED(5)                 <= SFP_Present(0);
  LED(7)                 <= TCDS_in.locked;

  LED(0) <= TCDS_in.TTS_state(3) and TCDS_in.locked;
  LED(2) <= TCDS_in.TTS_state(2) and TCDS_in.locked;
  LED(4) <= TCDS_in.TTS_state(1) and TCDS_in.locked;
  LED(6) <= TCDS_in.TTS_state(0) and TCDS_in.locked;     
  
  TTC_Interface_1 : entity work.TTC_Interface
    port map (
      clk_lhc     => clk_lhc,
      clk_lhc_4x  => clk_lhc_4x,
      reset       => reset,
      TTS         => TCDS_in.TTS_state,
      SFP_Tx_P    => SFP_Tx_P(0),
      SFP_Tx_N    => SFP_Tx_N(0),
      external_trigger => NIM_in(0),
      Monitor     => TTC_in,
      Control     => TTC_out);


  SFP_TX_Disable(0) <= '0';
  TCDS_interface_1 : entity work.TCDS_interface
    port map (
      clk_LHC     => clk_LHC,
      reset       => reset,
      TCDS_p      => SFP_Rx_P(0),
      TCDS_n      => SFP_Rx_N(0),
      Monitor     => TCDS_in,
      Control     => TCDS_out);


  register_map_1: entity work.register_map
    port map (
      clk              => clk_LHC,
      reset            => reset,
      address          => reg_addr,
      out_port_rd      => reg_data_in_rd,
      out_port         => reg_data_in,
      out_port_dv      => reg_data_in_dv,
      in_port          => reg_data_out,
      in_port_wr       => reg_wr,
      Firmware_Version => x"2015091800000002",
      TCDS_in          => TCDS_in,
      TCDS_out         => TCDS_out,
      TTC_in           => TTC_in,
      TTC_out          => TTC_out);

  uC_1: entity work.uC
    port map (
      clk            => clk_LHC,
      reset          => reset,
      UART_Rx        => UART_Rx,
      UART_Tx        => UART_Tx,
      reg_addr       => reg_addr,
      reg_data_in_rd => reg_data_in_rd,
      reg_data_in    => reg_data_in,
      reg_data_in_dv => reg_data_in_dv,
      reg_data_out   => reg_data_out,
      reg_wr         => reg_wr);
  
end Behavioral;

