restart
source vhdl/sim/UART_helper.tcl

#set initial conditions for UART
isim force add {/top/UART_rx} 1

#set clock on /top/clk
isim force add {/top/clk_local_in} 1 -radix bin -value 0 -radix bin -time 12.5 ns -repeat 25 ns

#get random number generator into a useful state
#isim force /top/TTC_Interface_1/Trigger_Generator_1/urand_1/m1/U0/ 

#let everything start up
run 2 ms

# take some triggers
sendUART_str rw 115200 /top/UART_rx;
#send space (0x20)
sendUART_hex 20 115200 /top/UART_rx;
sendUART_str 21 115200 /top/UART_rx;

#send space (0x20)
sendUART_hex 20 115200 /top/UART_rx;
sendUART_str 0FFF0FFF00000002 115200 /top/UART_rx;
#send CR (0x0D)
sendUART_hex 0D 115200 /top/UART_rx;

run 100 us

#set the BC trigger range to the middle 0x55 to 0x800
sendUART_str rw 115200 /top/UART_rx;
#send space (0x20)
sendUART_hex 20 115200 /top/UART_rx;
sendUART_str 20 115200 /top/UART_rx;

#send space (0x20)
sendUART_hex 20 115200 /top/UART_rx;
sendUART_str 0000010000551000 115200 /top/UART_rx;
#send CR (0x0D)
sendUART_hex 0D 115200 /top/UART_rx;



run 2 ms
